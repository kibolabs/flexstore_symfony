<?php
// Populate all defined categories in the accordion	
$new_project_ui = '<div class="card panel">
						<div class="card-head style-default-light">
							<header>Add new project</header>
							<div class="tools">
								<span data-toggle="tooltip" data-placement="top" data-original-title="Add project">
									<a href="#projectsModal" class="btn btn-icon-toggle ink-reaction" data-toggle="modal"><i class="fa fa-plus-circle"></i></a>
								</span>
							</div>
						</div>
					</div>';
$item_projects = "";
$sql = "SELECT * FROM projects ORDER BY projname ASC";
$proj_query = mysqli_query($db_connect, $sql);
if(mysqli_num_rows($proj_query) > 0){
	$item_projects .='
					<div class="card">
						<div class="card-body">
							<table id="projects_tb" class="table table-hover no-margin">
								<thead>
									<tr>
										<th>#</th>
										<th>Project Name</th>
										<th>Project Code</th>
										<th>Added By</th>
										<th class="text-right">Actions</th>
									</tr>
								</thead>
								<tbody id="proj_list">
	';
	$i = 0;
	while ($row = mysqli_fetch_array($proj_query, MYSQLI_ASSOC)) {
			$proj_id = $row["id"];
			$proj_name = $row["projname"];
			$proj_code = $row["projcode"];
			$userid = $row["addedby"];
				$user_query = mysqli_query($db_connect, "SELECT username FROM users WHERE id=$userid LIMIT 1");
				if(mysqli_num_rows($user_query) > 0){
					while ($row = mysqli_fetch_array($user_query, MYSQLI_ASSOC)) {
						$addedby = $row["username"];
					}
				} else {
					$addedby = "unknown";
				}
			$adddate = $row["add_date"];
			
			$i = $i+1;
			$item_projects .= '
							<tr id="proj_row' . $proj_id . '">
								<td>' . $i . '</td>
								<td id="projname_' . $proj_id . '">' . $proj_name . '</td>
								<td id="projcode_' . $proj_id . '">' . $proj_code . '</td>
								<td>' . $addedby . '</td>
								<td class="text-right">
									<span type="button" class="btn btn-icon-toggle  ink-reaction" data-toggle="tooltip" data-placement="top" data-original-title="Edit project">
										<a href="#EditProjModal" type="button" id="EditProjLink' . $proj_id . '" data-toggle="modal" data-id="' . $proj_id . '" class="open-EditProjModal btn btn-icon-toggle" data-projname="' . $proj_name . '" data-projcode="' . $proj_code . '"><i class="fa fa-pencil"></i></a>
									</span>
									<span type="button" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Delete project">	
										<a href="#DelProjModal" type="button" id="DelProjLink' . $proj_id . '" data-toggle="modal" data-id="' . $proj_id . '" class="open-DelProjModal btn btn-icon-toggle" data-projname="' . $proj_name . '"  data-projcode="' . $proj_code . '"><i class="fa fa-trash-o"></i></a>	
									</span>
								</td>
							</tr>
							';
			
		
	} // END WHILE LOOP
	$item_projects .='
					</tbody>
				</table>
			</div><!--end .card-body -->
		</div><!--end .card -->
	';
	
} else {
	$item_projects = "No projects defined";
}
?>
<script>
//AJAX PROCESS TO SAVE NEW PROJECT
function saveProject(){
	var action = "newProj";
	var newProj_name = _("proj_name").value;
	var newProj_code = _("proj_code").value;
	//var statusMessage = _("saveProjectMessage").innerHTML;
	var actionBtn = _("saveProjBtn");
	if(newProj_name == "" || newProj_code == ""){
		_("saveProjectMessage").innerHTML = "Please fill both Project name and code";
		return false;
	} 
	actionBtn.disabled = true;
	_("saveProjectMessage").innerHTML = "Saving ...";
	var ajax = ajaxObj("POST", "assets/parsers/projects_system.php");
	console.log('Ajax request sent:'+ajax);
	ajax.onreadystatechange = function() {
		if(ajaxReturn(ajax) == true) {
			var datArray = ajax.responseText.split("|");
			if(datArray[0] == "saving_ok"){
				var newProj_id = datArray[1];
				var proj_rows = _("projects_tb").rows.length;
				var proj_no = proj_rows + 1 - 1;
				var currentHTML = _("proj_list").innerHTML;
				_("proj_list").innerHTML = currentHTML+'<tr id="proj_row'+newProj_id+'"><td>'+proj_no+'</td><td id="projname_'+newProj_id+'">'+newProj_name+'</td><td id="projname_'+newProj_id+'">'+newProj_code+'</td><td>'+'You'+'</td><td class="text-right"><span type="button" class="btn btn-icon-toggle  ink-reaction" data-toggle="tooltip" data-placement="top" data-original-title="Edit project"><a href="#EditProjModal" type="button" id="EditProjLink'+newProj_id+'" data-toggle="modal" data-id="'+newProj_id+'" class="open-EditProjModal btn btn-icon-toggle" data-projname="'+newProj_name+'" data-projcode="'+newProj_code+'"><i class="fa fa-pencil"></i></a></span><span type="button" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Delete project">	<a href="#DelProjModal" type="button" id="DelProjLink'+newProj_id+'" data-toggle="modal" data-id="'+newProj_id+'" class="open-DelProjModal btn btn-icon-toggle" data-projname="'+newProj_name+'"  data-projcode="'+newProj_code+'"><i class="fa fa-trash-o"></i></a></span></td></tr>';
				
				_("proj_name").value = "";
				_("proj_code").value = "";
				console.log('Proj_name field:'+_("proj_name").value+', Proj_code field:'+_("proj_code").value);
				_("saveProjectMessage").innerHTML = "";
				actionBtn.disabled = false;
				$('#projectsModal').modal('hide');
				console.log(ajax.responseText);
			} else {
				//alert(ajax.responseText);
				console.log(ajax.responseText);
				_("saveProjectMessage").innerHTML = ajax.responseText;
				actionBtn.disabled = false;
			}
		}
	}
	ajax.send("action="+action+"&proj_name="+newProj_name+"&proj_code="+newProj_code);
}

//AJAX PROCESS TO EDIT PROJECT
function editProject(editProjID,editProjName,editProjCode){
	var action = "editProj";
	var editProjID = editProjID;
	var oldProj_Name = editProjName;
	var newProj_Name = _("newproject_name").value;
	var oldProj_Code = editProjCode;
	var newProj_Code = _("newproject_code").value;
	//var statusMessage = _("editProjectMessage").innerHTML;
	var actionBtn = _("editProjBtn"+editProjID);
	console.log('editProjID:'+editProjID+',oldProj_Name:'+oldProj_Name+',newProj_Name:'+newProj_Name+',oldProj_Code:'+oldProj_Code+',newProj_Code:'+newProj_Code+',statusMessage:'+statusMessage);
	if(editProjID == ""){
		_("editProjectMessage").innerHTML = "Project has not been well specified";
		return false;
	} else if (newProj_Name == ""){
		_("editProjectMessage").innerHTML = "The project name can not be empty";
		return false;
	} else if (newProj_Code == ""){
		_("editProjectMessage").innerHTML = "The project code can not be empty";
		return false;
	} else if (oldProj_Name == newProj_Name && oldProj_Code == newProj_Code){
		_("editProjectMessage").innerHTML = "You have not made any changes yet!";
		return false;
	} else {
		actionBtn.disabled = true;
		statusMessage = "Saving ...";
		var ajax = ajaxObj("POST", "assets/parsers/projects_system.php");
		//console.log('Ajax request sent:'+ajax);
		ajax.onreadystatechange = function() {
			if(ajaxReturn(ajax) == true) {
				if(ajax.responseText == "saving_ok"){
					actionBtn.disabled = false;
					_("editProjectMessage").innerHTML = "";
					$('#EditProjModal').modal('hide');
					_("projname_"+editProjID).innerHTML = newProj_Name;
					_("projcode_"+editProjID).innerHTML = newProj_Code;
					_("DelProjLink"+editProjID).setAttribute('data-projname', newProj_Name);
					_("EditProjLink"+editProjID).setAttribute('data-projname', newProj_Name);
					_("EditProjLink"+editProjID).setAttribute('data-projcode', newProj_Code);
					
										
					console.log('Changed Project name from: '+oldProj_Name+' to :'+$("#EditProjLink").attr('data-projname'));
					console.log('Changed Project code from: '+oldProj_Code+' to :'+$("#EditProjLink").attr('data-projcode'));
					console.log(ajax.responseText);
				} else {
					//alert(ajax.responseText);
					console.log(ajax.responseText);
					//statusMessage = ajax.responseText;
					_("editProjectMessage").innerHTML = ajax.responseText;
					actionBtn.disabled = false;
				}
			}
		}
		ajax.send("action="+action+"&proj_id="+editProjID+"&oldProjName="+oldProj_Name+"&oldProjCode="+oldProj_Code+"&newProjName="+newProj_Name+"&newProjCode="+newProj_Code);
	}
}

//AJAX PROCESS TO DELETE CATEGORY
function delProject(id){
	//console.log('delCategory function excecuted');
	var action = "delProj";
	var delProj_id = id;
	//var statusMessage = _("deleteProjectMessage").innerHTML;
	var actionBtn = _("delProjBtn"+delProj_id);
	if(delProj_id == ""){
		_("deleteProjectMessage").innerHTML = "Something went wrong!!! a project id was not specified";
		return false;
	}
	actionBtn.disabled = true;
	actionBtn.innerHTML = "Deleting ...";
	var ajax = ajaxObj("POST", "assets/parsers/projects_system.php");
	//console.log('Ajax request sent:'+ajax);
	ajax.onreadystatechange = function() {
		if(ajaxReturn(ajax) == true) {
			if(ajax.responseText == "delete_ok"){
				_("proj_row"+delProj_id).outerHTML = "";
				delete _("proj_row"+delProj_id);
				actionBtn.disabled = false;
				_("deleteProjectMessage").innerHTML = "";
				$('#DelProjModal').modal('hide');
			} else {
				//alert(ajax.responseText);
				//console.log(ajax.responseText);
				_("deleteProjectMessage").innerHTML = ajax.responseText;
				actionBtn.disabled = false;
				actionBtn.innerHTML = "Delete";
			}
		}
	}
	ajax.send("action="+action+"&proj_id="+delProj_id);
}

</script>
<?php echo $new_project_ui; ?>
<div class="panel-group" id="asset_categories">
	<?php echo $item_projects; ?>
</div><!--end .panel-group -->

<!-- BEGIN NEW PROJECT MODAL MARKUP -->
<div class="modal fade" id="projectsModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">New Project ...</h4>
            </div>
            <form id="newProjectForm" class="form-horizontal" role="form" data-toggle="validator" onSubmit="return false;" accept-charset="utf-8"> 
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="proj_name" class="control-label">Project name *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="proj_name" id="proj_name" type="text" class="form-control" onfocus="emptyElement('saveProjectMessage')" placeholder="new project name" data-rule-minlength="3" maxlength="40" required>
                            <p class="help-block">3 to 40 characters</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="proj_code" class="control-label">Project code *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="proj_code" id="proj_code" type="text" class="form-control" onfocus="emptyElement('saveProjectMessage')" onkeyup="omitChars('proj_code','letters_underscore_i')" placeholder="new project code" data-rule-minlength="3" maxlength="16" required>
                            <p class="help-block">only letters and underscore, 3-16 characters</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                        </div>
                        <p></p>
                        <p style="text-align:center; color:red" id="saveProjectMessage"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" id="saveProjBtn" onClick="saveProject()" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END NEW PROJECT MODAL MARKUP -->

<!-- BEGIN EDIT PROJECT MODAL MARKUP -->
<div class="modal fade" id="EditProjModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="editModalHeader" class="modal-title">Edit project ...</h4>
            </div>
            <form id="editProjForm" class="form-horizontal" role="form" data-toggle="validator" onSubmit="return false;" accept-charset="utf-8"> 
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="newproject_name" class="control-label">Project name *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="newproject_name" id="newproject_name" type="text" class="form-control" onfocus="emptyElement('editProjectMessage')" placeholder="new project name" data-rule-minlength="3" maxlength="40" required>
                            <p class="help-block">3 to 40 characters</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="newproject_code" class="control-label">Project code *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="newproject_code" id="newproject_code" type="text" class="form-control" onfocus="emptyElement('editProjectMessage')" placeholder="new project code" data-rule-minlength="3" maxlength="16" required>
                            <p class="help-block">3 to 16 characters</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                        </div>
                        <p></p>
                        <p style="text-align:center; color:red" id="editProjectMessage"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <span id="confirmBtn">
                    	<button type="submit" id="editProjBtn" class="btn btn-primary">Save Changes</button>
                    </span>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END EDIT PROJECT MODAL MARKUP -->

<!-- BEGIN DELETE PROJECT MODAL MARKUP -->
<div id="DelProjModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 id="modalHeader" class="modal-title text-danger">Warning!</h4>

            </div>
            <div class="modal-body">
                <p id="modalContent">...</p>
                <p id="deleteProjectMessage" style="color:red"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <span id="confirmBtn">
                	<button type="button" class="btn btn-danger">Confirm</button>
                </span>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END DELETE PROJECT MODAL MARKUP -->