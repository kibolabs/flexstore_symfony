<!-- BEGIN AJAX LOGIN FORM MODAL MARKUP -->
<div class="modal fade" id="loginformModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="formModalLabel">Login to continue ...</h4>
        </div>
        <form id="loginform" class="form-horizontal" role="form" data-toggle="validator" onSubmit="return false;" accept-charset="utf-8"> 
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="email" class="control-label">Email *</label>
                    </div>
                    <div class="col-sm-9">
                        <input name="email" id="email" type="text" class="form-control" onfocus="emptyElement('nMessage')" placeholder="your email address" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="password" class="control-label">Password *</label>
                    </div>
                    <div class="col-sm-9">
                        <input name="pass" id="pass" type="password" class="form-control" onfocus="emptyElement('nMessage')" placeholder="your password" required>
                        <p class="help-block"><a href="temppass.php">Forgot password?</a>&nbsp;|&nbsp;<a href="register.php">Register?</a></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input name="remember" type="checkbox" id="remember" value="yes" checked="checked" /> Remember
                            </label>
                        </div>
                    </div>
                    <p style="text-align:center; color:red" id="nMessage"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" id="loginbtn" onClick="ajaxlogin()" class="btn btn-primary">Login</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END AJAX LOGIN FORM MODAL MARKUP -->