<?php
// Populate all defined categories in the accordion	
$new_category_ui = '<div class="card panel">
						<div class="card-head style-default-light">
							<header>Add new category</header>
							<div class="tools">
								<span data-toggle="tooltip" data-placement="top" data-original-title="Add category">
									<a class="btn btn-icon-toggle" href="#categoriesModal" data-toggle="modal"><i class="fa fa-plus-circle"></i></a>
								</span>
							</div>
						</div>
					</div>';
$asset_categories = "";
$sql = "SELECT * FROM categories ORDER BY cat_name ASC";
$cat_query = mysqli_query($db_connect, $sql);
if(mysqli_num_rows($cat_query) > 0){
	while ($row = mysqli_fetch_array($cat_query, MYSQLI_ASSOC)) {
			$category_id = $row["cat_id"];
			$category_name = $row["cat_name"];
			$category_shortname = $row["cat_shortname"];
			$category_addby = $row["addedby"];
			$category_adddate = $row["add_date"];
			
			// Get all related sub-categories
			$subcategories_list = ""; // Initialise an empty subcategories string 1st
			$sql_subcat = "SELECT * FROM sub_categories WHERE cat_id='$category_id' ORDER BY subcat_id";
			$query_subcat = mysqli_query($db_connect, $sql_subcat);
			if(mysqli_num_rows($query_subcat) > 0){
				while ($row = mysqli_fetch_array($query_subcat, MYSQLI_ASSOC)) {
						$subcat_id = $row["subcat_id"];
						$cat_id = $row["cat_id"];
						$subcat_name = $row["subcat_name"];
						$subcat_shortname = $row["subcat_shortname"];
						$subcat_addby = $row["addedby"];
						$subcat_adddate = $row["add_date"];
					
					$subcategories_list .= '<li id="subcategory-' . $subcat_id . '" class="tile">
												<a class="tile-content ink-reaction">
													<div id="subcatTitle_' . $subcat_id . '" class="tile-text">
														' . $subcat_name . '
														<small>' . $subcat_shortname . '</small>
													</div>
												</a>
												<a id="DelSubCatLink' . $subcat_id . '" class="open-DelSubCatModal btn btn-flat ink-reaction" href="#DelSubCatModal"  data-toggle="modal" data-catid="' . $category_id . '" data-subcatid="' . $subcat_id . '" data-subcatname="' . $subcat_name . '" data-subcatsname="' . $subcat_shortname . '">
													<i class="fa fa-trash"></i>
												</a>
												<a id="EditSubCatLink' . $subcat_id . '" class="open-EditSubCatModal btn btn-flat ink-reaction" href="#EditSubCatModal"  data-toggle="modal" data-catid="' . $category_id . '" data-subcatid="' . $subcat_id . '" data-subcatname="' . $subcat_name . '" data-subcatsname="' . $subcat_shortname . '">
													<i class="fa fa-pencil"></i>
												</a>
											</li>';
				} // END WHILE LOOP
			} else {
				$subcategories_list = 'No subcategories defined';
			}

			$asset_categories .= '
							<div class="card panel" id="category-' . $category_id . '">
								<div class="card-head collapsed" data-toggle="collapse" data-parent="#asset_categories" data-target="#asset_category-' . $category_id . '">
									<header id="catTitle' . $category_id . '">' . $category_name . '</header>
									<div class="tools">
										<span data-toggle="tooltip" data-placement="top" data-original-title="Delete category">
											<a href="#DelCatModal" id="DelCatLink' . $category_id . '" data-toggle="modal" data-id="' . $category_id . '" class="open-DelCatModal btn btn-icon-toggle" data-catname="' . $category_name . '"><i class="md md-delete"></i></a>
										</span>
										<span data-toggle="tooltip" data-placement="top" data-original-title="Edit category">
											<a  href="#EditCatModal" id="EditCatLink' . $category_id . '" data-toggle="modal" data-id="' . $category_id . '" class="open-EditCatModal btn btn-icon-toggle" data-catname="' . $category_name . '"><i class="md md-edit"></i></a>
										</span>
										<span data-toggle="tooltip" data-placement="top" data-original-title="Add new sub-category">
											<a href="#newSubCatModal" id="newSubCatLink' . $category_id . '" class="open-newSubCatModal btn btn-icon-toggle" data-toggle="modal" data-catid="' . $category_id . '" data-catname="' . $category_name . '"><i class="fa fa-plus-circle"></i></a>
										</span>
										<a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
									</div>
								</div>
								<div id="asset_category-' . $category_id . '" class="collapse">
									<div class="card-body">
										<div class="card-body no-padding">
											<ul class="list" id="subcategories_list' . $category_id . '">
												' . $subcategories_list . '
											</ul>
										</div>
									</div>
								</div>
							</div><!--end .panel -->';
			
		
	} // END WHILE LOOP
} else {
	$asset_categories = "No categories defined";
}
?>
<script>
//AJAX PROCESS TO SAVE NEW ASSETS CATEGORY
function saveCategory(){
	var action = "newCat";
	var newCat_name = _("category_name").value;
	var newCat_sname = _("cat_shortname").value;
	if(newCat_name == "" || newCat_sname == ""){
		_("statusMessage").innerHTML = "Please fill both Category name and shortname";
		return false;
	} 
	_("saveCatBtn").disabled = true;
	_("statusMessage").innerHTML = "Saving ...";
	var ajax = ajaxObj("POST", "assets/parsers/categories_system.php");
	console.log('Ajax request sent:'+ajax);
	ajax.onreadystatechange = function() {
		if(ajaxReturn(ajax) == true) {
			var datArray = ajax.responseText.split("|");
			if(datArray[0] == "saving_ok"){
				var newCat_id = datArray[1];
				var currentHTML = _("asset_categories").innerHTML;
				_("asset_categories").innerHTML = '<div class="card panel" id="category-'+newCat_id+'"><div class="card-head collapsed" data-toggle="collapse" data-parent="#asset_categories" data-target="#asset_category-'+newCat_id+'"><header id="catTitle'+newCat_id+'">'+newCat_name+'</header><div class="tools"><span data-toggle="tooltip" data-placement="top" data-original-title="Delete category"><a href="#DelCatModal" id="DelCatLink'+newCat_id+'" data-toggle="modal" data-id="'+newCat_id+'" class="open-DelCatModal btn btn-icon-toggle" data-catname="'+newCat_name+'"><i class="md md-delete"></i></a></span><span data-toggle="tooltip" data-placement="top" data-original-title="Edit category"><a  href="#EditCatModal" id="EditCatLink'+newCat_id+'" data-toggle="modal" data-id="'+newCat_id+'" class="open-EditCatModal btn btn-icon-toggle" data-catname="'+newCat_name+'"><i class="md md-edit"></i></a></span><span data-toggle="tooltip" data-placement="top" data-original-title="Add new sub-category"><a href="#newSubCatModal" id="newSubCatLink'+newCat_id+'" class="open-newSubCatModal btn btn-icon-toggle" data-toggle="modal" data-catid="'+newCat_id+'" data-catname="'+newCat_name+'"><i class="fa fa-plus-circle"></i></a></span><a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a></div></div><div id="asset_category-'+newCat_id+'" class="collapse"><div class="card-body"><div class="card-body no-padding"><ul class="list" id="subcategories_list'+newCat_id+'">No subcategories defined</ul></div></div></div></div>'+currentHTML;
				
				_("category_name").value = "";
				_("cat_shortname").value = "";
				_("saveCatBtn").disabled = false;
				_("statusMessage").innerHTML = "";
				$('#categoriesModal').modal('hide');
				console.log(ajax.responseText);
			} else {
				//alert(ajax.responseText);
				console.log(ajax.responseText);
				_("statusMessage").innerHTML = ajax.responseText;
				_("saveCatBtn").disabled = false;
			}
		}
	}
	ajax.send("action="+action+"&cat_name="+newCat_name+"&cat_sname="+newCat_sname);
}

//AJAX PROCESS TO EDIT CATEGORY
function editCategory(editCatID,editCatName){
	var action = "editCat";
	var editCat_ID = editCatID;
	var oldCat_Name = editCatName;
	var newCat_Name = _("newcategory_name").value;
	var catTitleBox = _("catTitle"+editCat_ID);
	var editCatMessage = _("editCatMessage");
	var editCat_Btn = _("editCatBtn"+editCat_ID);
	console.log(editCat_ID+','+oldCat_Name+','+newCat_Name+','+catTitleBox+','+editCatMessage+','+editCat_Btn);
	if(editCat_ID == ""){
		editCatMessage.innerHTML = "Category has not been well specified";
		return false;
	} else if (newCat_Name == ""){
		editCatMessage.innerHTML = "The category name can not be empty";
		return false;
	} else if (newCat_Name == oldCat_Name){
		editCatMessage.innerHTML = "You have not made any changes yet!";
		return false;
	} else {
		editCat_Btn.disabled = true;
		editCatMessage.innerHTML = "Saving ...";
		var ajax = ajaxObj("POST", "assets/parsers/categories_system.php");
		//console.log('Ajax request sent:'+ajax);
		ajax.onreadystatechange = function() {
			if(ajaxReturn(ajax) == true) {
				if(ajax.responseText == "saving_ok"){
					catTitleBox.innerHTML = newCat_Name;
					editCat_Btn.disabled = false;
					editCatMessage.innerHTML = "";
					$('#EditCatModal').modal('hide');
					
					_("DelCatLink"+editCat_ID).setAttribute('data-catname', newCat_Name);
					_("EditCatLink"+editCat_ID).setAttribute('data-catname', newCat_Name);
					
										
					console.log('Changed Category name from: '+oldCat_Name+' to :'+$("#EditCatLink").attr('data-catname'));
					console.log(ajax.responseText);
				} else {
					//alert(ajax.responseText);
					console.log(ajax.responseText);
					editCatMessage.innerHTML = ajax.responseText;
					editCat_Btn.disabled = false;
				}
			}
		}
		ajax.send("action="+action+"&cat_id="+editCat_ID+"&oldCatName="+oldCat_Name+"&newCatName="+newCat_Name);
	}
}

//AJAX PROCESS TO DELETE CATEGORY
function delCategory(id){
	var action = "delCat";
	var delCat_id = id;
	var eMessage = _("modalError");
	if(delCat_id == ""){
		eMessage.innerHTML = "Something went wrong!!! a category id was not specified";
		return false;
	}
	_("delCatBtn"+delCat_id).disabled = true;
	_("delCatBtn"+delCat_id).innerHTML = "Deleting ...";
	var ajax = ajaxObj("POST", "assets/parsers/categories_system.php");
	//console.log('Ajax request sent:'+ajax);
	ajax.onreadystatechange = function() {
		if(ajaxReturn(ajax) == true) {
			if(ajax.responseText == "delete_ok"){
				_("category-"+delCat_id).outerHTML = "";
				delete _("category-"+delCat_id);
				_("delCatBtn"+delCat_id).disabled = false;
				eMessage.innerHTML = "";
				$('#DelCatModal').modal('hide');
			} else {
				//alert(ajax.responseText);
				//console.log(ajax.responseText);
				eMessage.innerHTML = ajax.responseText;
				_("delCatBtn"+delCat_id).disabled = false;
				_("delCatBtn"+delCat_id).innerHTML = "Delete";
			}
		}
	}
	ajax.send("action="+action+"&cat_id="+delCat_id);
}

//AJAX PROCESS TO SAVE NEW SUB-CATEGORY
function saveSubCategory(catID){
	var action = "newSubCat";
	var cat_id = catID;
	var newSubCat_name = _("subcategory_name").value;
	var newSubCat_sname = _("subcat_shortname").value;
	var confirmBtn = _("newSubCatBtn"+cat_id);
	var statusMessage = _("newSubCatMessage");
	if(newSubCat_name == "" || newSubCat_sname == ""){
		_("statusMessage").innerHTML = "Please fill both Category name and shortname";
		return false;
	} 
	_("newSubCatBtn"+cat_id).disabled = true;
	statusMessage.innerHTML = "Saving ...";
	var ajax = ajaxObj("POST", "assets/parsers/categories_system.php");
	console.log('Ajax request sent:'+ajax);
	ajax.onreadystatechange = function() {
		if(ajaxReturn(ajax) == true) {
			var datArray = ajax.responseText.split("|");
			if(datArray[0] == "subcat_saved"){
				var newSubCat_id = datArray[1];
				var subcats_rows = _("subcategories_list"+cat_id).getElementsByTagName("li").length
				if (subcats_rows < 1) {
					var currentHTML = "";
				} else {
					var currentHTML = _("subcategories_list"+cat_id).innerHTML;
				}
				_("subcategories_list"+cat_id).innerHTML = currentHTML+'<li id="subcategory-'+newSubCat_id+'" class="tile"><a class="tile-content ink-reaction"><div id="subcatTitle_'+newSubCat_id+'" class="tile-text">'+newSubCat_name+'<small>'+newSubCat_sname+'</small></div></a><a id="DelSubCatLink'+newSubCat_id+'" class="open-DelSubCatModal btn btn-flat ink-reaction" href="#DelSubCatModal"  data-toggle="modal" data-catid="'+cat_id+'" data-subcatid="'+newSubCat_id+'" data-subcatname="'+newSubCat_name+'" data-subcatsname="'+newSubCat_sname+'"><i class="fa fa-trash"></i></a><a id="EditSubCatLink'+newSubCat_id+'" class="open-EditSubCatModal btn btn-flat ink-reaction" href="#EditSubCatModal"  data-toggle="modal" data-catid="'+cat_id+'" data-subcatid="'+newSubCat_id+'" data-subcatname="'+newSubCat_name+'" data-subcatsname="'+newSubCat_sname+'"><i class="fa fa-pencil"></i></a></li>';
				
				_("subcategory_name").value = "";
				_("subcat_shortname").value = "";
				_("newSubCatBtn"+cat_id).disabled = false;
				statusMessage.innerHTML = "";
				$('#newSubCatModal').modal('hide');
				console.log(ajax.responseText);
			} else {
				//alert(ajax.responseText);
				console.log(ajax.responseText);
				statusMessage.innerHTML = ajax.responseText;
				_("newSubCatBtn"+cat_id).disabled = false;
			}
		}
	}
	ajax.send("action="+action+"&subcat_name="+newSubCat_name+"&subcat_sname="+newSubCat_sname+"&cat_id="+cat_id);
}

//AJAX PROCESS TO EDIT SUB-CATEGORY
function editSubCategory(catID,scatID,scatName,scatsName){
	var action = "editSubCat";
	var catID = catID;
	var scatID = scatID;
	var oldSubCat_Name = scatName;
	var oldSubCat_sName = scatsName;
	var newSubCat_Name = _("newsubcat_name").value;
	var newSubCat_sName = _("newsubcat_sname").value;
	var subcatTitleBox = _("subcatTitle_"+scatID);
	var statusMessage = _("editSubCatMessage");
	var confirmBtn = _("editSubCatBtn"+scatID);
	console.log(catID+','+scatID+','+oldSubCat_Name+','+oldSubCat_sName+','+newSubCat_Name+','+newSubCat_sName);
	if(catID == "" || scatID == ""){
		statusMessage.innerHTML = "Sub-Category has not been well specified";
		return false;
	} else if (newSubCat_Name == ""){
		statusMessage.innerHTML = "The sub-category name can not be empty";
		return false;
	} else if (newSubCat_sName == ""){
		statusMessage.innerHTML = "The sub-category shortname can not be empty";
		return false;
	} else if (oldSubCat_Name == newSubCat_Name && oldSubCat_sName == newSubCat_sName){
		statusMessage.innerHTML = "You have not made any changes yet!";
		return false;
	} else {
		confirmBtn.disabled = true;
		statusMessage.innerHTML = "Saving ...";
		var ajax = ajaxObj("POST", "assets/parsers/categories_system.php");
		//console.log('Ajax request sent:'+ajax);
		ajax.onreadystatechange = function() {
			if(ajaxReturn(ajax) == true) {
				if(ajax.responseText == "edit_subcat_ok"){
					subcatTitleBox.innerHTML = newSubCat_Name+'<small>'+newSubCat_sName+'</small>';
					confirmBtn.disabled = false;
					statusMessage.innerHTML = "";
					$('#EditSubCatModal').modal('hide');

					_("EditSubCatLink"+scatID).setAttribute('data-subcatname', newSubCat_Name);
					_("EditSubCatLink"+scatID).setAttribute('data-subcatsname', newSubCat_sName);
					//_("EditCatLink"+editCat_ID).setAttribute('data-catname', newCat_Name);
										
					console.log('Changed Sub-Category name from: '+oldSubCat_Name+' to :'+$("#EditSubCatLink"+scatID).attr('data-subcatname'));
					console.log('Changed Sub-Category shortname from: '+oldSubCat_sName+' to :'+$("#EditSubCatLink"+scatID).attr('data-subcatsname'));
					console.log(ajax.responseText);
				} else {
					//alert(ajax.responseText);
					console.log(ajax.responseText);
					statusMessage.innerHTML = ajax.responseText;
					confirmBtn.disabled = false;
				}
			}
		}
		ajax.send("action="+action+"&cat_id="+catID+"&subcat_id="+scatID+"&oldSubCat_Name="+oldSubCat_Name+"&oldSubCat_sName="+oldSubCat_sName+"&newSubCat_Name="+newSubCat_Name+"&newSubCat_sName="+newSubCat_sName);
	}
}

//AJAX PROCESS TO DELETE SUB-CATEGORY
function delSubCategory(catid,subcatid){
	var action = "delSubCat";
	var catid = catid;
	var subcatid = subcatid;
	console.log('Category id : '+catid+' , Sub Category id : '+subcatid);
	var confirmBtn = _("delSubCatBtn"+subcatid);
	var statusMessage = _("delSubCatMessage");
	if(catid == ""){
		statusMessage.innerHTML = "Something went wrong!!! a category id was not specified";
		return false;
	} else if(subcatid == ""){
		statusMessage.innerHTML = "Something went wrong!!! a sub-category id was not specified";
		return false;
	}
	confirmBtn.disabled = true;
	confirmBtn.innerHTML = "Deleting ...";
	var ajax = ajaxObj("POST", "assets/parsers/categories_system.php");
	//console.log('Ajax request sent:'+ajax);
	ajax.onreadystatechange = function() {
		if(ajaxReturn(ajax) == true) {
			if(ajax.responseText == "subcat_deleted"){
				// find and remove the item in the page
				_("subcategory-"+subcatid).outerHTML = "";
				delete _("subcategory-"+subcatid);
				//var elem = document.getElementById('subcategory-'+delSubCat_id);
				//elem.parentNode.removeChild(elem);
				confirmBtn.disabled = false;
				statusMessage.innerHTML = "";
				$('#DelSubCatModal').modal('hide');
			} else {
				//alert(ajax.responseText);
				//console.log(ajax.responseText);
				statusMessage.innerHTML = ajax.responseText;
				confirmBtn.disabled = false;
				confirmBtn.innerHTML = "Delete";
			}
		}
	}
	ajax.send("action="+action+"&cat_id="+catid+"&subcat_id="+subcatid);
}

</script>
<?php echo $new_category_ui; ?>
<div class="panel-group" id="asset_categories">
	<?php echo $asset_categories; ?>
</div><!--end .panel-group -->

<!-- BEGIN NEW CATEGORY MODAL MARKUP -->
<div class="modal fade" id="categoriesModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">New item category ...</h4>
            </div>
            <form id="loginform" class="form-horizontal" role="form" data-toggle="validator" onSubmit="return false;" accept-charset="utf-8"> 
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="category_name" class="control-label">Category name *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="category_name" id="category_name" type="text" class="form-control" onfocus="emptyElement('statusMessage')" placeholder="new category name" data-rule-minlength="3" maxlength="20" required>
                            <p class="help-block">3 to 20 characters</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="cat_shortname" class="control-label">Category shortname *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="cat_shortname" id="cat_shortname" type="text" class="form-control" onkeyup="omitChars('cat_shortname','letters_underscore_i')" onfocus="emptyElement('statusMessage')" placeholder="new shortname" data-rule-minlength="3" maxlength="16" required>
                            <p class="help-block">only letters and underscore</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                        </div>
                        <p></p>
                        <p style="text-align:center; color:red" id="statusMessage"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" id="saveCatBtn" onClick="saveCategory()" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END NEW CATEGORY MODAL MARKUP -->

<!-- BEGIN EDIT CATEGORY MODAL MARKUP -->
<div class="modal fade" id="EditCatModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="editModalHeader" class="modal-title">Edit inventory category ...</h4>
            </div>
            <form id="editCatForm" class="form-horizontal" role="form" data-toggle="validator" onSubmit="return false;" accept-charset="utf-8"> 
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="newcategory_name" class="control-label">Category name *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="newcategory_name" id="newcategory_name" type="text" class="form-control" onfocus="emptyElement('editCatMessage')" placeholder="new category name" data-rule-minlength="3" maxlength="20" required>
                            <p class="help-block">3 to 20 characters</p>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-3">
                        </div>
                        <p></p>
                        <p style="text-align:center; color:red" id="editCatMessage"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <span id="confirmBtn">
                    	<button type="submit" class="btn btn-primary">Save Changes</button>
                    </span>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END EDIT CATEGORY MODAL MARKUP -->

<!-- BEGIN DELETE CATEGORY MODAL MARKUP -->
<div id="DelCatModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 id="modalHeader" class="modal-title text-danger">Warning!</h4>

            </div>
            <div class="modal-body">
                <p id="modalContent">...</p>
                <p id="modalError" style="color:red"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <span id="confirmBtn">
                	<button type="button" class="btn btn-danger">Confirm</button>
                </span>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END DELETE CATEGORY MODAL MARKUP -->

<!-- BEGIN NEW SUB-CATEGORY MODAL MARKUP -->
<div class="modal fade" id="newSubCatModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">New item sub-category ...</h4>
            </div>
            <form id="newSubCatForm" class="form-horizontal" role="form" data-toggle="validator" onSubmit="return false;" accept-charset="utf-8"> 
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="subcategory_name" class="control-label">Sub-Category name *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="subcategory_name" id="subcategory_name" type="text" class="form-control" onfocus="emptyElement('newSubCatMessage')" placeholder="new sub-category name" data-rule-minlength="3" maxlength="20" required>
                            <p class="help-block">3 to 20 characters</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="subcat_shortname" class="control-label">Sub-Category shortname *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="subcat_shortname" id="subcat_shortname" type="text" class="form-control" onfocus="emptyElement('newSubCatMessage')" onkeyup="omitChars('subcat_shortname','letters_underscore_i')" placeholder="new sub-category shortname" data-rule-minlength="3" maxlength="16" required>
                            <p class="help-block">only letters and underscore</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                        </div>
                        <p></p>
                        <p style="text-align:center; color:red" id="newSubCatMessage"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <span id="confirmBtn">
                    	<button type="submit" id="newSubCatBtn" onClick="saveSubCategory()" class="btn btn-primary">Save</button>
                    </span>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END NEW SUB-CATEGORY MODAL MARKUP -->

<!-- BEGIN EDIT SUB-CATEGORY MODAL MARKUP -->
<div class="modal fade" id="EditSubCatModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="editModalHeader" class="modal-title">Edit inventory sub-category ...</h4>
            </div>
            <form id="editSubCatForm" class="form-horizontal" role="form" data-toggle="validator" onSubmit="return false;" accept-charset="utf-8"> 
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="newsubcat_name" class="control-label">Sub-category name *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="newsubcat_name" id="newsubcat_name" type="text" class="form-control" onfocus="emptyElement('editSubCatMessage')" placeholder="new sub-category name" data-rule-minlength="3" maxlength="20" required>
                            <p class="help-block">3 to 20 characters</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="newsubcat_sname" class="control-label">Sub-category shortname *</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="newsubcat_sname" id="newsubcat_sname" type="text" class="form-control" onfocus="emptyElement('editSubCatMessage')" onkeyup="omitChars('newsubcat_sname','letters_underscore_i')" placeholder="new sub-category short-name" data-rule-minlength="3" maxlength="16" required>
                            <p class="help-block">3 to 16 characters</p>
                        </div>
                    </div>
                    
                    <div id="the-basics">
                      <input class="typeahead" type="text" placeholder="States of USA">
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-3">
                        </div>
                        <p></p>
                        <p style="text-align:center; color:red" id="editSubCatMessage"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <span id="confirmBtn">
                    	<button type="submit" class="btn btn-primary">Save Changes</button>
                    </span>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END EDIT SUB-CATEGORY MODAL MARKUP -->

<!-- BEGIN DELETE SUB-CATEGORY MODAL MARKUP -->
<div id="DelSubCatModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 id="modalHeader" class="modal-title text-danger">Warning!</h4>

            </div>
            <div class="modal-body">
                <p id="modalContent">...</p>
                <p id="delSubCatMessage" style="color:red"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <span id="confirmBtn">
                	<button type="button" class="btn btn-danger">Confirm</button>
                </span>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> <!-- /.modal -->
<!-- END DELETE SUB-CATEGORY MODAL MARKUP -->