(function(namespace, $) {
    "use strict";

    var DataTableComponents = function() {
        // Create reference to this instance
        var o = this;
        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });

    };
    var p = DataTableComponents.prototype;

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        this._initDataTables();
    };

    // =========================================================================
    // DATATABLES
    // =========================================================================

    p._initDataTables = function() {
        if (!$.isFunction($.fn.dataTable)) {
            return;
        }

        // Init the demo DataTables
        this._createassetList();
    };

    p._createassetList = function() {
        $('#assetList').DataTable({
            // responsive: true,
            "autoWidth": true,
            "dom": 'lCfrtip',
            "order": [],
            buttons: [{
                extend: "copy",
                className: "btn-sm"
            }, {
                extend: "csv",
                className: "btn-sm"
            }, {
                extend: "excel",
                className: "btn-sm"
            }, {
                extend: "pdf",
                className: "btn-sm"
            }, {
                extend: "print",
                className: "btn-sm"
            }],
            "colVis": {
                "buttonText": "Select columns",
                /*"overlayFade": 0,*/
                "align": "right",
                "aiExclude": [ 0 ],
                "bRestore": true,
                "sRestore": "Restore defaults",
                "bVisible": 15
            },
            "language": {
                "lengthMenu": '_MENU_ assets per page',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#assetList tbody').on('click', 'tr', function() {
            var assetid = $(this).data('id');
            var asseturl = assetListPath + assetid;
            //$(this).toggleClass('selected'); //to highlight selected table row
            window.location = asseturl;
        });
    };
    // =========================================================================
    namespace.DataTableComponents = new DataTableComponents;
}(window.materialadmin, jQuery)); // pass in (namespace, jQuery):