import $ from 'jquery';
import Swal from "sweetalert2";
import typeahead from 'typeahead.js';

// import './components/checkin_checkout'; Will be loaded asynchronously when assign button is clicked

(function (namespace, $) {
    "use strict";

    var CheckinCheckout = function () {
        // Create reference to this instance
        var o = this;
        // Initialize app when document is ready
        $(document).ready(function () {
            o.initialize();
        });

    };
    var p = CheckinCheckout.prototype;

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function () {
        this._enableEvents();
        // this._initTooltips();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    // events
    p._enableEvents = function () {
        var o = this;
        var $itemCard = $('#show_item_card');

        // Listen for the checkin/checkout link click
        $('.tools #assignItemLink').on('click', function (e) {
            o._handleCheckinCheckoutButtonClick(e);
        });
        //listen for checkout ajax call button
        var $assignModalWrapper = $('.js-checkout-modal-wrapper');
        $assignModalWrapper.on('click', 'form .js-checkout-btn', function (e) {
            o._handlecheckoutButtonClick(e);
        });
        $assignModalWrapper.on('click', 'form .js-checkin-btn', function (e) {
            o._handlecheckinButtonClick(e);
        });
        //listen for asset delete link ckick
        $('.tools .js-asset-delete-link').on('click', function (e) {
            o._handleAssetDeleteLinkClick(e);
        });
        //Listen to load history button click
        $itemCard.on("click", '.js-load-history-btn',function(e){
            o._handleLoadHistoryButtonClick(e);
        });
        //Listen to link assets btn click
        $itemCard.on("click", '.js-link-assets-btn',function(e){
            o._handleLinkAssetButtonClick(e);
        });
        //Listen to unlink assets btn click
        $itemCard.on("click", '.js-unlink-asset-btn',function(e){
            o._handleUnLinkAssetButtonClick(e);
        });
        //Listen to tiled asset click to link
        $('#offcanvas-abstract').on("click", '.tile',function(e){
            //o._handleLoadHistoryButtonClick(e);
            o._handleAssetTileClickedToLink(e);
        });
        //Listen to offcanvas search input 'onkeyup event' to filter listed assets
        $('#offcanvas-abstract').on("keyup", '.offSearchQuery',function(e){
            o._handleListedAssetsFilter(e);
        });

    };

    // =========================================================================
    // Handle Checkin/Checkout Link click
    // =========================================================================

    p._handleCheckinCheckoutButtonClick = function (e) {
        e.preventDefault();

        var modalUrl;
        var $assignLinkBtn = $(e.currentTarget);
        if($assignLinkBtn.hasClass('js-asset-isassigned')){
            modalUrl = $assignLinkBtn.data('checkinurl');
        } else {
            modalUrl = $(e.currentTarget).data('checkouturl');
        }

        Swal.fire({
            title: 'Loading',
            html: 'Please wait...',
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading()
                $.ajax({
                    url: modalUrl,
                    method: 'POST'
                }).then(
                    function (data) {
                        $('.js-checkout-modal-wrapper').html(data);
                        p._importCheckoutFilesAsync();
                        Swal.close();
                    }
                );

            }
        }).then((result) => {
            if (result.dismiss) {
                console.log('I was dismissed')
            }
        })

    };

    p._importCheckoutFilesAsync = function () {
        import('./components/checkin_checkout').then(
            function () {
                p._initSelect2();
                p._initInputMask(); //needed by date picker
                p._initDatePicker();
                p._initDatetime();
            }
        ).then(
            function () {
                $('.js-assignItemModal').modal('show')
            }
        );
    }

    p._handlecheckoutButtonClick = function (e) {
        e.preventDefault();

        var $clickedCheckoutBtn = $(e.currentTarget);
        var $checkoutForm = $clickedCheckoutBtn.closest('form');
        var checkoutUrl = $checkoutForm.data('formurl');

        $clickedCheckoutBtn.addClass('disabled');

        $.ajax({
            url: checkoutUrl,
            method: 'POST',
            data: $checkoutForm.serialize(),
            success: function (data) {
                //toastr.success('Item checked out successfully', '');
                $('.js-assignItemModal').modal('hide');
                $('.tools #assignItemLink').addClass('js-asset-isassigned').closest('span').attr('data-original-title', 'Check in this item');;
                $('.tools #assignItemLink i').removeClass('fa-user-plus').addClass('fa-reply');
                var assigneeDetailsHtml = `
                    <div class="col-md-12">
                        <div class="sq-cell">
                            <label class="control-label">Assigned To : </label>&nbsp;<span>${ data.details.assignee  }</span>
                        </div>
                    </div><!--end .col -->
                    <div class="col-md-6">
                        <div class="sq-cell">
                            <div class="">
                                <label class="control-label">Check-out date</label><br>
                                <span>${ data.details.checkout_date }</span>
                            </div>
                        </div>
    
                    </div><!--end .col -->
                    <div class="col-md-6">
                        <div class="sq-cell">
                            <label class="control-label">Due Date</label><br>
                            <span>${ data.details.due_date }</span>
                        </div><!--end .form-group -->
                    </div><!--end .col -->
                    <div class="col-md-12">
                        <div class="opacity-50 text-center">Checkin Due</div>
                        <div class="text-center js-due-date-formatted">${ data.details.days_diff }</div>
                    </div><!--end .col -->
                `
                $('.js-assignee-details-wrapper').html(assigneeDetailsHtml);

            },
            error: function (jqXHR) {
                let errorData = JSON.parse(jqXHR.responseText);
                //toastr.error('failed to checkout an item, please retry', '');

                console.log(errorData);
                var printErrors = 'Sorry, there was a problem checking out this item, please retry';
                if(errorData.status === 'failed') {
                    printErrors = errorData.message;
                }
                $("#assignItemMessage").innerHTML = printErrors;
                p._resetConfirmBtn($clickedCheckoutBtn, 'Check out');
            }

        });
    }

    p._handlecheckinButtonClick = function (e) {
        e.preventDefault();

        var $clickedCheckonBtn = $(e.currentTarget);
        var $checkinForm = $clickedCheckonBtn.closest('form');
        var checkinUrl = $checkinForm.data('formurl');

        $clickedCheckonBtn.addClass('disabled');

        $.ajax({
            url: checkinUrl,
            method: 'POST',
            data: $checkinForm.serialize(),
            success: function (data) {
                //toastr.success('Item checked in successfully', '');
                $('.js-assignItemModal').modal('hide');
                $('.tools #assignItemLink').removeClass('js-asset-isassigned').closest('span').attr('data-original-title', 'Assign (Check-out)');
                $('.tools #assignItemLink i').removeClass('fa-reply').addClass('fa-user-plus');
                var assigneeDetailsHtml = `
                <div class="col-md-12 sq-col-nobdr-b">
                    <div class="sq-cell">
                        <label class="control-label">Item Not Assigned</label>
                    </div>
                </div><!--end .col -->
                `;
                $('.js-assignee-details-wrapper').html(assigneeDetailsHtml);
                $('.js-asset-status').html(data.details.status);
                $('.js-asset-conditionnotes').attr('data-original-title', data.details.conditionNotes == "" ? 'Not specified' : data.details.conditionNotes );
            },
            error: function (jqXHR) {
                let errorData = JSON.parse(jqXHR.responseText);
                //toastr.error('failed to check in an item, please retry', '');

                console.log(errorData);
                var printErrors = 'Sorry, there was a problem checking out this item, please retry';
                if(errorData.status === 'failed') {
                    printErrors = errorData.message;
                }
                $("#assignItemMessage").innerHTML = printErrors;
                p._resetConfirmBtn($clickedCheckoutBtn, 'Check out');
            }
        });
    }

    p._resetConfirmBtn = function resetConfirmBtn(btn, defaultBtnText = 'Confirm') {
        btn.disabled = false;
        btn.html(defaultBtnText);
    }

    p._handleLoadHistoryButtonClick = function (e) {
        var $loadHistoryBtn = $(e.currentTarget);
        $loadHistoryBtn.attr("disabled", true);
        $loadHistoryBtn.html('loading asset history ...');
        $.ajax({
            url:        $loadHistoryBtn.data('history-path'),
            type:       'POST',
            dataType:   'json',
            async:      true,

            success: function(json) {
                //console.log(json.histories.length);
                if (json.histories.length > 0) {
                    $loadHistoryBtn.hide();
                    $('#history-container').html('');
                    $('#history-container').append('<br/><hr class="no-margin"/><h2 class="text-light text-center">Item History</h2>' +
                        '<form class="form" id="formFilter" accept-charset="utf-8" method="post"><br/><div class="text-center"><label class="checkbox-inline checkbox-styled checkbox-info activityFilters">\n' +
                        '<input type="checkbox" id="filter_system" value="filter_system" checked><span>System alerts</span></label><label class="checkbox-inline checkbox-styled checkbox-primary activityFilters">\n' +
                        '<input type="checkbox" id="filter_assignees" value="filter_assignees" checked><span>Assignees</span></label><label class="checkbox-inline checkbox-styled checkbox-default-dark activityFilters">\n' +
                        '<input type="checkbox" id="filter_reviews" value="filter_reviews" checked><span>Reviews</span></label></div><br/></form><hr class="no-margin"/>');
                    $('#history-container').append('<ul id="feeds-list" class="timeline collapse-md"><ul>');
                    $.each(json.histories, function (idx, history) {
                        $('#feeds-list').append('<li class="item-feed ' + history.update_type + '"><div class="timeline-circ circ-lg"><span class="' + history.update_icon + '"></span></div><div class="timeline-entry"><div class="card style-default-light"><div class="card-body small-padding"><span class="update_content"><span class="opacity-50">' + history.update_date + '</span>&nbsp; by <a class="text-primary" href="#">' + history.update_user + '</a><br/><span>' + history.update_notes + '</span></span></div></div></div></li>');
                    });
                } else {
                    $loadHistoryBtn.hide();
                    $loadHistoryBtn.html('<div class="text-center "><span class="text-danger text-medium">No history found for this item</span></div>');
                }
            },
            error : function(xhr, textStatus, errorThrown) {
                $loadHistoryBtn.html('Load asset history');
                $loadHistoryBtn.attr("disabled", false);
                alert('Loading asset history failed due to Ajax error.');
            }
        });

    }

    // =========================================================================
    // Handle asset DELETE click
    // =========================================================================
    p._handleAssetDeleteLinkClick = function(e) {
        e.preventDefault();
        Swal.fire({
            icon: "warning",
            title: "Are you sure you want to delete this asset and all it's records from the system?",
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: '#d14529',
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                var $deleteBtn = $(e.target);
                var $form = $deleteBtn.closest('form');
                var deleteUrl = $deleteBtn.data('deleteurl');
                return $.ajax({
                    url: deleteUrl,
                    type: 'POST',
                    data: $form.serialize(),
                }).then(response => {
                    //Show success toastr message
                    //toastr.success('Asset deleted successfully', '');
                    Swal.fire({
                        icon: 'success',
                        title: 'Asset deleted successfully',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(
                        function () {
                            window.location.href = "/asset/";
                        }
                    )
                })
                .fail(
                    function(jqXHR,textStatus,errorThrown ){
                        let errorData = JSON.parse(jqXHR.responseText);
                        //toastr.error('failed to delete Asset, please retry', '');

                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        if(errorData.status === 'failed') {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oooops!!!',
                            })
                            Swal.showValidationMessage(
                                `An error occurred: ${errorData.message}`
                            )
                        }
                    }
                )
            },
            allowOutsideClick: true
        }).then((result) => {
            if (result.value) {
                Swal({
                    type: 'success',
                    title: 'Success',
                    text: result.value
                })
            }
        }).catch(
            function (cancelEvent) {
                console.log('cancelled delete operation, error is ' + cancelEvent);
            }
        )
    }

    // =========================================================================
    // Handle asset LINKING clicks
    // =========================================================================

    p._handleLinkAssetButtonClick = function(e) {
        materialadmin.AppOffcanvas.openAbstractOffcanvas("Choose asset to link");


        var searchPath, listPath, linkPath, unLinkPath, $canvasBox, $headerText, $offCanvasBody;
        searchPath = $(e.currentTarget).data('search-path');
        listPath = $(e.currentTarget).data('list-path');
        linkPath = $(e.currentTarget).data('link-path');
        unLinkPath = $(e.currentTarget).data('unlink-path');
        $canvasBox = $('#offcanvas-abstract');
        $headerText = $canvasBox.find('.off-canvas-header').html();
        $offCanvasBody = $canvasBox.find('.offcanvas-body');

        //Add the Loading animation
        materialadmin.AppCard.addCardLoader($canvasBox);

        $.ajax({
            url: listPath,
            method: 'POST',
            data: null,
            success: function (data) {
                let html = '<ul class="list">';
                $.each(data.assets, (key, asset) => {
                    html += `
						<li class="tile ink-reaction">
							<a class="tile-content ink-reaction asset-anchor" data-asset='${JSON.stringify(asset).replace(/'/g, "\\'")}' data-asset-id="${asset.id}" data-asset-name="${asset.name}" data-link-path="${linkPath}" data-unlink-path="${unLinkPath}" href="/asset/${asset.id}" target="_blank" onclick="event.preventDefault();">
								<div class="tile-icon">
									<img src="${asset.photo !== null && asset.photo !== '' ? '/uploads/asset/' + asset.photo : noImage2path}" alt="" />
								</div>
								<div class="tile-text relative-positioned">
									${asset.name}
									<small><i class="fa fa-sitemap"></i> : ${asset.category.parent.name} :: ${asset.category.name}</small>
									<small><i class="fa fa-qrcode"></i> : ${asset.barcode}, Sn : ${asset.sn}</small>
									<button class="tile-button btn btn-icon-toggle ink-reaction"><i class="fa fa-fw fa-link"></i></button>
								</div>
							</a>
						</li>
						`
                });
                html += '</ul>';
                $canvasBox.find('form.offcanvas-search').removeClass('hidden');
                $offCanvasBody.html(html);

                //Remove the Loading animation
                materialadmin.AppCard.removeCardLoader($canvasBox);
                //Refresh the scrollbar. This simply re-calculates the position and height of the scrollbar gadget.
                $(".nano").nanoScroller();
            },
            error: function (jqXHR) {
                $offCanvasBody.html('<p class="text-danger text-sm">There was an error listing assets, please refresh</p>');
                //$("#offcanvasSearchForm").removeClass('hidden');
            }
        })



    }

    p._handleAssetTileClickedToLink = function(e) {
        e.preventDefault();
        var $clickedTile, $clickedTileAnchor, assetId, assetName, linkPath, unLinkPath, assetObj, $linkedAssetsContainer;
        $clickedTile = $(e.currentTarget);
        $clickedTileAnchor = $clickedTile.find('.tile-content');
        assetId = $clickedTileAnchor.data('asset-id');
        assetName = $clickedTileAnchor.data('asset-name');
        linkPath = $clickedTileAnchor.data('link-path');
        unLinkPath = $clickedTileAnchor.data('unlink-path');
        assetObj = $clickedTileAnchor.data('asset');
        $linkedAssetsContainer = $('#show_item_card .linked-assets-card ul.list');

        Swal.fire({
            icon: "warning",
            confirmButtonText:
                '<i class="fa fa-link"></i> Yes, link it!',
            html:
                '<p class="text-xl">Are you sure you want to link ' +
                '<a href="/asset/' + assetId + '" class="text-primary" target="_blank">' + assetName + '</a> ' +
                'with this asset?</p>',
            confirmButtonColor: '#0aa89e',
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return $.ajax({
                    url: linkPath,
                    type: 'POST',
                    data: {asset_to_link: assetId},
                }).then(response => {
                    //Show success toastr message
                    //toastr.success('Asset deleted successfully', '');
                    Swal.fire({
                        icon: 'success',
                        title: 'Asset linked successfully',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(
                        function () {
                            // window.location.href = "/asset/";
                            // window.location.reload();
                            var assetTileHtml;
                            assetTileHtml = `
                                    <li class="tile">
                                        <a class="tile-content ink-reaction" href="/asset/${assetObj.id}" target="_blank">
                                            <div class="tile-icon">
                                                ${assetObj.photo !== null && assetObj.photo !== '' ? '<img src="/uploads/asset/thumbs/' + assetObj.photo + '" alt="">' : '<img src="' + noImage2path + '" alt="">'}
                                            </div>
                                            <div class="tile-text">
                                                ${assetObj.name}
                                                <small>Barcode : ${assetObj.barcode} | Sn : ${assetObj.sn} </small>
                                            </div>
                                        </a>
                                        <a class="btn btn-flat ink-reaction js-unlink-asset-btn" data-unlink-id="${ assetObj.id }" data-unlink-path="${unLinkPath}">
                                            <i class="fa fa-unlink"></i>
                                        </a>
                                    </li>
                            `;
                            $linkedAssetsContainer.append(assetTileHtml);

                            $('.js-linked-assets-header').html(p._refreshLinkedAssets($linkedAssetsContainer));

                            //Refresh the scrollbar. This simply re-calculates the position and height of the scrollbar gadget.
                            $('.nano').nanoScroller();
                            //Scroll to the last added linked asset
                            $('.linked-assets-card .nano').nanoScroller({ scroll: 'bottom' });
                            $linkedAssetsContainer.closest('.nano').show();
                            materialadmin.AppOffcanvas.closeAbstractOffcanvas();
                        }
                    )
                })
                    .fail(
                        function(jqXHR,textStatus,errorThrown ){
                            let errorData = JSON.parse(jqXHR.responseText);
                            //toastr.error('failed to delete Asset, please retry', '');

                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            if(errorData.status === 'failed') {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oooops!!!',
                                })
                                Swal.showValidationMessage(
                                    `An error occurred: ${errorData.message}`
                                )
                            }
                        }
                    )
            },
            allowOutsideClick: true
        }).then((result) => {
            if (result.value) {
                Swal({
                    type: 'success',
                    title: 'Success',
                    text: result.value
                })
            }
        }).catch(
            function (cancelEvent) {
                console.log('cancelled delete operation, error is ' + cancelEvent);
            }
        )
    }

    p._handleUnLinkAssetButtonClick = function(e) {
        var $clickedBtn, $clickedTileContainer, $tileListUl, assetId, unLinkPath;
        $clickedBtn = $(e.currentTarget);
        $clickedTileContainer = $clickedBtn.closest('.tile');
        $tileListUl = $clickedBtn.closest('ul.list');

        assetId = $clickedBtn.data('unlink-id');
        unLinkPath = $clickedBtn.data('unlink-path');
        Swal.fire({
            icon: "warning",
            title: "Are you sure you want to unlink this asset?",
            confirmButtonText: '<i class="fa fa-link"></i> Yes, unlink it!',
            confirmButtonColor: '#0aa89e',
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return $.ajax({
                    url: unLinkPath,
                    type: 'POST',
                    data: {asset_to_unlink: assetId},
                }).then(response => {
                    //Show success toastr message
                    //toastr.success('Asset deleted successfully', '');
                    Swal.fire({
                        icon: 'success',
                        title: 'Asset unlinked successfully',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(
                        function () {
                            $clickedTileContainer.hide( "fast", function() {
                                $clickedTileContainer.remove();
                                $('.js-linked-assets-header').html(p._refreshLinkedAssets($tileListUl));
                            });

                            //Refresh the scrollbar. This simply re-calculates the position and height of the scrollbar gadget.
                            $(".nano").nanoScroller();
                        }
                    )
                })
                    .fail(
                        function(jqXHR,textStatus,errorThrown ){
                            let errorData = JSON.parse(jqXHR.responseText);
                            //toastr.error('failed to delete Asset, please retry', '');

                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            if(errorData.status === 'failed') {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oooops!!!',
                                })
                                Swal.showValidationMessage(
                                    `An error occurred: ${errorData.message}`
                                )
                            }
                        }
                    )
            },
            allowOutsideClick: true
        }).then((result) => {
            if (result.value) {
                Swal({
                    type: 'success',
                    title: 'Success',
                    text: result.value
                })
            }
        }).catch(
            function (cancelEvent) {
                console.log('cancelled delete operation, error is ' + cancelEvent);
            }
        )
    }

    p._countListedItems = function ($ul) {
        var liCount = 0;
        $ul.find('li').each(function () {
            liCount ++;
        })
        return liCount;
    }

    p._calculateListHeights = function ($ul) {
        var liHeights = 0;
        $ul.find('li').each(function () {
            liHeights += $(this).height();
        })
        return liHeights;
    }

    p._refreshLinkedAssets = function($ul) {
        var $linkedAssetsHeader, assetsCount, linkedAssets, sentence, $linkedAssetsCard, $liHeights;
        $linkedAssetsHeader =  $('.js-linked-assets-header');
        $linkedAssetsCard = $('.linked-assets-card');
        $liHeights = p._calculateListHeights($ul);

        if($liHeights < 300) {
            $linkedAssetsCard.find('.nano').height($liHeights);
        }

        //Count assets
        assetsCount = p._countListedItems($ul);
        linkedAssets = assetsCount < 1 ? 'No' : assetsCount;
        sentence = assetsCount === 1 ? ' linked asset' : ' linked assets';
        return linkedAssets + sentence;
    }

    p._handleListedAssetsFilter = function(e) {
        // Declare variables
        var $container, $input, filter, $ul, $li, assetLi, assetAnchor, txtValue;
        $container = $('#offcanvas-abstract');
        $input = $(e.currentTarget);

        filter = $input.val().toUpperCase();
        $ul = $('#offcanvas-abstract ul.list');

        // Loop through all list items, and hide those who don't match the search query
        $ul.find('li').each(
            function () {
                assetLi = $(this);
                assetAnchor = assetLi.find('.asset-anchor');
                txtValue = JSON.stringify(assetAnchor.data('asset'));

                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    assetLi.css('display', '');
                } else {
                    assetLi.fadeOut();
                }
            }
        );
    }


    // =========================================================================
    // SELECT2
    // =========================================================================
    p._initSelect2 = function () {
        if (!$.isFunction($.fn.select2)) {
            return;
        }

        // added this to fix the select2 width issue when put inside bootstrap modal
        $.fn.select2.defaults.set( "width", "100%" );// added this to fix the select2 width issue when put inside bootstrap modal

        $(".select2-users").select2({
            allowClear: true,
        });

        $(".select2-locations").select2({
            allowClear: true,
            placeholder :'Select a location',
            tags: true
        });

    };

    // =========================================================================
    // InputMask
    // =========================================================================
    p._initInputMask = function () {
        if (!$.isFunction($.fn.inputmask)) {
            return;
        }
        $(":input").inputmask();
    };

    // =========================================================================
    // Date Picker
    // =========================================================================
    p._initDatePicker = function () {
        if (!$.isFunction($.fn.datepicker)) {
            return;
        }
        $('#assignDateRange').datepicker({autoclose: true, todayHighlight: true, format: "yyyy-mm-dd"}); //Item assigning date range
        $('#demo-date-inline').datepicker({todayHighlight: true});
    };

    // =========================================================================
    // DATETIME
    // =========================================================================
    p._initDatetime = function() {
        if (!$.isFunction($.fn.datepicker)) {
            return;
        }
        //$('#purchasedatediv').datepicker({todayHighlight: true});
        $('#purchasedatediv').datepicker({autoclose: true, todayHighlight: true, format: "yyyy-mm-dd"});
        $('#checkindatediv').datepicker({autoclose: true, todayHighlight: true, format: "yyyy-mm-dd"});
        $('#reviewdatediv').datepicker({autoclose: true, todayHighlight: true, format: "yyyy-mm-dd"});
    };

    // // =========================================================================
    // // TOOLTIPS
    // // =========================================================================
    //
    // p._initTooltips = function () {
    //     if (!$.isFunction($.fn.tooltip)) {
    //         return;
    //     }
    //     $('[data-toggle="tooltip"]').tooltip({container: 'body'});
    // };


    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.CheckinCheckout = new CheckinCheckout;
}(window.materialadmin, $)); // pass in (namespace, jQuery):




// Activity hide filters
// $(document).ready(
//     function hideActivity(){
//         $("#filter_system").click(function(){
//             $(".new_item").slideToggle("fast");
//         });
//         $("#filter_assignees").click(function(){
//             $(".class_b").slideToggle("fast");
//         });
//         $("#filter_reviews").click(function(){
//             $(".class_c").slideToggle("fast");
//         });
//     }
// );

//Typeahead test code
// var engine, remoteHost, template, empty;
//
// $.support.cors = true;
//
// remoteHost = 'https://typeahead-js-twitter-api-proxy.herokuapp.com';
// template = Handlebars.compile($("#result-template").html());
// empty = Handlebars.compile($("#empty-template").html());
//
// engine = new Bloodhound({
//     identify: function(o) { return o.id_str; },
//     queryTokenizer: Bloodhound.tokenizers.whitespace,
//     datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name', 'screen_name'),
//     dupDetector: function(a, b) { return a.id_str === b.id_str; },
//     prefetch: remoteHost + '/demo/prefetch',
//     remote: {
//         url: remoteHost + '/demo/search?q=%QUERY',
//         wildcard: '%QUERY'
//     }
// });
//
// // ensure default users are read on initialization
// engine.get('1090217586', '58502284', '10273252', '24477185')
//
// function engineWithDefaults(q, sync, async) {
//     if (q === '') {
//         sync(engine.get('1090217586', '58502284', '10273252', '24477185'));
//         async([]);
//     }
//
//     else {
//         engine.search(q, sync, async);
//     }
// }
//
// Swal.fire({
//     title: 'Select assets to link',
//     input: 'text',
//     inputAttributes: {
//         autocapitalize: 'off'
//     },
//     customClass : {
//         input: 'js-link-assets-swal-input',
//     },
//     didOpen: () => {
//         $('.js-link-assets-swal-input').typeahead({
//             hint: $('.Typeahead-hint'),
//             menu: $('.Typeahead-menu'),
//             minLength: 0,
//             classNames: {
//                 open: 'is-open',
//                 empty: 'is-empty',
//                 cursor: 'is-active',
//                 suggestion: 'Typeahead-suggestion',
//                 selectable: 'Typeahead-selectable'
//             }
//         }, {
//             source: engineWithDefaults,
//             displayKey: 'screen_name',
//             templates: {
//                 suggestion: template,
//                 empty: empty
//             }
//         })
//         .on('typeahead:asyncrequest', function() {
//             $('.Typeahead-spinner').show();
//         })
//         .on('typeahead:asynccancel typeahead:asyncreceive', function() {
//             $('.Typeahead-spinner').hide();
//         });
//     },
//     showCancelButton: true,
//     confirmButtonText: 'Link assets',
//     showLoaderOnConfirm: true,
//     preConfirm: (login) => {
//         return fetch(`//api.github.com/users/${login}`)
//             .then(response => {
//                 if (!response.ok) {
//                     throw new Error(response.statusText)
//                 }
//                 return response.json()
//             })
//             .catch(error => {
//                 Swal.showValidationMessage(
//                     `Request failed: ${error}`
//                 )
//             })
//     },
//     allowOutsideClick: () => !Swal.isLoading()
// }).then((result) => {
//     if (result.isConfirmed) {
//         Swal.fire({
//             title: `${result.value.login}'s avatar`,
//             imageUrl: result.value.avatar_url
//         })
//     }
// })