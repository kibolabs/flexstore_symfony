import Swal from "sweetalert2";
import toastr from "toastr";

(function (namespace, $) {
    "use strict";

    var AppUserProfile = function () {
        // Create reference to this instance
        var o = this;
        // Initialize app when document is ready
        $(document).ready(function () {
            o.initialize();
        });

    };
    var p = AppUserProfile.prototype;

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function () {
        this._enableKeyEvents();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    // events
    p._enableKeyEvents = function () {
        var o = this;

        // Listen for the user-basic-details-edit button click
        $('.profile-main-content .user-basic-details-edit .user-basic-details-edit-btn').on('click', function (e) {
            o._handleBasicInfoButtonClick(e);
        });

        $(".profile-upload-form .pmop-edit").click(function(e){
            e.preventDefault();
            $('.uploadFile').click();
        });
        $(".pmo-pic .user-profile-photo").click(function(e){
            e.preventDefault();
        });
        //Listen to profile photo upload event
        $('.profile-upload-form .uploadFile').on('change', function(e)
        {
            var uploadFile = $(e.currentTarget);

            var form = uploadFile.closest('form');
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) {// no file selected, or no FileReader support
                console.log('no file selected, or no FileReader support');
                return;
            }

            if (/^image/.test( files[0].type)){ // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
                var file = uploadFile[0].files[0];

                reader.onloadend = function(){ // set image data as background of div
                    p._handleProfilePhotoUploadAjax(form, file)
                        .then(ajaxResult => {
                            uploadFile.closest(".pmo-pic").find('.js-profile-pic-anchor img').attr("src",this.result);
                            $('.js-header-profile-img').attr("src", ajaxResult.photo_thumb);

                            uploadFile.closest(".pmo-pic").find('.rem-photo').removeClass("hidden");
                            form.append('<i class="fa fa-times rem-photo" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-original-title="Click to remove a profile photo"></i>');
                            //Show success toatr message
                            toastr.success('Profile photo updated successfully', '');

                            //Refresh the scrollbar. This simply re-calculates the position and height of the scrollbar gadget.
                            $(".nano").nanoScroller();
                        })
                        .catch(
                            function (jqXHR) {
                                toastr.error('failed to upload profile photo, please retry', '');
                                var errorHTML = p._ParseAndMapSymfonyFormAjaxErrors(jqXHR);
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...Please try to refresh the page and re-upload!',
                                    html: errorHTML,
                                })
                            }
                        );
                }
            }
        });
        //Listen to profile photo delete event
        $('.profile-upload-form').on("click", "i.rem-photo" , function(e) {
            Swal.fire({
                icon: "warning",
                title: "Are you sure you want to delete this profile photo from the system?",
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: '#d14529',
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    var $deleteBtn = $(e.currentTarget);
                    //var $form = $deleteBtn.closest('form');
                    var deleteUrl = $deleteBtn.closest('form').data('delete-path');
                    return $.ajax({
                        url: deleteUrl,
                        type: 'POST',
                    }).then(response => {
                        //Show success toastr message
                        //toastr.success('Asset deleted successfully', '');
                        Swal.fire({
                            icon: 'success',
                            title: 'Profile photo deleted successfully',
                            showConfirmButton: false,
                            timer: 1000
                        }).then(
                            function () {
                                //Show success toatr message
                                toastr.success('Profile photo deleted successfully', '');

                                $deleteBtn.closest(".pmo-pic").find('.js-profile-pic-anchor img').attr("src",defaultProfileImg);
                                $('.js-header-profile-img').attr("src",defaultProfileImg);
                                $deleteBtn.closest(".pmo-pic").find('.rem-photo').removeClass("hidden");
                                $deleteBtn.remove();
                            }
                        )
                    })
                        .fail(
                            function(jqXHR,textStatus,errorThrown ){
                                let errorData = JSON.parse(jqXHR.responseText);

                                toastr.error('failed to delete Photo, please retry', '');

                                console.log(jqXHR);
                                console.log(textStatus);
                                console.log(errorThrown);
                                if(errorData.status === 'failed') {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oooops!!!',
                                    })
                                    Swal.showValidationMessage(
                                        `An error occurred: ${errorData.message}`
                                    )
                                }
                            }
                        )
                },
                allowOutsideClick: true
            }).then((result) => {
                if (result.value) {
                    Swal({
                        type: 'success',
                        title: 'Success',
                        text: result.value
                    })
                }
            }).catch(
                function (cancelEvent) {
                    console.log('cancelled delete operation, error is ' + cancelEvent);
                }
            )
        });

        $('#profile-main #user-info-tab .js-roles-manager').on('click', 'a', function (e) {
            e.preventDefault();
            o._handleRolesManagerLinkClick(e);
        });


    };

    // =========================================================================
    // SAVE USERNAME AND PASSWORD BY AJAX
    // =========================================================================

    p._handleBasicInfoButtonClick = function (e) {
        e.preventDefault();

        var btn = $(e.currentTarget);
        var form = $(e.currentTarget).closest('form');
        var cancel = $('form .profile-edit-cancel');
        var firstnameField = $("#user_name_firstname");
        var lastnameField = $("#user_name_lastname");
        var infoEditErrors = $(".user-info-edit-errors");

        btn.addClass('disabled');
        cancel.addClass('disabled');

        if (firstnameField.val() === '') {
            infoEditErrors.html('<p class="mt-5 mb-3 text-danger">First name can not be left blank!</p>');
        }
        else if (lastnameField.val() === '') {
            infoEditErrors.html('<p class="mt-5 mb-3 text-danger">Last name can not be left blank!</p>');
        }
        else {
            $.ajax({
                url: form.attr('action'),
                method: 'POST',
                data: form.serialize(),
                success: function (data) {
                    infoEditErrors.html('');
                    btn.removeClass('disabled');
                    cancel.removeClass('disabled');
                    $(".pmb-block-basic-info").removeClass("toggled");
                    $(".js-pmo-username").html(data.username);
                    $(".js-pmb-firstname").html(data.firstname);
                    $(".js-pmb-lastname").html(data.lastname);
                    $(".js-pmb-phone").html(data.phone);
                    $(".js-pmb-address").html(data.address.replace(',', '<br />'));

                    //Show success toatr message
                    toastr.success('Profile updated successfully', '');

                    //Refresh the scrollbar. This simply re-calculates the position and height of the scrollbar gadget.
                    $(".nano").nanoScroller();
                },
                error: function (jqXHR) {
                    toastr.error('failed to update profile information', '');

                    infoEditErrors.html(p._ParseAndMapSymfonyFormAjaxErrors(jqXHR));

                    btn.removeClass('disabled');
                    cancel.removeClass('disabled');

                }
            })
        }
    };


    /*-------------------------------------------
                Profile Edit/Edit Cancel
    ---------------------------------------------*/
    p._handleRolesManagerLinkClick = function (e) {
        e.preventDefault();

        var linkBtn = $(e.currentTarget);
        var linkAddr = linkBtn.data('path');

        linkBtn.addClass('disabled');


        $.ajax({
            url: linkAddr,
            method: 'POST',
            data: null,
            success: function (data) {
                linkBtn.removeClass('disabled');
                $(".js-pmo-user-role").html(data.role);

                //Show success toatr message
                toastr.success(data.message, '');
            },
            error: function (jqXHR) {
                let errorData = JSON.parse(jqXHR.responseText);
                console.log(errorData);
                toastr.error('failed to update user role', '');

                linkBtn.removeClass('disabled');
            }
        })
    };

    /*-------------------------------------------
            Profile Edit/Edit Cancel
    ---------------------------------------------*/
    $('#profile-main').on('click', '[data-ma-action]', function (e) {
        e.preventDefault();

        var $this = $(this);
        var action = $(this).data('ma-action');

        switch (action) {
            case 'profile-edit':
                $this.closest('.pmb-block').toggleClass('toggled');
                // Recall the nano scroller
                $('.nano').nanoScroller();

                break;

            case 'profile-edit-cancel':
                $(this).closest('.pmb-block').removeClass('toggled');
                // Recall the nano scroller
                $('.nano').nanoScroller();

                break;
        }

    });

    /*-------------------------------------------
           Recall the nano scroller
   ---------------------------------------------*/
    $('.nav-tabs li').on('click', function (e) {
        $('.nano').nanoScroller();
    });


    /*-------------------------------------------
           profile photo uploader
   ---------------------------------------------*/

    p._handleProfilePhotoUploadAjax = function (form, file) {
        var formData = new FormData();
        formData.append('photoFile', file, file.name);
        formData.append('user_photo[_token]', form.find('._token').val());
        formData.append('_action', 'photo_upload');
        return $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: formData,
            contentType: false, // requires jQuery 1.6+
            processData: false, // required
            cache: false,
        });
    }

    p._ParseAndMapSymfonyFormAjaxErrors = function (jqXHR) {
        var errorData = JSON.parse(jqXHR.responseText);
        console.log(errorData);
        var errorsHTML = '';
        if(errorData.errors && errorData.errors.errors) {
            var i;
            for (i = 0; i < errorData.errors.errors.length; ++i) {
                errorsHTML += errorData.errors.errors[i] + '<br />';
            }
        }
        if(errorData.errors && errorData.errors.children) {
            $.each(errorData.errors.children, function(key,val){
                if (val.errors && val.errors.length > 0) {
                    $.each(val.errors, function (fieldName, fieldError) {
                        errorsHTML += key + ' : ' + fieldError + '<br />';
                    })
                }
            });
        }
        return errorsHTML;
    }



    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppUserProfile = new AppUserProfile;
}(window.materialadmin, jQuery)); // pass in (namespace, jQuery):