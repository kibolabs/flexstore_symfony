/*	Author: Sam Shaba , s4mw3l@gmail.com	*/
/*	Date created: April 28, 2017			*/
/*	Last edited on: August 08, 2017			*/
/*	JS Functions for add-new-asset page		*/
/*	--------------------------------------	*/
document.checksn = function checksn(){
	var sn_val = $("#asset_sn").val();
	var sn = encodeURIComponent(sn_val);
	if(sn != ""){
		$("#snstatus").html('checking ...');
		var ajax = ajaxObj("POST", "/asset/new/check_sn");
		ajax.onreadystatechange = function() {
			if(ajaxReturn(ajax) == true) {
				if(ajax.responseText == '"sn_ok"') {
                    $("#snstatus").html('<strong class="text-success"><span class="glyphicon glyphicon-ok"></span></strong>');
				} else {
                    $("#snstatus").html('<strong class="text-danger">Duplicate</strong>');
				}
				//_("snstatus").innerHTML = ajax.responseText;
			}
		}
		ajax.send("sncheck="+sn);
	} else {
		$("#snstatus").html('<strong style="color:#F00;">can not be empty</strong>');
		$('#sn').parent('div').addClass('has-error');
	}
}
document.check_id = function check_id(){
	var int_id_val = $("#asset_barcode").val();
	var int_id = encodeURIComponent(int_id_val);
	if(int_id != ""){
		$("#intidstatus").html('checking ...');
		var ajax = ajaxObj("POST", "/asset/new/check_barcode");
		ajax.onreadystatechange = function() {
			if(ajaxReturn(ajax) == true) {
                if(ajax.responseText == '"id_ok"') {
                    $("#intidstatus").html('<strong class="text-success"><span class="glyphicon glyphicon-ok"></span></strong>');
                } else {
                    $("#intidstatus").html('<strong class="text-danger">Duplicate</strong>');
                }
			}
		}
		ajax.send("intidcheck="+int_id);
	} else {
		$("#intidstatus").html('<strong style="color:#F00;">can not be empty</strong>');
		$('#internal_id').parent('div').addClass('has-error');
	}
}
// Empty the element & status
function emptyElement2(x){
	$(x).html("");
	$('#'+x).parent('div').removeClass('has-error');
}


// Custom image widgets
$(".imgAdd").click(function(){
	$(this).closest(".row").find('.imgAdd').before('<div class="col-sm-4 imgUp"><div class="imagePreview"></div><label class="btn btn-primary">Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
});
$(".item-photo").click(function(){
	$('.uploadFile').click();
});
$(document).on("click", "i.rem" , function() {
	// $(this).parent().remove();
	$(this).closest(".img-container").find('.deleteFileChk').attr('checked',true);
	$(this).closest(".img-container").find('.item-photo').css("background-image", noImagepath);
	$(this).closest(".img-container").find('.rem').addClass("hidden");
	// $(this).closest(".imgUp").find('.galabel').html("Upload");
	var $el = $(this).closest(".img-container").find('.uploadFile');
	$el.wrap('<form>').closest('form').get(0).reset();
	$el.unwrap();
});
$(function() {
	$(document).on("change",".uploadFile", function()
	{
		var uploadFile = $(this);
		var files = !!this.files ? this.files : [];
		if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

		if (/^image/.test( files[0].type)){ // only image file
			var reader = new FileReader(); // instance of the FileReader
			reader.readAsDataURL(files[0]); // read the local file

			reader.onloadend = function(){ // set image data as background of div
				//alert(uploadFile.closest(".upimage").find('.imagePreview').length);
				uploadFile.closest(".card-body").find('.item-photo').css("background-image", "url("+this.result+")");
				uploadFile.closest(".card-body").find('.rem').removeClass("hidden");
				uploadFile.closest(".card-body").find('.galabel').html("Change");
				// uploadFile.closest(".imgUp").find('.updateCheckbox').prop('checked', true);
			}
		}

	});
});
// Custom Ajax
function ajaxObj( meth, url ) {
	var x = new XMLHttpRequest();
	x.open( meth, url, true );
	x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	return x;
}
function ajaxReturn(x){
	if(x.readyState == 4 && x.status == 200){
		return true;
	}
}