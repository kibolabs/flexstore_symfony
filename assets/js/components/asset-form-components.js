// FormComponents scripts
(function (namespace, $) {
    "use strict";

    var FormComponents = function () {
        // Create reference to this instance
        var o = this;
        // Initialize app when document is ready
        $(document).ready(function () {
            o.initialize();
        });
    };
    var p = FormComponents.prototype;

    // =========================================================================
    // INIT
    // =========================================================================
    p.initialize = function () {
        this._initSelect2();
        this._initInputMask(); //needed by date picker
        this._initDatePicker();
        this._initDatetime();
    };

    // =========================================================================
    // SELECT2
    // =========================================================================
    p._initSelect2 = function () {
        if (!$.isFunction($.fn.select2)) {
            return;
        }
        $(".select2-categories").select2({
            allowClear: true,
            placeholder :'Select a category'
        });

        $(".select2-departments").select2({
            allowClear: true,
            tags: true
        });

        $(".select2-suppliers").select2({
            allowClear: true,
            tags: true
        });

        $(".select2-employees").select2({
            allowClear: true,
            tags: true
        });

        $(".select2-locations").select2({
            allowClear: true,
            placeholder :'Select a location',
            tags: true
        });

        $(".select2-tags").select2({
            allowClear: true,
            //tags: true //Allow manually entered text in drop down.
        });
    };

    // =========================================================================
    // InputMask
    // =========================================================================
    p._initInputMask = function () {
        if (!$.isFunction($.fn.inputmask)) {
            return;
        }
        $(":input").inputmask();
    };

    // =========================================================================
    // Date Picker
    // =========================================================================
    p._initDatePicker = function () {
        if (!$.isFunction($.fn.datepicker)) {
            return;
        }
        $('#assignDateRange').datepicker({autoclose: true, todayHighlight: true, format: "yyyy-mm-dd"}); //Item assigning date range
        $('#demo-date-inline').datepicker({todayHighlight: true});
    };

    // =========================================================================
    // DATETIME
    // =========================================================================
    p._initDatetime = function() {
        if (!$.isFunction($.fn.datepicker)) {
            return;
        }
        //$('#purchasedatediv').datepicker({todayHighlight: true});
        $('#purchasedatediv').datepicker({autoclose: true, todayHighlight: true, format: "yyyy-mm-dd"});
        $('#checkindatediv').datepicker({autoclose: true, todayHighlight: true, format: "yyyy-mm-dd"});
        $('#reviewdatediv').datepicker({autoclose: true, todayHighlight: true, format: "yyyy-mm-dd"});
    };
    // =========================================================================
    namespace.FormComponents = new FormComponents;
}(window.materialadmin, jQuery)); // pass in (namespace, jQuery):

