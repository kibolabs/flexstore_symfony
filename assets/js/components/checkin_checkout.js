import '../../styles/asset_show.css';

import $ from 'jquery';

import './../../libs/select2/select2_new/select2.min';
// import 'select2';

import './../../libs/inputmask/jquery.inputmask.bundle.min';
// import './../libs/moment/moment.min';
import './../../libs/bootstrap-datepicker/bootstrap-datepicker';
import './../../libs/jquery-validation/dist/jquery.validate.min';
import './../../libs/jquery-validation/dist/additional-methods.min';
import './../../libs/bootstrap-selectpicker/bootstrap-select.min';