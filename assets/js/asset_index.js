import '../styles/asset_index.css';

// import $ from 'jquery';

import './../libs/DataTables/jquery.dataTables.min';
import './../libs/DataTables/extensions/ColVis/js/dataTables.colVis.min';
import './../libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min';
import './../libs/DataTables/extensions/Responsive/js/dataTables.responsive.min';
import './components/asset_datatables_components';
