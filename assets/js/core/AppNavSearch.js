(function (namespace, $) {
	"use strict";

	var AppNavSearch = function () {
		// Create reference to this instance
		var o = this;
		// Initialize app when document is ready
		$(document).ready(function () {
			o.initialize();
		});

	};
	var p = AppNavSearch.prototype;

	// =========================================================================
	// MEMBERS
	// =========================================================================

	p._clearSearchTimer = null;

	// =========================================================================
	// INIT
	// =========================================================================

	p.initialize = function () {
		this._enableEvents();
	};

	// =========================================================================
	// EVENTS
	// =========================================================================

	// events
	p._enableEvents = function () {
		var o = this;

		// Listen for the nav search button click
		$('.navbar-search .btn').on('click', function (e) {
			o._handleButtonClick(e);
		});

		// When the search field loses focus
		$('.navbar-search input').on('blur', function (e) {
			o._handleFieldBlur(e);
		});
		//Listen to recent search click
		$('.js-search-suggestions .js-open-recent-search-offcanvas').on('click', function (e) {
			e.preventDefault();
			materialadmin.AppOffcanvas.openSearchOffcanvas();
		});
	};

	// =========================================================================
	// NAV SEARCH
	// =========================================================================

	p._handleButtonClick = function (e) {
		e.preventDefault();

		var form = $(e.currentTarget).closest('form');
		var input = form.find('input');
		var keyword = input.val();
		var $searchHeaderText = $('#offSearchHeader');
		var $canvasBox = $('#offcanvas-search');
		var $recentSearchDropdown = form.find('.dropdown');
		var $offSearchHeaderText = $('#offSearchHeader').html();

		if ($.trim(keyword) === '') {
			// When there is no keyword, just open the bar
			form.addClass('expanded');
			input.focus();

			if($offSearchHeaderText !== 'No Search Results') {
				$recentSearchDropdown.find('.js-open-recent-search-offcanvas a').html('Recent search<br><span class="text-xs">' + $offSearchHeaderText + '</span> <i class="fa fa-arrow-right"></i>')
				$recentSearchDropdown.addClass('open');
			}
		}
		else {
			// When there is a keyword, submit the keyword
			$searchHeaderText.html('Searching for <span>"' + keyword + '"</span>');
			//Open the search offcanvas
			materialadmin.AppOffcanvas.openSearchOffcanvas();
			//Add the Loading animation
			materialadmin.AppCard.addCardLoader($canvasBox);

			form.addClass('expanded');
			$('#searchResultsBox').html('');
			//form.submit();
			$.ajax({
				url: form.attr('action'),
				method: 'POST',
				data: form.serialize(),
				success: function (data) {
					$searchHeaderText.html('Search results for <span>' + keyword + '</span>...');
					$searchHeaderText.append('<br /><small class="text-primary text-sm">' + data.assets.length + ' assets found!</small>');
					//$("#offcanvasSearchForm").removeClass('hidden');
					let html = '';
					$.each(data.assets, (key, asset) => {
						html += `
						<li class="tile">
							<a class="tile-content ink-reaction" href="/asset/${asset.id}" target="_blank">
								<div class="tile-icon">
									<img src="${asset.photo != null && asset.photo !== '' ? '/uploads/asset/' + asset.photo : noImage2path}" alt="" />
								</div>
								<div class="tile-text">
									${asset.name}
									<small><i class="fa fa-sitemap"></i> : ${asset.category.parent.name} :: ${asset.category.name}</small>
									<small><i class="fa fa-qrcode"></i> : ${asset.barcode}, Sn : ${asset.sn}</small>
								</div>
							</a>
						</li>
						`
					});
					$('#searchResultsBox').html(html);
					//Remove the Loading animation
					materialadmin.AppCard.removeCardLoader($canvasBox);
					//Refresh the scrollbar. This simply re-calculates the position and height of the scrollbar gadget.
					$(".nano").nanoScroller();
				},
				error: function (jqXHR) {
					$searchHeaderText.html('Search results for <span>' + keyword + '</span>...');
					$searchHeaderText.append('<br /><small class="text-danger text-sm">Returned an error, please retry!</small>');
					//$("#offcanvasSearchForm").removeClass('hidden');

				}
			})

			// Clear the timer that removes the keyword
			clearTimeout(this._clearSearchTimer);
		}
	};

	// =========================================================================
	// FIELD BLUR
	// =========================================================================

	p._handleFieldBlur = function (e) {
		// When the search field loses focus
		var input = $(e.currentTarget);
		var form = input.closest('form');
		var $recentSearchDropdown = form.find('.dropdown');

		// Collapse the search field
		form.removeClass('expanded');


		// Clear the textfield after 300 seconds (the time it takes to collapse the field)
		clearTimeout(this._clearSearchTimer);
		this._clearSearchTimer = setTimeout(function () {
			input.val('');
			$recentSearchDropdown.removeClass('open');
		}, 150);
	};

	// =========================================================================
	// DEFINE NAMESPACE
	// =========================================================================

	window.materialadmin.AppNavSearch = new AppNavSearch;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
