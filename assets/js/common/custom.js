// JavaScript Document custom by Sam Shaba 28.04.2017
// Use "_" instead of "document.getElementById"

//Toggle element's dispay state
function toggleElement(x){
	var x = $(x);
	if(x.style.display == 'block'){
		x.style.display = 'none';
	}else{
		x.style.display = 'block';
	}
}

//JS Max textlimit Alert
function textLimit(field, maxlimit) {
	if (field.value.length > maxlimit){
		alert(maxlimit+" maximum character limit reached");
		field.value = field.value.substring(0, maxlimit);
	}
}
//Attach this to the onkeyup event of the text input field using [onkeyup="textLimit(this,250)"]

/*
 * JAVASCRIPT FUNCTIONS
 */


// RESTRICT ??? No spaces for emails, Only Alphanumeric for username
function restrict(elem){
	var tf = _(elem);
	var rx = new RegExp;
	if(elem == "email"){
		rx = /[' "]/gi;
	} else if(elem == "username"){
		rx = /[^a-z0-9]/gi;
	}
	tf.value = tf.value.replace(rx, "");
}

function omitChars(input,reg) {
	var input = _(input);
	var rx = new RegExp;
	if(reg == "email"){
		rx = /[' "]/gi;
	} else if(reg == "username"){
		rx = /[^a-z0-9]/gi;
	} else if(reg == "letters_underscore"){
		rx = /[^a-z_]/g;
	} else if(reg == "letters_num_underscore"){
		rx = /[^a-z0-9_]/g;
	} else if(reg == "letters_underscore_i"){
		rx = /[^a-z_]/gi;
	} else if(reg == "letters_num_underscore_i"){
		rx = /[^a-z0-9_]/gi;
	}
	input.value = input.value.replace(rx, "");
}

/*
 * Expand or collapse menubar from localStorage
 */
$(document).ready(function() {
	if (typeof(Storage) !== "undefined") {
		var remembered_menubar_style = localStorage.getItem("menubar-set");

		if(remembered_menubar_style === 'collapsed' && $(document.body).hasClass('menubar-pin')) {
			$(document.body).removeClass('menubar-pin');
			//console.log('Collapsed menubar from memory');
		} else if(remembered_menubar_style === 'expanded' && !$(document.body).hasClass('menubar-pin')) {
			$(document.body).addClass('menubar-pin');
			//console.log('Expanded menubar from memory');
		}
	} else {
		// Sorry! No Web Storage support..
		console.log('Sorry! No Web Storage support..');
	}
});
