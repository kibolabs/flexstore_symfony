/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './../styles/app.css';
// import 'bootstrap'; // adds bootstrap to jQuery
import toastr from 'toastr';
import 'nanoscroller';

// Need jQuery? Install it with "yarn add jquery",
import $ from 'jquery';

// create global $ and jQuery variables
// global.$ = global.jQuery = $;

// import 'jquery-ui';
import './../libs/jquery-ui/jquery-ui.min';

// import './libs/jquery/jquery-migrate-1.2.1.min';
import './../libs/bootstrap/bootstrap';


// import {Spinner} from 'spin.js';
// global.Spinner = Spinner;
// import './../libs/spin.js/spin'; // Failed to use this, decided to load it in base page


import './core/Main';
import './core/AppNavigation';
import './core/AppOffcanvas';
import './core/AppCard';
import './core/AppForm';
import './core/AppNavSearch';
import './core/AppVendor';

import './core/init';

// import './../libs/toastr/toastr'; // Failed to use this, decided to load it in base page


import './common/custom';

document.emptyElement = function emptyElement(x){
    $(x).html("");
}

