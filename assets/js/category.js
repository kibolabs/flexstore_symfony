// any CSS you import will output into a single css file (app.css in this case)
// import '../styles/category.css.css';

import $ from 'jquery';
import Swal from "sweetalert2";


(function (namespace, $) {
    "use strict";

    var Category = function () {
        // Create reference to this instance
        var o = this;
        // Initialize app when document is ready
        $(document).ready(function () {
            o.initialize();
        });

    };
    var p = Category.prototype;

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function () {
        this._enableEvents();
        // this._initTooltips();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    // events
    p._enableEvents = function () {
        var o = this;
        var $categoriesContainer = $('.js-categories-container');
        var $canvasBox = $('#offcanvas-abstract');

        //Prevent the category accordion from collapsing when action buttons are clicked while it is expanded
        $categoriesContainer.on("click", ".tools span a", function(e) {
            e.preventDefault();
            if ($(this).closest('.category-card').hasClass('expanded')) {
                e.stopPropagation();
            }
        });
        // Listen for the add new or edit category/sub-category click
        $categoriesContainer.on('click', '.js-new-cat-btn, .js-new-subcat-btn', function (e) {
            o._handleNewCategoryButtonClick(e);
        });
        // Listen for the add new or edit category/sub-category click
        $categoriesContainer.on('click', '.js-edit-cat-btn, .js-edit-subcat-btn', function (e) {
            o._handleEditCategoryButtonClick(e);
        });
        //Prevent the category form from automatically submitting on button click
        $canvasBox.on("submit", "form", function(e){
            e.preventDefault();
        });
        //Listen to delete category/sub-category click
        $categoriesContainer.on("click", ".js-delete-category, .js-delete-subcategory", function(e) {
            o._handleCategoryDelete(e)
        });
        $canvasBox.on('click','.js-save-category-btn', function (e) {
            o._handleNewCategoryFormSubmit(e);
        })
        $canvasBox.on('click','.js-edit-category-btn', function (e) {
            o._handleNewCategoryFormSubmit(e, true);
        })

    };

    // =========================================================================
    // Handle new/edit Category/subCategory button clock
    // =========================================================================
    p._handleNewCategoryButtonClick = function (e) {
        e.preventDefault();

        var $clickedBtn, $canvasBox, canvasHeaderText, $offCanvasBody, getFormUrl, isSubCategory, $formData;

        $clickedBtn = $(e.currentTarget);
        isSubCategory = false;
        $canvasBox = $('#offcanvas-abstract');
        $offCanvasBody = $canvasBox.find('.offcanvas-body');
        $offCanvasBody.html('');
        getFormUrl = $clickedBtn.data('getformurl');
        canvasHeaderText = 'Add new category';
        $formData = {action: 'getNewCatForm'};

        if($clickedBtn.hasClass('js-new-subcat-btn')) {
            isSubCategory = true;
        }

        Swal.fire({
            title: 'Loading',
            html: 'Please wait...',
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading()
                $.ajax({
                    url: getFormUrl,
                    method: 'POST',
                    data: $formData,
                }).then(
                    function (data) {
                        $offCanvasBody.html(data);
                        if(isSubCategory) {
                            var catName = $clickedBtn.data('catname');
                            var catId = $clickedBtn.data('catid');

                            $offCanvasBody.find('.js-parent-cat-container .form-group select').val(catId);
                            $offCanvasBody.find('.js-parent-cat-container .form-group select').prop('disabled', 'disabled');
                            //$offCanvasBody.find('.js-save-category-btn').data('catid', catId);
                            $offCanvasBody.find('.js-save-category-btn').attr('data-catid', catId);
                            canvasHeaderText = "New subcategory for " + catName;
                        }

                        materialadmin.AppOffcanvas.openAbstractOffcanvas(canvasHeaderText);
                        Swal.close();
                    }
                ).fail(
                    function(jqXHR,textStatus,errorThrown ){
                        let errorData = JSON.parse(jqXHR.responseText);
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        if(errorData.status === 'failed') {
                            var errorHTML = p._ParseAndMapSymfonyFormAjaxErrors(jqXHR);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                            })
                            Swal.showValidationMessage(
                                `An error occurred: ${errorThrown}`
                            )
                        }
                    }
                );

            }
        }).then((result) => {
            if (result.dismiss) {
                console.log('dismissed')
            }
        })
    }

    // =========================================================================
    // Handle edit Category/Sub-Category button clock
    // =========================================================================
    p._handleEditCategoryButtonClick = function (e) {
        e.preventDefault();

        var $clickedBtn, $canvasBox, canvasHeaderText, $offCanvasBody, getFormUrl, isSubCategory, $formData, catName;

        $clickedBtn = $(e.currentTarget);
        isSubCategory = false;
        $canvasBox = $("#offcanvas-abstract");
        $offCanvasBody = $canvasBox.find('.offcanvas-body');
        $offCanvasBody.html('');
        getFormUrl = $clickedBtn.data('getformurl');
        catName = $clickedBtn.data('catname');
        canvasHeaderText = 'Edit ' + catName;
        $formData = {action: 'getEditCatForm'};

        //$formData.id = $clickedBtn.data('id');
        //$offCanvasBody.find('.js-save-category-btn').addClass('js-edit-category-btn');
        //console.log($offCanvasBody.find('.js-save-category-btn'));

        if($clickedBtn.hasClass('js-edit-subcat-btn')) {
            isSubCategory = true;
        }

        Swal.fire({
            title: 'Loading',
            html: 'Please wait...',
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading()
                $.ajax({
                    url: getFormUrl,
                    method: 'POST',
                    data: $formData,
                }).then(
                    function (data) {
                        $offCanvasBody.html(data);
                        if(isSubCategory) {
                            var catId = $clickedBtn.data('catid');
                            //$offCanvasBody.find('.js-parent-cat-container .form-group select').val(catId);
                            //$offCanvasBody.find('.js-parent-cat-container .form-group select').prop('disabled', 'disabled');
                            //$offCanvasBody.find('.js-save-category-btn').data('catid', catId);
                            $offCanvasBody.find('.js-save-category-btn').attr('data-catid', catId);
                        }

                        materialadmin.AppOffcanvas.openAbstractOffcanvas(canvasHeaderText);
                        Swal.close();
                    }
                ).fail(
                    function(jqXHR,textStatus,errorThrown ){
                        let errorData = JSON.parse(jqXHR.responseText);
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        if(errorData.status === 'failed') {
                            var errorHTML = p._ParseAndMapSymfonyFormAjaxErrors(jqXHR);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                            })
                            Swal.showValidationMessage(
                                `An error occurred: ${errorThrown}`
                            )
                        }
                    }
                );

            }
        }).then((result) => {
            if (result.dismiss) {
                console.log('dismissed')
            }
        })
    }


    // =========================================================================
    // Handle new Category form submit
    // =========================================================================
    p._handleNewCategoryFormSubmit = function (e, isEdit = false) {
        e.preventDefault();

        var $clickedBtn, $canvasBox, $offCanvasBody, submitPath, $form, $formData, categoryID;

        $clickedBtn = $(e.currentTarget);
        $offCanvasBody = $('#offcanvas-abstract .offcanvas-body');
        $form = $clickedBtn.closest('form');
        submitPath = $clickedBtn.data('path');
        $formData = $form.serialize();
        categoryID = $clickedBtn.data('catid');
        $formData += '&catid=' + categoryID + '&otherkey=otherval';

        Swal.fire({
            title: 'Loading',
            html: 'Please wait...',
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading()
                $.ajax({
                    url: submitPath,
                    method: 'POST',
                    data: $formData
                }).then(
                    function (data) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Category saved successfully',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(
                            function(){
                                materialadmin.AppOffcanvas.closeAbstractOffcanvas();

                                if(isEdit) {
                                    location.reload();//to do code to dynamically edit category here
                                } else {
                                    if(categoryID != null) {//means it is a subcategory
                                        var $subCatContainer, $currentListHtml, $currentNoListItems, parentCategoryName;
                                        $subCatContainer = $('#asset_category-' + categoryID + ' .subcategories_list');
                                        $currentListHtml = '';
                                        $currentNoListItems = $subCatContainer.find('li.no-list-item');
                                        parentCategoryName = $offCanvasBody.find('.js-parent-cat-container select option:selected').html();

                                        if($currentNoListItems.length < 1) {//means there are some real subcategories already on the list
                                            $currentListHtml =  $subCatContainer.html();
                                        }

                                        var subCatTemplate = `
                                        <li class="tile subcategory-card">
                                            <a class="tile-content ink-reaction">
                                                <div" class="tile-text">
                                                    ${ data.category.name }
                                                    <small>${ parentCategoryName } &#8594; ${ data.category.name }</small>
                                                </div>
                                                <div class="tile-buttons">
                                                    <button class="btn btn-icon-toggle js-delete-subcategory" data-url="${ submitPath + data.category.id }"><i class="fa fa-fw fa-trash-o"></i></button>
                                                    <button class="btn btn-icon-toggle js-edit-cat-btn" data-id="${ data.category.id }" data-catid="{{ category.id }}" data-catname="${ data.category.name }" data-getformurl="${ submitPath + data.category.id + '/edit'}"><i class="fa fa-pencil"></i></button>
                                                </div>
                                            </a>
                                        </li>
                                    `

                                        $subCatContainer.html($currentListHtml + subCatTemplate);

                                    } else {//It is a main category
                                        var $catContainer, $currentHtml, categoryName, catTemplate;

                                        catTemplate = `
                                    <div class="card panel category-card" id="category-${ data.category.id }">
                                        <div class="card-head collapsed" data-toggle="collapse" data-parent="#asset_categories" data-target="#asset_category-${ data.category.id }">
                                            <header>
                                                <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                                <span>${ data.category.name }</span>
                                            </header>
                                            <div class="tools">
                                                <span data-toggle="tooltip" data-placement="top" data-original-title="Delete category">
                                                    <a href="#" data-url="${ submitPath + data.category.id }" class="js-delete-category btn btn-icon-toggle" data-catname="${ data.category.name }"><i class="md md-delete"></i></a>
                                                </span>
                                                <span data-toggle="tooltip" data-placement="top" data-original-title="Edit category">
                                                    <a  href="#" data-id="${ data.category.id }" class="js-edit-cat-btn btn btn-icon-toggle" data-catname="${ data.category.name }" data-getformurl="${ submitPath + data.category.id + '/edit'}"><i class="md md-edit"></i></a>
                                                </span>
                                                <span class="new-subcat-span" data-toggle="tooltip" data-placement="top" data-original-title="Add new sub-category">
                                                    <a href="#" class="js-new-subcat-btn btn btn-icon-toggle" data-catid="${ data.category.id }" data-catname="${ data.category.name }" data-getformurl="${ submitPath }"><i class="fa fa-plus"></i></a>
                                                </span>
                                            </div>
                                        </div>
                                        <div id="asset_category-${ data.category.id }" class="collapse">
                                            <div class="card-body">
                                                <ul class="list subcategories_list" id="subcategories_list${ data.category.id }">
                                                        <li class="tile subcategory-card no-list-item text-default-light">
                                                            No subcategories defined
                                                        </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    `;
                                        $catContainer = $('.categories-list');
                                        $currentHtml = $catContainer.html();
                                        $catContainer.html(catTemplate + $currentHtml);
                                    }
                                }

                                //clear the OffCanvas body content
                                $offCanvasBody.html('');
                            }
                        )
                    }
                ).fail(
                    function(jqXHR,textStatus,errorThrown ){
                        let errorData = JSON.parse(jqXHR.responseText);
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        if(errorData.status === 'failed') {
                            var errorHTML = p._ParseAndMapSymfonyFormAjaxErrors(jqXHR);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                html: errorHTML,
                            })
                            Swal.showValidationMessage(
                                `An error occurred: ${errorData.message}`
                            )
                        }
                    }
                );
            }
        }).then((result) => {
            if (result.dismiss) {
                return null;
            }
        }).catch(
            function (cancelEvent) {
                console.log('cancelled delete operation, error is ' + cancelEvent);
            }
        )
    }

    // =========================================================================
    // Handle new Category/Sub-category delete
    // =========================================================================
    p._handleCategoryDelete = function(e) {
        var $clickedBtn = $(e.currentTarget);
        var deleteUrl = $clickedBtn.data('url');
        var $deletedCategory = $clickedBtn.closest('.category-card, .subcategory-card');
        var $subCatContainer = $clickedBtn.closest('.subcategories_list');
        var isSubcategory = false;

        if($deletedCategory.hasClass('subcategory-card')) {
            isSubcategory = true;
        }

        Swal.fire({
            icon: "warning",
            title: "Are you sure you want to delete this category and all it's records from the system?",
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: '#d14529',
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return $.ajax({
                    url: deleteUrl,
                    method: 'DELETE',
                }).then(response => {
                    Swal.fire({
                        icon: 'success',
                        title: 'Category deleted successfully',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(
                        function () {
                            $deletedCategory.fadeOut('slow');
                            //$deletedCategory.remove();
                            if(isSubcategory) {
                                var $currentListItemsCount = $clickedBtn.closest('.subcategories_list').children('li').length;

                                if ($currentListItemsCount < 2) {//means there are no subcategories left on the list
                                    var noSubcatHtml = `
                                        <li class="tile subcategory-card no-list-item text-default-light">
                                            No subcategories defined
                                        </li>
                                    `;
                                    $deletedCategory.replaceWith(noSubcatHtml);
                                } else {
                                    $deletedCategory.remove();
                                }
                            } else {
                                $deletedCategory.remove();
                            }
                        }
                    )
                }).fail(
                    function(jqXHR,textStatus,errorThrown ){
                        Swal.fire({
                            icon: 'error',
                            title: 'Error deleting category',
                        })
                        Swal.showValidationMessage(
                            `An error occurred: ${JSON.parse(jqXHR.responseText).errors}`
                        )
                    }
                )
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                Swal({
                    type: 'success',
                    title: 'Success',
                    text: result.value
                })
            }
        }).catch(
            function (cancelEvent) {
                console.log('cancelled delete operation, error is ' + cancelEvent);
            }
        )
    }

    p._ParseAndMapSymfonyFormAjaxErrors = function (jqXHR) {
        var errorData = JSON.parse(jqXHR.responseText);
        console.log(errorData);
        var errorsHTML = '';
        if(errorData.errors && errorData.errors.errors) {
            var i;
            for (i = 0; i < errorData.errors.errors.length; ++i) {
                errorsHTML += errorData.errors.errors[i] + '<br />';
            }
        }
        if(errorData.errors && errorData.errors.children) {
            $.each(errorData.errors.children, function(key,val){
                if (val.errors && val.errors.length > 0) {
                    $.each(val.errors, function (fieldName, fieldError) {
                        errorsHTML += key + ' : ' + fieldError + '<br />';
                    })
                }
            });
        }
        return errorsHTML;
    }

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.Category = new Category;
}(window.materialadmin, jQuery)); // pass in (namespace, jQuery):