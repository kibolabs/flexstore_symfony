<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210108055556 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activity (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, action VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, entity VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, object_id INT NOT NULL, change_set LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_AC74095AB03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE asset (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, location_id INT NOT NULL, status_id INT NOT NULL, supplier_id INT DEFAULT NULL, department_id INT DEFAULT NULL, assigned_user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, sn VARCHAR(255) NOT NULL, barcode VARCHAR(255) NOT NULL, condition_notes VARCHAR(255) DEFAULT NULL, specs VARCHAR(255) DEFAULT NULL, itemtags VARCHAR(255) DEFAULT NULL, purchasedate DATETIME DEFAULT NULL, price NUMERIC(20, 2) DEFAULT NULL, currency VARCHAR(255) DEFAULT NULL, lifeyears INT DEFAULT NULL, icon VARCHAR(255) DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, images JSON DEFAULT NULL COMMENT \'(DC2Type:json_array)\', is_assigned TINYINT(1) DEFAULT NULL, item_notes VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, last_checkout_date DATETIME DEFAULT NULL, check_in_date DATETIME DEFAULT NULL, check_in_out_notes VARCHAR(255) DEFAULT NULL, INDEX IDX_2AF5A5C12469DE2 (category_id), INDEX IDX_2AF5A5C64D218E (location_id), INDEX IDX_2AF5A5C6BF700BD (status_id), INDEX IDX_2AF5A5C2ADD6D8C (supplier_id), INDEX IDX_2AF5A5CAE80F5DF (department_id), INDEX IDX_2AF5A5CADF66B1A (assigned_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, added_by_id INT NOT NULL, name VARCHAR(255) NOT NULL, added_on DATETIME NOT NULL, INDEX IDX_64C19C1727ACA70 (parent_id), INDEX IDX_64C19C155B127A4 (added_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE department (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_CD1DE18AB03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, type VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, icon_fa VARCHAR(255) DEFAULT NULL, icon_image VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_3BAE0AA7B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE history (id INT AUTO_INCREMENT NOT NULL, event_id INT NOT NULL, asset_id INT NOT NULL, user_id INT NOT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, change_set LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_27BA704B71F7E88B (event_id), INDEX IDX_27BA704B5DA1941 (asset_id), INDEX IDX_27BA704BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE linked_asset (id INT AUTO_INCREMENT NOT NULL, asset_id INT NOT NULL, linked_id INT NOT NULL, INDEX IDX_B2ABB5AB5DA1941 (asset_id), INDEX IDX_B2ABB5AB56D691E4 (linked_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, shortname VARCHAR(255) NOT NULL, address VARCHAR(255) DEFAULT NULL, added_by VARCHAR(255) DEFAULT NULL, added_on DATETIME NOT NULL, INDEX IDX_5E9E89CB727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, color VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_7B00651CB03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplier (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, shortname VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, added_by VARCHAR(255) NOT NULL, added_on DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, created_users_id INT DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, roles JSON NOT NULL, password VARCHAR(64) NOT NULL, reset_token VARCHAR(255) DEFAULT NULL, api_token VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, photo VARCHAR(255) DEFAULT NULL, skype VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6497BA2F5EB (api_token), INDEX IDX_8D93D649F84A3A8A (created_users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activity ADD CONSTRAINT FK_AC74095AB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE asset ADD CONSTRAINT FK_2AF5A5C12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE asset ADD CONSTRAINT FK_2AF5A5C64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE asset ADD CONSTRAINT FK_2AF5A5C6BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE asset ADD CONSTRAINT FK_2AF5A5C2ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id)');
        $this->addSql('ALTER TABLE asset ADD CONSTRAINT FK_2AF5A5CAE80F5DF FOREIGN KEY (department_id) REFERENCES department (id)');
        $this->addSql('ALTER TABLE asset ADD CONSTRAINT FK_2AF5A5CADF66B1A FOREIGN KEY (assigned_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C155B127A4 FOREIGN KEY (added_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE department ADD CONSTRAINT FK_CD1DE18AB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704B71F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704B5DA1941 FOREIGN KEY (asset_id) REFERENCES asset (id)');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE linked_asset ADD CONSTRAINT FK_B2ABB5AB5DA1941 FOREIGN KEY (asset_id) REFERENCES asset (id)');
        $this->addSql('ALTER TABLE linked_asset ADD CONSTRAINT FK_B2ABB5AB56D691E4 FOREIGN KEY (linked_id) REFERENCES asset (id)');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB727ACA70 FOREIGN KEY (parent_id) REFERENCES location (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE status ADD CONSTRAINT FK_7B00651CB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649F84A3A8A FOREIGN KEY (created_users_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE history DROP FOREIGN KEY FK_27BA704B5DA1941');
        $this->addSql('ALTER TABLE linked_asset DROP FOREIGN KEY FK_B2ABB5AB5DA1941');
        $this->addSql('ALTER TABLE linked_asset DROP FOREIGN KEY FK_B2ABB5AB56D691E4');
        $this->addSql('ALTER TABLE asset DROP FOREIGN KEY FK_2AF5A5C12469DE2');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE asset DROP FOREIGN KEY FK_2AF5A5CAE80F5DF');
        $this->addSql('ALTER TABLE history DROP FOREIGN KEY FK_27BA704B71F7E88B');
        $this->addSql('ALTER TABLE asset DROP FOREIGN KEY FK_2AF5A5C64D218E');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB727ACA70');
        $this->addSql('ALTER TABLE asset DROP FOREIGN KEY FK_2AF5A5C6BF700BD');
        $this->addSql('ALTER TABLE asset DROP FOREIGN KEY FK_2AF5A5C2ADD6D8C');
        $this->addSql('ALTER TABLE activity DROP FOREIGN KEY FK_AC74095AB03A8386');
        $this->addSql('ALTER TABLE asset DROP FOREIGN KEY FK_2AF5A5CADF66B1A');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C155B127A4');
        $this->addSql('ALTER TABLE department DROP FOREIGN KEY FK_CD1DE18AB03A8386');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7B03A8386');
        $this->addSql('ALTER TABLE history DROP FOREIGN KEY FK_27BA704BA76ED395');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE status DROP FOREIGN KEY FK_7B00651CB03A8386');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649F84A3A8A');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE asset');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE department');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE history');
        $this->addSql('DROP TABLE linked_asset');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE status');
        $this->addSql('DROP TABLE supplier');
        $this->addSql('DROP TABLE user');
    }
}
