<?php

namespace App\DataFixtures;

use App\Entity\Country;
use App\Entity\Destination;
use App\Entity\Property;
use App\Entity\Room;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Faker\Factory;
use Faker\Generator;

class AllFixtures extends Fixture

{
    //You can run ::: php bin/console doctrine:fixtures:load
    /** @var Generator */
    protected $faker;

    public function load(ObjectManager $manager)

    {
        $this->faker = Factory::create();
        $this->addUser($manager);
        $this->addCountry($manager);
        $this->addDestination($manager);
        $this->addProperty($manager);
        $this->addRoom($manager);

        $manager->flush();
    }

    private function addUser(EntityManager $em)
    {
        for ($i = 0; $i <= 2; $i++) {
            $user = new User();
            $user->setUsername(strtolower($this->user($i)));
            $user->setEmail(strtolower($this->user($i)) . "@honeyguide.org");
            $user->setPlainPassword($user->getEmail());
            //$user->setRoles(['ROLE_ADMIN']);
            //$user->setResetToken()

            $this->setReference('user_' . $i, $user);
            $em->persist($user);
        }

    }

    private function addCountry(EntityManager $em)
    {
        for ($i = 0; $i <= 6; $i++) {
            $country = new Country();
            $country->setName($this->country($i));
            $country->setDescription($this->faker->paragraph());
            $country->setPhoto($this->faker->imageUrl(800, 530, 'abstract'));
            $country->setBackground($this->faker->imageUrl(800, 530, 'abstract'));
            $country->setCreatedBy($this->getReference('user_'.random_int(0, 2)));
            $country->setUpdatedBy($country->getCreatedBy());

            $this->setReference('country_'.$i, $country);
            $em->persist($country);
        }
    }

    private function addDestination(EntityManager $em)
    {
        for ($i = 0; $i <= 6; $i++) {
            $destination = new Destination();
            $destination->setName($this->destination($i));
            $destination->setDescription($this->faker->paragraph());
            $destination->setPhoto($this->faker->imageUrl(800, 530, 'abstract'));
            $destination->setCountry($this->getReference('country_'.random_int(0, 6)));
            $destination->setCreatedBy($this->getReference('user_'.random_int(0, 2)));
            $destination->setUpdatedBy($destination->getCreatedBy());


            $this->setReference('destination_'.$i, $destination);
            $em->persist($destination);
        }
    }

    private function addProperty(EntityManager $em)
    {
        for ($i = 0; $i <= 6; $i++) {
            $property = new Property();
            $property->setName($this->property($i));
            $property->setDescription($this->faker->paragraph());
            $property->setPhoto($this->faker->imageUrl(800, 530, 'abstract'));
            $property->setDestination($this->getReference('destination_'.random_int(0, 6)));
            $property->setAddress($this->faker->streetName);
            $property->setEmail($this->faker->safeEmail);
            $property->setPhone($this->faker->e164PhoneNumber);
            $property->setWebsite($this->faker->domainName);
            $property->setSponsored($this->faker->boolean());
            $property->setCreatedBy($this->getReference('user_'.random_int(0, 2)));
            $property->setUpdatedBy($property->getCreatedBy());

            $this->setReference('property_'.$i, $property);
            $em->persist($property);
        }
    }

    private function addRoom(EntityManager $em)
    {
        for ($j = 0; $j <= 6; $j++)
        {
            for ($i = 0; $i <= 2; $i++) {
                $room = new Room();
                $room->setName($this->room($i));
                $room->setDescription($this->faker->paragraph());
                $room->setPhoto($this->faker->imageUrl(800, 530, 'abstract'));
                $room->setProperty($this->getReference('property_'.$j));
                $room->setCreatedBy($this->getReference('user_'.random_int(0, 2)));
                $room->setUpdatedBy($room->getCreatedBy());

                $this->setReference('room'.$i, $room);
                $em->persist($room);
            }
        }
    }

    private function user($i)
    {
        $users = [
            'Sam',
            'Belinda',
            'Lizy'
        ];

        return $users[$i];
    }

    private function category($i)
    {
        $categories = [
            'Smartphones',
            'Laptops',
            'ElectricAppliances',
            'Furnitures',
            'Stationeries',
            'Flashlights',
            'Mechanics'
        ];

        return $categories[$i];
    }

    private function location($i)
    {
        $locations = [
            'Serengeti National Park',
            'Manyara Ranch',
            'Randilen WMA',
            'Makame WMA'
        ];

        return $locations[$i];
    }

    private function employee($i)
    {
        $employees = [
            'Sam Shaba',
            'Belinda Mullo',
            'Elizabeth Mintoi',
            'Loiruku Mollel',
            'Daudi Mollel'
        ];

        return $employees[$i];
    }

    private function project($i)
    {
        $projects = [
            'USAID',
            'AWF',
            'CORE'
        ];

        return $projects[$i];
    }

    private function supplier($i)
    {
        $suppliers = [
            'Benson and Company Limited',
            'Sound and Vision',
            'Power Computers'
        ];

        return $suppliers[$i];
    }
}
