<?php


namespace App\EventListener;


use App\Entity\Asset;
use App\Entity\History;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AssetChangesEventListener
{

    private $entityManager;
    private $tokenStorage;
    private $eventRepository;
    private $auditFields = ['name', 'sn','barcode','specs','purchasedate','price','currency','lifeyears','itemNotes','category','location','status','supplier','department'];
    private $isAssignedChanged = false;
    private $isPriceUpdated = false;

    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage, EventRepository $eventRepository)
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->eventRepository = $eventRepository;
    }


    // callback methods must be called exactly like the events they listen to;
    // they receive an argument of type LifecycleEventArgs, which gives you access
    // to both the entity object of the event and the entity manager itself


    // The prePersist event occurs for a given entity before the respective EntityManager persist operation for that entity is executed.
    // It should be noted that this event is only triggered on initial persist of an entity (i.e. it does not trigger on future updates).
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // Subscribe only to Asset entity type
        if ($entity instanceof Asset) {
            //$entity->setAssigned(false);
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // Subscribe only to Asset entity type
        if ($entity instanceof Asset) {
            $this->saveHistory($args, 'new_item');
        }
    }

    public function preUpdate(PreUpdateEventArgs $args) // OR LifecycleEventArgs
    {
        $entity = $args->getEntity();

        if ($entity instanceof Asset) {
            //dd($args->getEntityChangeSet());
            if ($args->hasChangedField('isAssigned')) {
                $this->isAssignedChanged = true;
            }
            if ($args->hasChangedField('price')) {
                if($args->getOldValue('price') != $entity->getPrice()) {
                    $this->isPriceUpdated = true;
                }
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {

        $entity = $args->getObject();

        // Subscribe only to Asset entity type
        if (!$entity instanceof Asset) {
            return;
        }

        $entityManager = $args->getObjectManager();
        $dataChangeSet = $entityManager->getUnitOfWork()->getEntityChangeSet($entity);

        if(count($dataChangeSet) < 3 && $this->isPriceUpdated == false) {
            //return;
            //When there are not more than 2 field-changes it is because 'Price' and 'UpdatedAt' field triggered the change, you can ignore logging
        } else {
            $this->saveHistory($args,'edit');
        }

    }

    public function postRemove(LifecycleEventArgs $args)
    {
        //...
    }


    private function saveHistory(LifecycleEventArgs $args, string $action)
    {
        $description = '';
        $entity = $args->getObject();

        //Save history only when token is set
        if (!$token = $this->tokenStorage->getToken()) {
            return ;
        }
        //Save history only when user token is authenticated
        if (!$token->isAuthenticated()) {
            return ;
        }
        //Save history only when user is set
        if (!$user = $token->getUser()) {
            return ;
        }

        $user = $this->tokenStorage->getToken()->getUser();

        // ... get the entity information and log it somehow
        $history = new History();

        //If it is edit
        if($action == 'edit') {
            $entityManager = $args->getObjectManager();
            $dataChangeSet = $entityManager->getUnitOfWork()->getEntityChangeSet($entity);

            $associatedFields = $entityManager->getMetadataFactory()->getMetadataFor(get_class($entity))->getAssociationNames();
            //dd($dataChangeSet);

            if ($this->isAssignedChanged) { //Item was checked in or out

                    $description = $this->checkInOutLogs($entity, $dataChangeSet);

            } else { //Not checked in or out, must be some other asset update
                $log = '';
                $idx = 1;
                $changeSet = [];

                foreach ($dataChangeSet as $field => $change) {

                    $niceFieldName = ucfirst(strtolower($field));
                    if (isset($meta['properties']['data'][$field])) {
                        //...
                    }

                    //Check if any audit field was changed
                    else if(in_array($field, $this->auditFields)) {
                        $changeSet[$field] = [];

                        //Populate the 'was' values
                        if($change[0] == null) {
                            $was = '{no value}';
                            $wasChangeset = null;
                        } else if (in_array($field, $associatedFields)) {
                            if ($field == 'location') {
                                $was = $change[0]->getParent()->getName() . ' :: ' . $change[0]->getName();
                            } else {
                                $was = $change[0]->getName();
                            }
                            $wasChangeset = $change[0]->getId();
                        }
                        else if ($change[0] instanceof \DateTime) {
                            $was = $change[0]->format('Y-m-d');
                            $wasChangeset = $change[0]->format('Y-m-d H:is');
                        } else {
                            $was = $change[0];
                            $wasChangeset = $change[0];
                        }

                        //Populate the 'became' values
                        if($change[1] == null) {
                            $became = '{no value}';
                            $becameChangeset = null;
                        } elseif (in_array($field, $associatedFields)) {
                            if ($field == 'location') {
                                $became = $change[1]->getParent()->getName() . ' :: ' . $change[0]->getName();
                            } else {
                                $became = $change[1]->getName();
                            }
                            $becameChangeset = $change[1]->getId();
                        }
                        else if ($change[1] instanceof \DateTime) {
                            $became = $change[1]->format('Y-m-d');
                            $becameChangeset = $change[1]->format('Y-m-d H:is');
                        } else {
                            $became = $change[1];
                            $becameChangeset = $change[1];
                        }

                        $log .= "<strong>" . $niceFieldName . "</strong>";
                        $log .= " : " . $was . "<strong> &#8594; </strong>";
                        $log .= $became;
                        if ($idx < count($dataChangeSet)) {
                            $log .= "<br />";
                            $idx++;
                        }

                        $changeSet[$field][0] = $wasChangeset;
                        $changeSet[$field][1] = $becameChangeset;

                    }

                }
                //dd($changeSet);
                //dd($log);
                $description = 'Asset was updated.<br /><span>' . $log . '</span>';
                $history->setChangeSet($changeSet);
            }

            $history->setDescription($description);

        } else if ($action == 'new_item') { // it is an new item added
            $history->setDescription('Asset added into the database.');
        } else { // Any other action
            $history->setDescription('Asset changed!');
        }

        //Proceed with common history details and flush
        $history->setEvent($this->eventRepository->findOneBy(['type' => $action]));
        $history->setAsset($entity);
        $history->setUser($user);

        $this->entityManager->persist($history);
        $this->entityManager->flush();

    }

    public function checkInOutLogs(Asset $entity, $dataChangeSet) {
        $description = '';
        if($dataChangeSet['isAssigned'][0] == false && $dataChangeSet['isAssigned'][1] == true) { //Item was checked out
            //Activate asset checkout logging mechanism

            $action = 'checkout';
            $checkout_notes = '';
            if(isset($dataChangeSet['checkInOutNotes']) && $dataChangeSet['checkInOutNotes'][1] != '') {
                $checkout_notes .= '<br /><strong>Checkout notes : </strong>';
                $checkout_notes .= $dataChangeSet['checkInOutNotes'][1];
            }
            $description = '<strong>Item checked out to : </strong>' . $entity->getAssignedUser() . '<br /><strong>Location : </strong>' . $entity->getLocation()->getParent() . ' :: ' . $entity->getLocation() . '<br /><strong>Checkout status : </strong>' . $entity->getStatus() . '<br /><strong>Return date : </strong>' . $entity->getCheckInDate()->format('M d, Y') . $checkout_notes;

        } else if ($dataChangeSet['isAssigned'][0] == true && $dataChangeSet['isAssigned'][1] == false) {
            //Activate asset checkin logging mechanism
            $action = 'checkin';
            $checkin_notes = '';
            if(isset($dataChangeSet['checkInOutNotes']) && $dataChangeSet['checkInOutNotes'][1] != '') {
                $checkin_notes .= '<br /><strong>Checkin notes : </strong>';
                $checkin_notes .= $dataChangeSet['checkInOutNotes'][1];
            }
            $description = '<strong>Item returned from : </strong>' . $dataChangeSet['assignedUser'][0] . '<br /><strong>Return date : </strong>' . $entity->getCheckInDate()->format('M d, Y') . '<br /><strong>Location : </strong>' . $entity->getLocation()->getParent() . ' :: ' . $entity->getLocation() . '<br /><strong>Item status : </strong>' . $entity->getStatus() . $checkin_notes;
        }
        return $description;
    }

    protected function getClassNameOnly($entity)
    {
        $class = get_class($entity);
        $parts = explode('\\', $class);
        return end($parts);
    }

}