<?php

namespace App\Repository;

use App\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Location|null find($id, $lockMode = null, $lockVersion = null)
 * @method Location|null findOneBy(array $criteria, array $orderBy = null)
 * @method Location[]    findAll()
 * @method Location[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Location::class);
    }

    public function findByParentLocation () {
        return $this->createQueryBuilder('location')
            ->andWhere('location.parent IS NULL')
            ->orderBy('location.name', 'ASC');
    }

    public function findByParentNotNull()
    {
        return $this->createQueryBuilder('location')
            ->andWhere('location.parent IS NOT NULL')
            ->orderBy('location.name', 'ASC');
    }

    public function findAllSubLocations()
    {
        return $this->createQueryBuilder('l')
            ->select('l,a')
            ->where('l.parent IS NOT NULL')
            ->orderBy('l.parent', 'ASC')
            ->leftJoin('l.assets', 'a')
            ->getQuery()
            ->getResult();
    }

    public function findLocationsArray()
    {
        return $this->createQueryBuilder('loc')
            ->select('loc')
            ->andWhere('loc.parent IS NOT NULL')
            ->orderBy('loc.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findParentLocationsArray()
    {
        return $this->createQueryBuilder('loc')
            ->select('loc')
            ->andWhere('loc.parent IS NULL')
            ->orderBy('loc.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return Location[] Returns an array of Location objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Location
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
