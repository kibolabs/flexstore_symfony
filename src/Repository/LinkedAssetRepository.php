<?php

namespace App\Repository;

use App\Entity\LinkedAsset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LinkedAsset|null find($id, $lockMode = null, $lockVersion = null)
 * @method LinkedAsset|null findOneBy(array $criteria, array $orderBy = null)
 * @method LinkedAsset[]    findAll()
 * @method LinkedAsset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinkedAssetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LinkedAsset::class);
    }

    // /**
    //  * @return LinkedAsset[] Returns an array of LinkedAsset objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LinkedAsset
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
