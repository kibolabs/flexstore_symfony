<?php

namespace App\Repository;

use App\Entity\Asset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
//use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method Asset|null find($id, $lockMode = null, $lockVersion = null)
 * @method Asset|null findOneBy(array $criteria, array $orderBy = null)
 * @method Asset[]    findAll()
 * @method Asset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Asset::class);
    }

    public function findAllAssetsWithJoins()
    {

        return
            $this->createQueryBuilder('a')
                ->select('a, c, pc, l, pl, d, e')
                ->leftJoin('a.category', 'c')
                ->leftJoin('c.parent', 'pc')
                ->leftJoin('a.location', 'l')
                ->leftJoin('l.parent', 'pl')
                ->leftJoin('a.department', 'd')
                ->leftJoin('a.assignedUser', 'e')
                ->getQuery()
                ->getResult();

    }

    public function findAllAssetsWithParentCategory($parentCategoryID)
    {

        return
            $this->createQueryBuilder('a')
                ->select('a, c, pc')
                ->leftJoin('a.category', 'c')
                ->leftJoin('c.parent', 'pc')
                ->where("pc.id = :pcId")
                ->setParameter('pcId', $parentCategoryID)
                ->getQuery()
                ->getResult();

    }

    public function findByKeyword($keyword = null, $maxResults = 30)
    {

        $qb =  $this->createQueryBuilder('a');
        //$fields = array('a, status, c, pc, d, s, l, pl, partial u.{id, email, firstname, lastname}, h, partial hu.{id, email, firstname, lastname}, he'); //if need history info
        //$fields = array('a, status, c, pc, d, s, l, pl, partial u.{id, email, firstname, lastname}, partial h.{id, user}, partial hu.{id, email, firstname, lastname}'); //history user only
        $fields = array('a, status, c, pc, d, s, l, pl, partial u.{id, email, firstname, lastname}'); //No history info
        if($keyword) { //if keyword specified, search for it in these selected fields
            $qb->andWhere('a.name LIKE :searchquery
                 OR a.barcode LIKE :searchquery
                 OR a.sn LIKE :searchquery
                 OR pc.name LIKE :searchquery
                 OR c.name LIKE :searchquery
                 OR d.name LIKE :searchquery
                 OR s.name LIKE :searchquery
                 OR pl.name LIKE :searchquery
                 OR l.name LIKE :searchquery
                 OR u.firstname LIKE :searchquery
                 OR u.lastname LIKE :searchquery')
                ->setParameter('searchquery', "%$keyword%")
            ;
        }

        $result = $qb->select($fields)
                ->leftJoin('a.status', 'status')
                ->leftJoin('a.category', 'c')
                ->leftJoin('c.parent', 'pc')
                ->leftJoin('a.department', 'd')
                ->leftJoin('a.supplier', 's')
                ->leftJoin('a.location', 'l')
                ->leftJoin('l.parent', 'pl')
                ->leftJoin('a.assignedUser', 'u')
                ->orderBy('a.updatedAt', 'DESC')
                ->groupBy('a')
                ->setFirstResult(0)
                ->setMaxResults($maxResults)
                ->getQuery()
                ->getArrayResult()
        ;

        return $result;

//        ->addSelect('COUNT(a.name) as AssetCount')
//        ->leftJoin('a.histories', 'h')
//        ->leftJoin('h.user', 'hu')
//        ->leftJoin('h.event', 'he')
    }

    public function findByKeywordAndPaginate($keyword = null, $maxResults = 30, $currentPage = 1)
    {

        $qb =  $this->createQueryBuilder('a');
        //$fields = array('a, status, c, pc, d, s, l, pl, partial u.{id, email, firstname, lastname}, h, partial hu.{id, email, firstname, lastname}, he'); //if need history info
        //$fields = array('a, status, c, pc, d, s, l, pl, partial u.{id, email, firstname, lastname}, partial h.{id, user}, partial hu.{id, email, firstname, lastname}'); //history user only
        $fields = array('a, status, c, pc, d, s, l, pl, partial u.{id, email, firstname, lastname}'); //No history info
        if($keyword) {
            $qb->where('a.name LIKE :searchquery')
                ->orWhere('a.barcode LIKE :searchquery')
                ->orWhere('a.sn LIKE :searchquery')
                ->orWhere('pc.name LIKE :searchquery')
                ->orWhere('c.name LIKE :searchquery')
                ->orWhere('d.name LIKE :searchquery')
                ->orWhere('s.name LIKE :searchquery')
                ->orWhere('pl.name LIKE :searchquery')
                ->orWhere('l.name LIKE :searchquery')
                ->orWhere('u.firstname LIKE :searchquery')
                ->orWhere('u.lastname LIKE :searchquery')
                ->orderBy('a.updatedAt', 'DESC')
                ->groupBy('a')
                ->setParameter('searchquery', "%$keyword%")
            ;

//            ->setFirstResult(0) //Removed because the pagonator class will take care of this
//            ->setMaxResults($maxResults) //Removed because the pagonator class will take care of this
        } else {
            $qb-> where('a.name IS NOT NULL');
        }

        $query = $qb->select($fields)
                ->leftJoin('a.status', 'status')
                ->leftJoin('a.category', 'c')
                ->leftJoin('c.parent', 'pc')
                ->leftJoin('a.department', 'd')
                ->leftJoin('a.supplier', 's')
                ->leftJoin('a.location', 'l')
                ->leftJoin('l.parent', 'pl')
                ->leftJoin('a.assignedUser', 'u')
                ->getQuery()
            ;

        return $this->paginate($query, $currentPage, 3);

//                ->getArrayResult(); //Removed because the pagonator class will take care of this, // No need to manually get get the result ($query->getResult())
//        ->leftJoin('a.histories', 'h')
//        ->leftJoin('h.user', 'hu')
//        ->leftJoin('h.event', 'he')
    }


    public function listAssets()
    {
        $fields = array('a, status, c, pc, d, s, l, pl, e, h, partial hu.{id, email, firstname, lastname}, he');
        return
            $this->createQueryBuilder('a')
                ->select($fields)
                ->leftJoin('a.status', 'status')
                ->leftJoin('a.category', 'c')
                ->leftJoin('c.parent', 'pc')
                ->leftJoin('a.department', 'd')
                ->leftJoin('a.supplier', 's')
                ->leftJoin('a.location', 'l')
                ->leftJoin('l.parent', 'pl')
                ->leftJoin('a.assignedUser', 'e')
                ->leftJoin('a.histories', 'h')
                ->leftJoin('h.user', 'hu')
                ->leftJoin('h.event', 'he')
                ->getQuery()
                ->getArrayResult();
    }


    public function findTenRecentAssets()
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.updatedAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
    }

    public function listRecentAssetsByLimit($maxResults)
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.updatedAt', 'DESC')
            ->setMaxResults($maxResults)
            ->getQuery()
            ->getArrayResult()
            ;
    }


//    /**
//     * Paginator Helper
//     *
//     * Pass through a query object, current page & limit
//     * the offset is calculated from the page and limit
//     * returns an `Paginator` instance, which you can call the following on:
//     *
//     *     $paginator->getIterator()->count() # Total fetched (ie: `5` posts)
//     *     $paginator->count() # Count of ALL posts (ie: `20` posts)
//     *     $paginator->getIterator() # ArrayIterator
//     *
//     * @param $dql
//     * @param integer $page Current page (defaults to 1)
//     * @param integer $limit The total number per page (defaults to 5)
//     *
//     * @return Paginator
//     */
//    public function paginate($dql, $page = 1, $limit = 5)
//    {
//        $paginator = new Paginator($dql);
//
//        $paginator->getQuery()
//            ->setFirstResult($limit * ($page - 1)) // Offset
//            ->setMaxResults($limit); // Limit
//
//        return $paginator;
//    }

//    /**
//     * @return Asset[] Returns an array of Asset objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Asset
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


}
