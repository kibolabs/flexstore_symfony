<?php

namespace App\Service;

use App\Entity\Asset;
use App\Entity\User;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    const ASSET_PHOTO = 'asset';
    const PROFILE_PHOTO = 'user/profile';

    private $targetDirectory;
    private $imageResizerService;

    public function __construct($targetDirectory, ImageResizer $imageResizerService)
    {
        $this->targetDirectory = $targetDirectory;
        $this->imageResizerService = $imageResizerService;
    }


    public function upload(UploadedFile $file, $path = null)
    {
        $photoName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $photoName = preg_replace('#[^a-z0-9]#i', '', $photoName); //Remove all chars except letters and numbers

        $fileName = $photoName.'_'.md5(uniqid()).'.'.$file->guessExtension();


        try {
            $file->move($this->getTargetDirectory() . '/' . $path, $fileName);
            //Create photo thumbnail using a custom ImageResizer class
            $args = array(
                'height'    => 200,
                'width'     => 200,
                'img_dir' => $this->getTargetDirectory() . '/' . $path,
                'thumb_dir' => $this->getTargetDirectory() . '/' . $path . '/thumbs',
                'is_crop_hard' => 1
            );
            $img = new ImageResizer($args);
            $img->createThumbnail($fileName,200,200);
            //Or you can use Imagine() library below
//                $imagine = new Imagine();
//                $image = $imagine->open($this->getTargetDirectory() . '/' . $type . '/' . $fileName)
//                    ->resize(new Box(200,200))
//                    ->save($this->getTargetDirectory() . '/' . $type . '/thumbs2.jpg');
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;

    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    public function deletePhoto($entity, $fileToDelete)
    {
        // delete only works for Asset entity for now

        if ($entity instanceof Asset) {
            //dd('delete photo was called and photo filename is '. $fileToDelete);
            if (!empty($fileToDelete)) {
                //Check if there's a file stored in the filesystem, if it does get rid of it
                $there_is_a_file = file_exists($this->getTargetDirectory() . '/asset/' . $fileToDelete);
                $there_is_a_thumb = file_exists($this->getTargetDirectory() . '/asset/thumbs/' . $fileToDelete);
                //dd($there_is_a_file);
                if ($there_is_a_file) {
                    unlink($this->getTargetDirectory() . '/asset/' . $fileToDelete);
                }
                if ($there_is_a_thumb) {
                    unlink($this->getTargetDirectory() . '/asset/thumbs/' . $fileToDelete);
                }
            }
        } else if ($entity instanceof User) {
            //dd('delete photo was called and photo filename is '. $fileToDelete);
            if (!empty($fileToDelete)) {
                //Check if there's a file stored in the filesystem, if it does get rid of it
                $there_is_a_file = file_exists($this->getTargetDirectory() . '/user/profile/' . $fileToDelete);
                $there_is_a_thumb = file_exists($this->getTargetDirectory() . '/user/profile/thumbs/' . $fileToDelete);
                //dd($there_is_a_file);
                if ($there_is_a_file) {
                    unlink($this->getTargetDirectory() . '/user/profile/' . $fileToDelete);
                }
                if ($there_is_a_thumb) {
                    unlink($this->getTargetDirectory() . '/user/profile/thumbs/' . $fileToDelete);
                }
            }
        } else {
            //dd('delete photo was not called because it is not an instance of Asset');
            return;
        }
    }
}