<?php
//
//namespace App\Console\Commands;
//
//use App\Asset;
//use App\Employee;
//use App\Event;
//use App\History;
//use App\Office;
//use App\Project;
//use App\State;
//use App\Supplier;
//use App\Type;
//use App\User;
//use Illuminate\Console\Command;
//use Illuminate\Support\Facades\Artisan;
//use Illuminate\Support\Facades\Storage;
//
//class InventorySeeder extends Command
//{
//    /**
//     * The name and signature of the console command.
//     *
//     * @var string
//     */
//    protected $signature = 'app:seed';
//
//    /**
//     * The console command description.
//     *
//     * @var string
//     */
//    protected $description = 'Command description';
//
//    /**
//     * Create a new command instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        parent::__construct();
//    }
//
//    public function handle()
//    {
//        Artisan::call('migrate:fresh');
//
//        $this->info('Fresh migration successful');
//
//        foreach ($this->seeds() as $userSeed)
//        {
//            $user = User::create([
//                'name' => $userSeed->name,
//                'email' => $userSeed->email,
//                'password' => $userSeed->password
//            ]);
//
//            foreach ($userSeed->events as $eventSeed)
//            {
//                Event::create([
//                   'name' => $eventSeed->name,
//                    'description' => $eventSeed->description,
//                    'icon' => $eventSeed->icon,
//                    'color' => $eventSeed->color,
//                    'user_id' => $user->id
//                ]);
//            }
//
//            foreach ($userSeed->projects as $projectSeed)
//            {
//                Project::create([
//                    'name' => $projectSeed->name,
//                    'short_name' => $projectSeed->short_name,
//                    'user_id' => $user->id
//                ]);
//            }
//
//            foreach ($userSeed->suppliers as $supplierSeed)
//            {
//                Supplier::create([
//                    'name' => $supplierSeed->name,
//                    'short_name' => $supplierSeed->short_name,
//                    'phone' => $supplierSeed->phone,
//                    'email' => $supplierSeed->email,
//                    'website' => $supplierSeed->website,
//                    'user_id' => $user->id
//                ]);
//            }
//
//            foreach ($userSeed->states as $stateSeed)
//            {
//                State::create([
//                    'name' => $stateSeed->name,
//                    'icon' => $stateSeed->icon,
//                    'color' => $stateSeed->color,
//                    'user_id' => $user->id
//                ]);
//            }
//
//            foreach ($userSeed->types as $typeSeed)
//            {
//                $parentType = Type::create([
//                    'name' => $typeSeed->name,
//                    'icon' => $typeSeed->icon,
//                    'type_id' => null,
//                    'user_id' => $user->id
//                ]);
//
//                foreach ($typeSeed->children as $childTypeSeed)
//                {
//                    Type::create([
//                        'name' => $childTypeSeed->name,
//                        'icon' => $childTypeSeed->icon,
//                        'type_id' => $parentType->id,
//                        'user_id' => $user->id
//                    ]);
//                }
//            }
//
//            foreach ($userSeed->offices as $officeSeed)
//            {
//                $office = Office::create([
//                    'name' => $officeSeed->name,
//                    'short_name' => $officeSeed->short_name,
//                    'address' => $officeSeed->address,
//                    'user_id' => $user->id
//                ]);
//
//                foreach ($officeSeed->employees as $employeeSeed)
//                {
//                    Employee::create([
//                        'name' => $employeeSeed->name,
//                        'address' => $employeeSeed->address,
//                        'email' => $employeeSeed->email,
//                        'phone' => $employeeSeed->phone,
//                        'office_id' => $office->id,
//                        'user_id' => $user->id
//                    ]);
//                }
//
//                foreach ($officeSeed->assets as $assetSeed)
//                {
//                    $asset = Asset::create([
//                        'name' => $assetSeed->name,
//                        'sn' => $assetSeed->sn,
//                        'barcode' => $assetSeed->barcode,
//                        'specs' => $assetSeed->specs,
//                        'purchase_date' => $assetSeed->purchase_date,
//                        'price' => $assetSeed->price,
//                        'currency' => $assetSeed->currency,
//                        'life_years' => $assetSeed->life_years,
//                        'checked_out' => $assetSeed->checked_out,
//                        'employee_id' => $assetSeed->employee_id,
//                        'office_id' => $assetSeed->office_id,
//                        'state_id' => $assetSeed->state_id,
//                        'type_id' => $assetSeed->type_id,
//                        'project_id' => $assetSeed->project_id,
//                        'supplier_id' => $assetSeed->supplier_id,
//                        'user_id' => $user->id
//                    ]);
//
//                    foreach ($assetSeed->images as $seedImage)
//                    {
//                        $asset->images()->create([
//                            'name' => $seedImage->name,
//                            'url' => $seedImage->url,
//                            'directory' => 'api',
//                            'is_promoted' => $seedImage->is_promoted,
//                            'user_id' => $user->id
//                        ]);
//                    }
//
//                    foreach ($assetSeed->histories as $historySeed)
//                    {
//                        History::create([
//                            'event_id' => $historySeed->event_id,
//                            'asset_id' => $asset->id,
//                            'user_id' => $user->id
//                        ]);
//                    }
//                }
//            }
//        }
//
//        $this->info('All seeds planted, have fun!');
//    }
//
//    private function seeds()
//    {
//        return json_decode(Storage::disk('seeds')->get('seeds.json'));
//    }
//}
