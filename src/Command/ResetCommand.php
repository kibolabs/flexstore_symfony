<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ResetCommand extends Command
{
    protected static $defaultName = 'app:reset';

    protected function configure()
    {
        $this
            ->setDescription('Drops and creates the database as well as running fixtures')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->databaseDrop($output);
        $this->createDatabase($output);
        $this->migrationsMigrate($output);
        $this->fixturesLoad($output);
        $this->populateElastica($output);

        $io = new SymfonyStyle($input, $output);

        $io->success('Your Project has been reset!');
    }

    protected function databaseDrop($output)
    {
        $command = $this->getApplication()->find('doctrine:database:drop');

        $arguments = array(
            'command' => 'doctrine:database:drop',
            '--force'  => true,
        );

        $input = new ArrayInput($arguments);
        $command->run($input, $output);
    }

    protected function createDatabase($output)
    {
        $command = $this->getApplication()->find('doctrine:database:create');

        $arguments = array(
            'command' => 'doctrine:database:create',
        );

        $input = new ArrayInput($arguments);
        $command->run($input, $output);
    }

    protected function migrationsMigrate($output)
    {
        $command = $this->getApplication()->find('doctrine:migrations:migrate');

        $arguments = array(
            'command' => 'doctrine:migrations:migrate',
        );

        $input = new ArrayInput($arguments);
        $command->run($input, $output);

    }

    protected function fixturesLoad($output)
    {
        $command = $this->getApplication()->find('doctrine:fixtures:load');

        $arguments = array(
            'command' => 'doctrine:fixtures:load',
        );
        $input = new ArrayInput($arguments);
        $command->run($input, $output);
    }

    protected function populateElastica($output)
    {
        $command = $this->getApplication()->find('fos:elastica:populate');

        $arguments = array(
            'command' => 'fos:elastica:populate',
        );
        $input = new ArrayInput($arguments);
        $command->run($input, $output);
    }
}
