<?php

namespace App\Entity;

use App\Repository\LinkedAssetRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LinkedAssetRepository::class)
 */
class LinkedAsset
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Asset::class, inversedBy="linkedAssets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $asset;

    /**
     * @ORM\ManyToOne(targetEntity=Asset::class, inversedBy="linkedWithMe")
     * @ORM\JoinColumn(nullable=false)
     */
    private $linked;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAsset(): ?Asset
    {
        return $this->asset;
    }

    public function setAsset(?Asset $asset): self
    {
        $this->asset = $asset;

        return $this;
    }

    public function getLinked(): ?Asset
    {
        return $this->linked;
    }


    public function setLinked(?Asset $linked): self
    {
        $this->linked = $linked;

        return $this;
    }

    public function __toString()
    {
        return $this->getAsset()->getName();
    }
}
