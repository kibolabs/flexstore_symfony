<?php

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TestRepository::class)
 */
class Test
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $property1;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProperty1(): ?string
    {
        return $this->property1;
    }

    public function setProperty1(string $property1): self
    {
        $this->property1 = $property1;

        return $this;
    }
}
