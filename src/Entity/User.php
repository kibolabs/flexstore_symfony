<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"user:read"}}
 * )
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Email already taken")
 */
class User implements UserInterface
{
    public function __construct()
    {
        $this->histories = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->apiToken = bin2hex(random_bytes(52));
        $this->statuses = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->createdBy = new ArrayCollection();
        $this->assignedAssets = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getUsername();
    }


    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->firstname,
            $this->lastname,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @param $serialized
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->firstname,
            $this->lastname,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Groups({"user:read", "asset:read", "history:read"})
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=3)
     */
    private $firstname;

    /**
     * @Groups({"user:read", "asset:read", "history:read"})
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=3)
     */
    private $lastname;

    /**
     * @Groups({"user:read", "asset:read", "history:read"})
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @Assert\Length(min=3)
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resetToken;

    /**
    * @ORM\Column(type="string", unique=true, nullable=false)
    */
    private $apiToken;

    /**
     * @ORM\OneToMany(targetEntity=History::class, mappedBy="user")
     */
    private $histories;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="createdBy")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity=Status::class, mappedBy="createdBy")
     */
    private $statuses;

    /**
     * @ORM\OneToMany(targetEntity=Asset::class, mappedBy="assignedUser")
     */
    private $assignedAssets;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="createdBy")
     */
    private $createdUsers;

    /**
     * @ORM\OneToMany(targetEntity=Activity::class, mappedBy="createdBy")
     */
    private $activities;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="createdUsers")
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     *
     */
    private $deletePhoto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Skype;


    // other properties and methods

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        $username = "";
        if($this->getFirstname() != null && $this->getLastname() != null)
        {

            $username = $this->getFirstname() . " " . $this->getLastname();

        } else if ($this->getFirstname() != null && $this->getLastname() == null)
        {

            $username = $this->getFirstname();

        } else if ($this->getFirstname() == null && $this->getLastname() != null)
        {

            $username .= $this->getLastname();

        } else {

            $username .= " " . $this->getEmail();

        }

        return $username;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
        // forces the object to look "dirty" to Doctrine. Avoids
        // Doctrine *not* saving this entity, if only plainPassword changes
        $this->password = null;
        /*
         * What?! Yep, this is important. Soon, we'll use a Doctrine listener to read the plainPassword property, encode it, and update password. That means that password will be set to a value before it actually saves: it won't remain null.

So why add this weird line if it basically does nothing? Because Doctrine listeners are not called if Doctrine thinks that an object has not been updated. If you eventually create a "change password" form, then the only property that will be updated is plainPassword. Since this is not persisted, Doctrine will think the object is "un-changed", or "clean". In that case, the listeners will not be called, and the password will not be changed.

But by adding this line, the object will always look like it has been changed, and life will go on like normal.
         *
         */
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }



    /**
     * @return string
     */
    public function getApiToken(): string
    {
        return $this->apiToken;
    }


    //security methods
    public function getSalt()
    {
        // The bcrypt and argon2i algorithms don't require a separate salt.
        // You *may* need a real salt if you choose a different encoder.
        return null;
    }

    // other methods, including security methods like getRoles()

    // ...
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function getResetToken(): ?string
    {
        return $this->resetToken;
    }

    public function setResetToken(?string $resetToken): self
    {
        $this->resetToken = $resetToken;

        return $this;
    }

    /**
     * @return Collection|History[]
     */
    public function getHistories(): Collection
    {
        return $this->histories;
    }

    public function addHistory(History $history): self
    {
        if (!$this->histories->contains($history)) {
            $this->histories[] = $history;
            $history->setUser($this);
        }

        return $this;
    }

    public function removeHistory(History $history): self
    {
        if ($this->histories->contains($history)) {
            $this->histories->removeElement($history);
            // set the owning side to null (unless already changed)
            if ($history->getUser() === $this) {
                $history->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setCreatedBy($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getCreatedBy() === $this) {
                $event->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Status[]
     */
    public function getStatuses(): Collection
    {
        return $this->statuses;
    }

    public function addStatus(Status $status): self
    {
        if (!$this->statuses->contains($status)) {
            $this->statuses[] = $status;
            $status->setCreatedBy($this);
        }

        return $this;
    }

    public function removeStatus(Status $status): self
    {
        if ($this->statuses->contains($status)) {
            $this->statuses->removeElement($status);
            // set the owning side to null (unless already changed)
            if ($status->getCreatedBy() === $this) {
                $status->setCreatedBy(null);
            }
        }

        return $this;
    }

    public function getCreatedUsers(): ?self
    {
        return $this->createdUsers;
    }

    public function setCreatedUsers(?self $createdUsers): self
    {
        $this->createdUsers = $createdUsers;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getCreatedBy(): Collection
    {
        return $this->createdBy;
    }

    public function addCreatedBy(self $createdBy): self
    {
        if (!$this->createdBy->contains($createdBy)) {
            $this->createdBy[] = $createdBy;
            $createdBy->setCreatedUsers($this);
        }

        return $this;
    }

    public function removeCreatedBy(self $createdBy): self
    {
        if ($this->createdBy->contains($createdBy)) {
            $this->createdBy->removeElement($createdBy);
            // set the owning side to null (unless already changed)
            if ($createdBy->getCreatedUsers() === $this) {
                $createdBy->setCreatedUsers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Asset[]
     */
    public function getAssignedAssets(): Collection
    {
        return $this->assignedAssets;
    }

    public function addAssignedAsset(Asset $assignedAsset): self
    {
        if (!$this->assignedAssets->contains($assignedAsset)) {
            $this->assignedAssets[] = $assignedAsset;
            $assignedAsset->setAssignedUser($this);
        }

        return $this;
    }

    public function removeAssignedAsset(Asset $assignedAsset): self
    {
        if ($this->assignedAssets->contains($assignedAsset)) {
            $this->assignedAssets->removeElement($assignedAsset);
            // set the owning side to null (unless already changed)
            if ($assignedAsset->getAssignedUser() === $this) {
                $assignedAsset->setAssignedUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activities->contains($activity)) {
            $this->activities[] = $activity;
            $activity->setCreatedBy($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activities->contains($activity)) {
            $this->activities->removeElement($activity);
            // set the owning side to null (unless already changed)
            if ($activity->getCreatedBy() === $this) {
                $activity->setCreatedBy(null);
            }
        }

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletePhoto()
    {
        return $this->deletePhoto;
    }

    /**
     * @param mixed $deletePhoto
     */
    public function setDeletePhoto($deletePhoto): void
    {
        $this->deletePhoto = $deletePhoto;
    }

    public function getSkype(): ?string
    {
        return $this->Skype;
    }

    public function setSkype(?string $Skype): self
    {
        $this->Skype = $Skype;

        return $this;
    }

}