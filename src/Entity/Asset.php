<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\PostLoad;
use Doctrine\ORM\Mapping\PostPersist;
use Doctrine\ORM\Mapping\PostUpdate;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={"get", "post"},
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"asset:read", "asset:item:get"}},
 *          },
 *          "put",
 *          "delete"
 *     },
 *     normalizationContext={"groups"={"asset:read"}},
 *     denormalizationContext={"groups"={"asset:write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\AssetRepository")
 * @Entity @ORM\HasLifecycleCallbacks()
 */
class Asset
{
    private $tokenStorage;

    public function __construct()
    {
        $this->histories = new ArrayCollection();
        $this->linkedAssets = new ArrayCollection();
        $this->linkedWithMe = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @Groups({"asset:read"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"asset:read","asset:write"})
     * @SerializedName("name")
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @Groups({"asset:read"})
     * @ORM\Column(type="string", length=255)
     */
    private $sn;

    /**
     * @Groups({"asset:read"})
     * @ORM\Column(type="string", length=255)
     */
    private $barcode;

    /**
     * @Groups({"asset:read", "category:read"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="assets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @Groups({"asset:read", "location:read"})
     * @ORM\ManyToOne(targetEntity=Location::class, inversedBy="assets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @Groups({"asset:read", "status:read"})
     * @ORM\ManyToOne(targetEntity=Status::class, inversedBy="assets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $conditionNotes;

    /**
     * @Groups({"asset:item:get"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specs;

    /**
     * @Groups({"asset:item:get"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $itemtags;

    /**
     * @Groups({"asset:item:get"})
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $purchasedate;

    /**
     * @Groups({"asset:item:get", "supplier:read"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", inversedBy="assets")
     * @ORM\JoinColumn(nullable=true)
     */
    private $supplier;

//* @Assert\Type(
//*     type="decimal",
//*     message="The entered price value {{ value }} is not a valid amount, only accepts {{ type }} values."
//* )
    /**
     * @Groups({"asset:item:get"})
     * @ORM\Column(type="decimal", precision=20, scale=2, nullable=true)
     */
    private $price;


    /**
     * @Groups({"asset:item:get"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $currency;

    /**
     * @Groups({"asset:item:get"})
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lifeyears;

    /**
     * @Groups({"asset:read", "department:read"})
     * @ORM\ManyToOne(targetEntity=Department::class, inversedBy="assets")
     * @ORM\JoinColumn(nullable=true)
     */
    private $department;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @Groups({"asset:read"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     *
     */
    private $deletePhoto;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $images;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAssigned;

    /**
     * @Groups({"asset:read", "user:read"})
     * @SerializedName("assignee")
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="assignedAssets")
     */
    private $assignedUser;


    /**
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $itemNotes;

    //@Groups({"asset:item:get", "history:read"})
    //Order By with a OneToMany ORM code from https://symfonycasts.com/screencast/symfony3-doctrine-relations/one-to-many-order-by
    /**
     * @ApiSubresource()
     * @ORM\OneToMany(targetEntity="App\Entity\History", mappedBy="asset", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $histories;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastCheckoutDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $checkInDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $checkInOutNotes;

    /**
     * The assets that I think are my links.
     *
     * @ORM\OneToMany(targetEntity=LinkedAsset::class, mappedBy="asset", orphanRemoval=true)
     */
    private $linkedAssets;

    /**
     * The assets who think that I’m linked to them.
     *
     * @ORM\OneToMany(targetEntity="LinkedAsset", mappedBy="linked", orphanRemoval=true)
     */
    private $linkedWithMe;





    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSn(): ?string
    {
        return $this->sn;
    }

    public function setSn(string $sn): self
    {
        $this->sn = $sn;

        return $this;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setBarcode(string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getSpecs(): ?string
    {
        return $this->specs;
    }

    public function setSpecs(?string $specs): self
    {
        $this->specs = $specs;

        return $this;
    }

    public function getItemtags(): ?string
    {
        return $this->itemtags;
    }

    public function setItemtags(?string $itemtags): self
    {
        $this->itemtags = $itemtags;

        return $this;
    }

    public function getPurchasedate(): ?\DateTimeInterface
    {
        return $this->purchasedate;
    }

    public function setPurchasedate(?\DateTimeInterface $purchasedate): self
    {
        $this->purchasedate = $purchasedate;

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getLifeyears(): ?int
    {
        return $this->lifeyears;
    }

    public function setLifeyears(?int $lifeyears): self
    {
        $this->lifeyears = $lifeyears;

        return $this;
    }

    public function getConditionNotes(): ?string
    {
        return $this->conditionNotes;
    }

    public function setConditionNotes(?string $conditionNotes): self
    {
        $this->conditionNotes = $conditionNotes;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param $photo
     */
    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getDeletePhoto()
    {
        return $this->deletePhoto;
    }

    /**
     * @param mixed $deletePhoto
     */
    public function setDeletePhoto($deletePhoto): void
    {
        $this->deletePhoto = $deletePhoto;
    }



    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images): void
    {
        $this->images = $images;
    }

    public function getIsAssigned(): ?bool
    {
        return $this->isAssigned;
    }

    public function setIsAssigned(bool $isAssigned): self
    {
        $this->isAssigned = $isAssigned;

        return $this;
    }

    public function getAssignedUser(): ?User
    {
        return $this->assignedUser;
    }

    public function setAssignedUser(?User $assignedUser): self
    {
        $this->assignedUser = $assignedUser;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getItemNotes(): ?string
    {
        return $this->itemNotes;
    }

    public function setItemNotes(?string $itemNotes): self
    {
        $this->itemNotes = $itemNotes;

        return $this;
    }

    /**
     * @return Collection|History[]
     */
    public function getHistories(): Collection
    {
        return $this->histories;

//        //Custom sorting code from http://jdjeetonweb.blogspot.com/2015/01/filtering-and-sorting-doctrine.html
//        $historiesToReturn = $this->histories;
//        // Collect an array iterator.
//        $iterator = $historiesToReturn->getIterator();
//        // Do sort the new iterator.
//        $iterator->uasort(function ($a, $b) {
//            return ($a->getCreatedAt() > $b->getCreatedAt()) ? -1 : 1;
//        });
//        // pass sorted array to a new ArrayCollection.
//        return new ArrayCollection(iterator_to_array($iterator));
    }

    public function addHistory(History $history): self
    {
        if (!$this->histories->contains($history)) {
            $this->histories[] = $history;
            $history->setAsset($this);
        }

        return $this;
    }

    public function removeHistory(History $history): self
    {
        if ($this->histories->contains($history)) {
            $this->histories->removeElement($history);
            // set the owning side to null (unless already changed)
            if ($history->getAsset() === $this) {
                $history->setAsset(null);
            }
        }

        return $this;
    }

    public function getLastCheckoutDate(): ?\DateTimeInterface
    {
        return $this->lastCheckoutDate;
    }

    public function setLastCheckoutDate(?\DateTimeInterface $lastCheckoutDate): self
    {
        $this->lastCheckoutDate = $lastCheckoutDate;

        return $this;
    }

    public function getCheckInDate(): ?\DateTimeInterface
    {
        return $this->checkInDate;
    }

    public function setCheckInDate(?\DateTimeInterface $checkInDate): self
    {
        $this->checkInDate = $checkInDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCheckInOutNotes()
    {
        return $this->checkInOutNotes;
    }

    /**
     * @param mixed $checkInOutNotes
     */
    public function setCheckInOutNotes($checkInOutNotes): void
    {
        $this->checkInOutNotes = $checkInOutNotes;
    }


    /**
     * @return Collection|LinkedAsset[]
     */
    public function getLinkedAssets(): Collection
    {
        return $this->linkedAssets;
    }


    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    /*
     *
     * Lifecycle Callbacks are defined on an entity class.
     * They allow you to trigger callbacks whenever an instance of that entity class experiences a relevant lifecycle event.
     * More than one callback can be defined for each lifecycle event.
     * Lifecycle Callbacks are best used for simple operations specific to a particular entity class's lifecycle.
     *
     */

    /** @PrePersist */
    public function doStuffOnPrePersist()
    {
        //$this->createdAt = date('Y-m-d H:i:s');
    }

    /** @PrePersist */
    public function doOtherStuffOnPrePersist()
    {
        //$this->value = 'changed from prePersist callback!';
    }

    /** @PostPersist */
    public function doStuffOnPostPersist()
    {
        //$this->value = 'changed from postPersist callback!';
    }

    /** @PostLoad */
    public function doStuffOnPostLoad()
    {
        //$this->value = 'changed from postLoad callback!';
    }

    /** @PreUpdate */
    public function doStuffOnPreUpdate()
    {
        //$this->value = 'changed from preUpdate callback!';
    }

    /** @PostUpdate
     * @param LifecycleEventArgs $args
     */
    public function doStuffAfterUpdate(LifecycleEventArgs $args)
    {
//        $entity = $args->getObject();
//        $entityManager = $args->getObjectManager();
//        $dataChangeSet = $entityManager->getUnitOfWork()->getEntityChangeSet($entity);
        //dd($dataChangeSet);

    }

}
