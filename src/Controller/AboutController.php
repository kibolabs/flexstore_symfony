<?php
/**
 * Created by PhpStorm.
 * User: sam
 * Date: 6/10/18
 * Time: 9:45 PM
 */

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AboutController extends AbstractController
{
    /**
     * @Route("/about",name="about_page")
     */
    public function about()
    {
        return $this->render('about.html.twig');
    }
}