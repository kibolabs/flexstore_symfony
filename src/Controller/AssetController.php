<?php

namespace App\Controller;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Asset;
use App\Entity\LinkedAsset;
use App\Entity\Location;
use App\Entity\Status;
use App\Form\AssetCheckInType;
use App\Form\AssetCheckoutType;
use App\Form\AssetType;
use App\Repository\AssetRepository;
use App\Repository\EventRepository;
use App\Repository\LinkedAssetRepository;
use App\Repository\LocationRepository;
use App\Repository\StatusRepository;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use Exception;
use Knp\Bundle\TimeBundle\DateTimeFormatter;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 *
 * @Route("/asset")
 */
class AssetController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        // Avoid calling getUser() in the constructor: auth may not
        // be complete yet. Instead, store the entire Security object.
        $this->security = $security;
    }

    /**
     * @Route("/", name="asset_list", methods="GET")
     * @param AssetRepository $assetRepository
     * @param Request $request
     * @return Response
     */
    public function index(AssetRepository $assetRepository, Request $request): Response
    {
        $query_params = $request->query;
        $breadcrumbs = '' . $query_params->get('bc');
        if($query_params->has('category')) {
            $assets = $assetRepository->findBy(['category' => $query_params->get('category')]);
        } else if($query_params->has('pc')) {
            $assets = $assetRepository->findAllAssetsWithParentCategory($query_params->get('pc'));
        } else if($query_params->has('location')) {
            $assets = $assetRepository->findBy(['location' => $query_params->get('location')]);
        } else if($query_params->has('department')) {
            $assets = $assetRepository->findBy(['department' => $query_params->get('department')]);
        } else if($query_params->has('user')) {
            $assets = $assetRepository->findBy(['assignedUser' => $query_params->get('user')]);
        } else {
            $assets = $assetRepository->findAll();
        }
        return $this->render('asset/index.html.twig', [
            'assets' => $assets,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    /**
     * @Route("/new", name="asset_new", methods="GET|POST")
     * @param Request $request
     * @param EventRepository $eventRepository
     * @param FileUploader $fileUploader
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request, EventRepository $eventRepository, FileUploader $fileUploader): Response
    {
        $asset = new Asset();
        $form = $this->createForm(AssetType::class, $asset);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $photoInput = $form->get('photo')->getData();
            if ($photoInput) {
                $photoFileName = $fileUploader->upload($photoInput, FileUploader::ASSET_PHOTO);
                $asset->setPhoto($photoFileName);
            }

            $asset->setIsAssigned(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($asset);
            $em->flush();

            $this->addFlash('success', 'New asset was successfully added!');

            return $this->redirectToRoute('asset_show' , ['id' => $asset->getId()]);
        }

        return $this->render('asset/new.html.twig', [
            'asset' => $asset,
            'form' => $form->createView(),
            'errors' => $form->getErrors($form->getName())
        ]);
    }

    /**
     * @Route("/{id}", name="asset_show", methods="GET")
     * @param Asset $asset
     * @return Response
     */
    public function show(Asset $asset): Response
    {
        return $this->render('asset/show.html.twig', [
            'asset' => $asset
        ]);
    }

    /**
     * @Route("/{id}/edit", name="asset_edit", methods="GET|POST")
     * @param Request $request
     * @param Asset $asset
     * @param UserRepository $userRepository
     * @param LocationRepository $locationRepository
     * @param StatusRepository $statusRepository
     * @param FileUploader $fileUploader
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Asset $asset, UserRepository $userRepository, LocationRepository $locationRepository, StatusRepository $statusRepository, FileUploader $fileUploader): Response
    {
        $databasePhotoFilename = $asset->getPhoto();
        $form = $this->createForm(AssetType::class, $asset);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //Check if delete photo was triggered
            $photoDeleteCheck = $form->get('deletePhoto')->getData();
            if ($photoDeleteCheck) {
                //Check if there's a file stored in the filesystem, if it does get rid of it
                $fileUploader->deletePhoto($asset, $databasePhotoFilename);
                //Remove the filename from the database and set $filename to null
                $asset->setPhoto(null);
            }
            //Check if there is a new image being uploaded
            $photoInput = $form->get('photo')->getData();
            if ($photoInput) {
                $photoFileName = $fileUploader->upload($photoInput, FileUploader::ASSET_PHOTO);
                $asset->setPhoto($photoFileName);
            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Asset was successfully edited!');

            return $this->redirectToRoute('asset_show', ['id' => $asset->getId()]);
        }

        return $this->render('asset/edit.html.twig', [
            'asset' => $asset,
            'form' => $form->createView(),
            'users' => $userRepository->findAll(),
            'locations' => $locationRepository->findLocationsArray(),
            'statuses' => $statusRepository->findAll(),
            'errors' => $form->getErrors($form->getName())
        ]);
    }

    /**
     * @Route("/{id}", name="asset_delete", methods="DELETE")
     * @param Request $request
     * @param Asset $asset
     * @param FileUploader $fileUploader
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Asset $asset, FileUploader $fileUploader): Response
    {
        if(!$asset) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Asset not found, please refresh the page and retry!'
            ], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($this->isCsrfTokenValid('delete'.$asset->getId(), $request->request->get('_token'))) {
            //Check if has photo and delete it first
            if($asset->getPhoto() != null) {
                $fileUploader->deletePhoto($asset, $asset->getPhoto());
            }
            $em = $this->getDoctrine()->getManager();
            $em->remove($asset);
            $em->flush();
        } else {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Invalid token, please refresh the page and retry!'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        return $this->redirectToRoute('asset_list');
    }

    /**
     * @Route("/new/check_sn", name="check_sn", methods="POST")
     * @param AssetRepository $assetRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function check_sn(AssetRepository $assetRepository, Request $request)
    {
        $sncheck = $request->get('sncheck');
        $sn = $assetRepository->findBy(['sn' => $sncheck]);

        if (!$sn){
            return new JsonResponse('sn_ok');
        }
        else{
            return new JsonResponse('sn_bad');
        }

    }

    /**
     * @Route("/new/check_barcode", name="check_barcode", methods="POST")
     * @param AssetRepository $assetRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function check_barcode(AssetRepository $assetRepository, Request $request)
    {
        $idcheck = $request->get('intidcheck');
        $barcode = $assetRepository->findBy(['barcode' => $idcheck]);

        if (!$barcode){
            return new JsonResponse('id_ok');
        }
        else{
            return new JsonResponse('id_bad');
        }

    }

    /**
     * @Route("/{id}/history", name="asset_history", methods="POST")
     * @param Asset $asset
     * @return JsonResponse
     */
    public function getHistoryAction(Asset $asset) {
        $historyList = [];
        foreach ($asset->getHistories() as $history) {
            $historyList[] = [
                'id' => $history->getId(),
                'update_user' => $history->getUser()->getUsername(),
                'update_type' => $history->getEvent()->getType(),
                'update_notes' => $history->getDescription(),
                'update_date' => $history->getCreatedAt()->format('l, M d, Y'),
                'update_icon' => $history->getEvent()->getIconFa()
            ];
        }

        $data = [
            'histories' => $historyList,
        ];

        return new JsonResponse($data);
    }

    //Get checkout modal

    /**
     * @Route("/{id}/getmodal/checkout/", name="asset_getmodal_checkout", methods="POST")
     * @param Request $request
     * @param Asset $asset
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function assetGetModalCheckout(Request $request, Asset $asset) {
        $form = $this->createForm(AssetCheckoutType::class, $asset);
        if($request->isXmlHttpRequest()) {
            return $this->render('asset/_form_checkout_modal.html.twig', [
                'checkform' => $form->createView(),
                'asset_id' => $asset->getId()
            ]);
        } else {
            return null;
        }
    }

    /**
     * @Route("/{id}/getmodal/checkin/", name="asset_getmodal_checkin", methods="POST")
     * @param Request $request
     * @param Asset $asset
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function assetGetModalCheckin(Request $request, Asset $asset) {
        $form = $this->createForm(AssetCheckInType::class, $asset);
        if($request->isXmlHttpRequest()) {
            return $this->render('asset/_form_checkin_modal.html.twig', [
                'form' => $form->createView(),
                'asset' => $asset
            ]);
        } else {
            return null;
        }
    }

    //ITEM CHECKOUT MECHANISM

    /**
     * @Route("/{id}/checkout/", name="asset_checkout", methods="POST")
     * @param Request $request
     * @param Asset $asset
     * @param UserRepository $userRepository
     * @param DateTimeFormatter $dateFormatter
     * @return JsonResponse
     * @throws Exception
     * @IsGranted("ROLE_ADMIN")
     */
    public function assetCheckoutAction(Request $request, Asset $asset, UserRepository $userRepository, DateTimeFormatter $dateFormatter) {
        $form = $this->createForm(AssetCheckoutType::class, $asset);
        $form->handleRequest($request);

        $data = $request->request->get($form->getName());

        // FORM DATA ERROR HANDLING
        if (!$asset) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'This item could not be found in the database'
            ], JsonResponse::HTTP_NOT_FOUND);
            //throw $this->createNotFoundException(
            // 'No Asset found for id '.$asset->getId()
            //);
        } else if ($asset->getIsAssigned()) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'This item is already assigned to someone else, need to be checked in first before it can be re-assigned'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
        else if(!$data['assignedUser']){
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, Assignee can not be blank'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
        else if(!$userRepository->findOneBy(['id' => $data['assignedUser']])) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, Specified user could not be found in our system'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
        else if($data['lastCheckoutDate'] == ""){
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, check-out date can not be blank'
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else if($data['location']  == ""){
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, location can not be blank'
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else if(!$this->getDoctrine()
            ->getRepository(Location::class)
            ->findOneBy(['id' => $data['location']])) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, Specified location could not be found in our system',
            ], JsonResponse::HTTP_NOT_FOUND);
        } else {
            // edit item info. to include assignees
            $asset->setIsAssigned(true);

            $asset->setAssignedUser($userRepository
                ->findOneBy(['id' => $data['assignedUser']]));
            $asset->setLastCheckoutDate(new \DateTime($data['lastCheckoutDate']));
            $asset->setCheckInDate(new \DateTime($data['checkInDate']));
            $asset->setCheckInOutNotes($data['checkInOutNotes']);
            $asset->setLocation($this->getDoctrine()
                ->getRepository(Location::class)
                ->findOneBy(['id' => $data['location']]));

            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return new JsonResponse([
                'status' => 'success',
                'message' => 'Item checked out successfully!',
                'details' => [
                    'assignee' => $asset->getAssignedUser()->getUsername(),
                    'checkout_date' => $asset->getLastCheckoutDate()->format('M d, Y'),
                    'due_date' => $asset->getCheckInDate()->format('M d, Y'),
                    'days_diff' => $dateFormatter->formatDiff($asset->getCheckInDate(), (new \DateTime())),
                ]
            ], JsonResponse::HTTP_OK);
        }
    }


    //ITEM CHECKIN MECHANISM
    /**
     * @Route("/{id}/checkin/", name="asset_checkin", methods="POST")
     * @param AssetRepository $assetRepository
     * @param Request $request
     * @param Asset $asset
     * @param EventRepository $eventRepository
     * @return JsonResponse
     * @throws Exception
     * @IsGranted("ROLE_ADMIN")
     */
    public function assetCheckinAction(AssetRepository $assetRepository, Request $request, Asset $asset, EventRepository $eventRepository) {
        $form = $this->createForm(AssetCheckInType::class, $asset);
        $form->handleRequest($request);

        $data = $request->request->get($form->getName());

        // FORM DATA ERROR HANDLING
        if (!$asset) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'This item could not be found in the database'
            ], JsonResponse::HTTP_NOT_FOUND);
            //throw $this->createNotFoundException(
            // 'No Asset found for id '.$asset->getId()
            //);
        } else if (!$asset->getIsAssigned()) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'This item is not assigned to anyone yet'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
        else if(!$data['checkInDate']){
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, check-in date must be specified'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
        else if($data['location']  == ''){
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, check-in location must be specified'
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else if(!$this->getDoctrine()
            ->getRepository(Location::class)
            ->findOneBy(['id' => $data['location']])) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, Specified location could not be found in our system',
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else if (!$data['status']) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, Return condition was not specified!'
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else if (!$this->getDoctrine()
            ->getRepository(Status::class)
            ->findOneBy(['id' => $data['status']])) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Unsuccessful, Specified asset status could not be found in our system'
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else {
            // edit item info. to include assignees
            $asset->setIsAssigned(false);

            $asset->setAssignedUser(null);
            $asset->setStatus($this->getDoctrine()
                ->getRepository(Status::class)
                ->findOneBy(['id' => $data['status']]));
            $asset->setConditionNotes($data['conditionNotes']);
            $asset->setCheckInDate(new \DateTime($data['checkInDate']));
            $asset->setCheckInOutNotes($data['checkInOutNotes']);
            $asset->setLocation($this->getDoctrine()
                ->getRepository(Location::class)
                ->findOneBy(['id' => $data['location']]));

            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return new JsonResponse([
                'status' => 'success',
                'message' => 'Item checked in successfully!',
                'details' => [
                    'status' => $asset->getStatus()->getName(),
                    'conditionNotes' => $asset->getConditionNotes()
                ]
            ], JsonResponse::HTTP_OK);
        }
    }

    /**
     * @Route("/{id}/link", name="link_asset", methods="POST")
     * @param AssetRepository $assetRepository
     * @param Request $request
     * @param Asset $asset
     * @param LinkedAssetRepository $linkedAssetRepository
     * @return JsonResponse
     * @IsGranted("ROLE_ADMIN")
     */
    public function linkAsset(AssetRepository $assetRepository, Request $request, Asset $asset, LinkedAssetRepository $linkedAssetRepository)
    {
        $id_asset_to_link = $request->get('asset_to_link');
        $asset_to_link = $assetRepository->findOneBy(['id' => $id_asset_to_link]);
        $linkedAsset = $linkedAssetRepository->findOneBy(['asset' => $asset, 'linked' => $asset_to_link]);

        if (!$asset_to_link){
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Asset you are trying to link could not be found in our system!'
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else if ($asset->getlinkedAssets()->contains($linkedAsset)) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'These assets are already linked!'
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else if ($asset === $asset_to_link) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'You can not link an asset with itself!'
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else {
            $linkedAsset_side_1 = new LinkedAsset();
            $linkedAsset_side_1->setAsset($asset);
            $linkedAsset_side_1->setLinked($asset_to_link);

            $linkedAsset_side_2 = new LinkedAsset();
            $linkedAsset_side_2->setAsset($asset_to_link);
            $linkedAsset_side_2->setLinked($asset);

            $em = $this->getDoctrine()->getManager();
            $em->persist($linkedAsset_side_1);
            $em->persist($linkedAsset_side_2);
            $em->flush();

            return new JsonResponse([
                'status' => 'success',
                'message' => 'Asset linked successfully!'
            ]);
        }

    }

    /**
     * @Route("/{id}/unlink", name="unlink_asset", methods="POST")
     * @param AssetRepository $assetRepository
     * @param Request $request
     * @param Asset $asset
     * @param LinkedAssetRepository $linkedAssetRepository
     * @return JsonResponse
     * @IsGranted("ROLE_ADMIN")
     */
    public function unLinkAsset(AssetRepository $assetRepository, Request $request, Asset $asset, LinkedAssetRepository $linkedAssetRepository)
    {
        $id_asset_to_unlink = $request->get('asset_to_unlink');
        $asset_to_unlink = $assetRepository->findOneBy(['id' => $id_asset_to_unlink]);
        $linkedAsset1 = $linkedAssetRepository->findOneBy(['asset' => $asset, 'linked' => $asset_to_unlink]);
        $linkedAsset2 = $linkedAssetRepository->findOneBy(['asset' => $asset_to_unlink, 'linked' => $asset]);

        if (!$asset_to_unlink){
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Asset you are trying to unlink could not be found in our system!'
            ]);
        } else if (!$asset->getlinkedAssets()->contains($linkedAsset1)) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'These assets are not linked!'
            ]);
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($linkedAsset1);
            $em->remove($linkedAsset2);
            $em->flush();

            return new JsonResponse([
                'status' => 'success',
                'message' => 'Asset unlinked successfully!'
            ]);
        }

    }

    /**
     * @Route("/search", name="asset_search", methods="POST")
     * @param AssetRepository $assetRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function search(AssetRepository $assetRepository, Request $request): JsonResponse
    {
        //if($request->isXmlHttpRequest()) {
        //$assets = $assetRepository->findAll();
        //$jsondata = $this->get('serializer')
            //->serialize($assets, 'json');
        $searchquery = $request->get('headerSearch');
        $assets = [];
        if ($searchquery) {
            $assets = $assetRepository->findByKeyword($searchquery);
        }

        return new JsonResponse([
            'status' => 'success',
            'message' => 'All assets retrieved successfully.',
            'assets' => $assets
        ], 200);
        //}
    }


    /**
     * @Route("/assetsArray", name="asset_array", methods="POST")
     * @param AssetRepository $assetRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return JsonResponse
     */
    public function assetsArray(AssetRepository $assetRepository, Request $request, PaginatorInterface $paginator): JsonResponse
    {
        //if($request->isXmlHttpRequest()) {
        //$assets = $assetRepository->findAll();
        //$jsondata = $this->get('serializer')
        //->serialize($assets, 'json');


        $assets = [];
        $assets = $assetRepository->findByKeyword();

        return new JsonResponse([
            'status' => 'success',
            'message' => 'All assets retrieved successfully.',
            'assets' => $assets
        ], 200);
        //}
    }

}


