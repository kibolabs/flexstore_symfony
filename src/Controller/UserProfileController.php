<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\UserEmailType;
use App\Form\UserInfoType;
use App\Form\UserPasswordType;
use App\Form\UserPhotoType;
use App\Model\UserPassowrdChangeModel;
use App\Service\FileUploader;
use App\Service\FormErrorSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/profile")
 * @method User|null getUser()
 */
class UserProfileController extends AbstractController
{
    /**
     * @var FormErrorSerializer
     */
    private $formErrorSerializer;

    public function __construct(FormErrorSerializer $formErrorSerializer)
    {
        $this->formErrorSerializer = $formErrorSerializer;
    }

    /**
     * @Route("/", name="user_profile", methods={"GET","POST"})
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return Response
     */
    public function profile(Request $request, FileUploader $fileUploader): Response
    {
        $user = $this->getUser();
        $editnameform = $this->createForm(UserInfoType::class, $user);
        $profilePhotoForm = $this->createForm(UserPhotoType::class, $user, array('csrf_protection' => true));

        if($request->isMethod('POST') && $request->request->get('_action') == 'photo_upload') {
            $existing_photo = $user->getPhoto();
            $profilePhotoForm->submit($request->request->get($profilePhotoForm->getName()));
            if ($profilePhotoForm->isSubmitted()) {
                if ($profilePhotoForm->isValid()){
                    //dd('was submitted and was valid');
                    ///** @var UploadedFile $uploadedFile */
                    $photoFile = $request->files->get('photoFile');

                    if ($photoFile) {
                        if($existing_photo) {
                            $fileUploader->deletePhoto($user, $existing_photo);
                        }
                        $photoFileName = $fileUploader->upload($photoFile, FileUploader::PROFILE_PHOTO);
                        $user->setPhoto($photoFileName);

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($user);
                        $em->flush();

                        return new JsonResponse([
                            'status' => 'success',
                            'message' => 'profile photo uploaded successfully',
                            'photo_thumb' => '/uploads/user/profile/thumbs/' . $user->getPhoto()
                        ], JsonResponse::HTTP_ACCEPTED);
                    } else {
                        return new JsonResponse([
                            'status' => 'failed',
                            'message' => 'A photo was not uploaded',
                            'errors' => 'no file found!',
                        ], JsonResponse::HTTP_BAD_REQUEST);
                    }
                } else {
                    //dd('was submitted but was not valid');
                    $this->getDoctrine()->getManager()->refresh($user);
                    //$errors = $profilePhotoChangeForm->getErrors(true);
                    $errors = $this->formErrorSerializer->convertFormToArray($profilePhotoForm);

                    return new JsonResponse([
                        'status' => 'failed',
                        'message' => 'some errors occurred',
                        'errors' => $errors,
                    ], JsonResponse::HTTP_BAD_REQUEST);
                }
            } else {
                return new JsonResponse([
                    'status' => 'failed',
                    'message' => 'A photo was not submitted',
                ], JsonResponse::HTTP_BAD_REQUEST);
            }
        }

        return $this->render('user/profile.html.twig', [
            'user' => $user,
            'usernameform' => $editnameform->createView(),
            'profilePhotoForm' => $profilePhotoForm->createView(),
        ]);
    }

    /**
     * @Route("/edit-info", name="user_profile_edit_info", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function profileInfoEdit(Request $request): Response
    {
        $user = $this->getUser();
        $editnameform = $this->createForm(UserInfoType::class, $user);
        $editnameform->handleRequest($request);

        if ($editnameform->isSubmitted() && $editnameform->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse([
                'status' => 'success',
                'message' => 'Saved successfully',
                'username' => $user->getUsername(),
                'firstname' => $user->getFirstname(),
                'lastname' => $user->getLastName(),
                'phone' => $user->getPhone(),
                'address' => $user->getAddress(),
            ], 200);
        }
        else {
            $this->getDoctrine()->getManager()->refresh($user);
            $errors = $this->formErrorSerializer->convertFormToArray($editnameform);
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'some errors occurred',
                'errors' => $errors,
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/change-email", name="user_profile_edit_email", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function profileEmailEdit(Request $request): Response
    {
        $user = $this->getUser();
        $editemailform = $this->createForm(UserEmailType::class, $user);
        $editemailform->handleRequest($request);

        if ($editemailform->isSubmitted() && $editemailform->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'success',
                'Email successfully updated!'
            );

            return $this->redirectToRoute('user_profile');
        }

        //Refreshes the persistent state of an object from the database, overriding any local changes that have not yet been persisted.
        $this->getDoctrine()->getManager()->refresh($user);

        return $this->render('user/edit_email.html.twig', [
            'user' => $user,
            'form' => $editemailform->createView(),
        ]);
    }

    /**
     * @Route("/change-password", name="user_profile_edit_password", methods="GET|POST")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function profilePasswordEdit(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $changePasswordModel = new UserPassowrdChangeModel();

        $user = $this->getUser();

        $editpasswordform = $this->createForm(UserPasswordType::class, $changePasswordModel);
        $editpasswordform->handleRequest($request);

        if ($editpasswordform->isSubmitted() && $editpasswordform->isValid()) {

            $new_pwd = $editpasswordform->get('plainPassword')->getData();

            $new_pwd_encoded = $passwordEncoder->encodePassword($this->getUser(), $new_pwd);

            $user->setPassword($new_pwd_encoded);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                'Password updated successfully !'
            );
            //Refreshes the persistent state of an object from the database, overriding any local changes that have not yet been persisted.
            $this->getDoctrine()->getManager()->refresh($user);

            //Redirect to user profile page after successful password change
            return $this->redirectToRoute('user_profile');

        } elseif ($editpasswordform->isSubmitted() && !$editpasswordform->isValid()) {
            $this->addFlash(
                'error',
                'There was an error while trying to change your password, please try again !'
            );
        }
        //Refreshes the persistent state of an object from the database, overriding any local changes that have not yet been persisted.
        $this->getDoctrine()->getManager()->refresh($user);

        return $this->render('user/edit_pass.html.twig', [
            'user' => $user,
            'form' => $editpasswordform->createView(),
        ]);
    }

    /**
     * @Route("/delete-profile-photo", name="user_profile_photo_delete", methods="POST")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return Response
     */
    public function profilePhotoDelete(Request $request, FileUploader $fileUploader): Response
    {
        $user = $this->getUser();

        //Check if there's a file stored in the filesystem, if it does get rid of it
        $fileUploader->deletePhoto($user, $user->getPhoto());
        //Remove the filename from the database and set $filename to null
        $user->setPhoto(null);

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'status' => 'success',
            'message' => 'profile photo deleted successfully',
        ], JsonResponse::HTTP_ACCEPTED);
    }


}