<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\User;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use App\Service\FormErrorSerializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/category")
 */
class CategoryController extends AbstractController
{
    /**
     * @var FormErrorSerializer
     */
    private $formErrorSerializer;

    public function __construct(FormErrorSerializer $formErrorSerializer)
    {
        $this->formErrorSerializer = $formErrorSerializer;
    }

    /**
     * @Route("/", name="category_index", methods="GET|POST")
     * @param CategoryRepository $categoryRepository
     * @param Request $request
     * @return JsonResponse|Response
     *
     */
    public function index(CategoryRepository $categoryRepository, Request $request)
    {
        //NEW CATEGORY ADD MECHANISM
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        //$newCatform->handleRequest($request);

        if($request->isXmlHttpRequest()) {
            if($request->get('action')) {
                $action = $request->get('action');
                if($action == 'getNewCatForm') {
                    return $this->render('category/_form.html.twig', [
                        'form' => $form->createView()
                    ]);
                } else {
                    return new JsonResponse([
                        'status' => 'failed',
                        'message' => 'no action specified, please retry!',
                    ], JsonResponse::HTTP_BAD_REQUEST);
                }
            } else {
                //dd('new cat submitted');
                $form->submit($request->request->get($form->getName()));
                if ($form->isSubmitted()) {
                    if ($form->isValid()){
                        //dd('was submitted and was valid');

                        //Check if parent category ID was submitted
                        if($request->get('catid')) {
                            $parentCategory = $categoryRepository->findOneBy(['id'=>$request->get('catid')]);
                            $category->setParent($parentCategory);
                        }

                        // GET LOGGED IN USER OBJECT
                        $user = $this->getUser();
                        /** @var User $user */
                        $category->setAddedBy($user);

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($category);
                        $em->flush();

                        return new JsonResponse([
                            'status' => 'success',
                            'message' => 'New category was successfully saved',
                            'category' => [
                                'id' => $category->getId(),
                                'name' => $category->getName()
                            ]
                        ], JsonResponse::HTTP_ACCEPTED);
                    } else {
                        //dd('was submitted but was not valid');
                        //$errors = $newCatform->getErrors(true);
                        $errors = $this->formErrorSerializer->convertFormToArray($form);

                        return new JsonResponse([
                            'status' => 'failed',
                            'message' => 'some errors occurred while saving a category',
                            'errors' => $errors,
                        ], JsonResponse::HTTP_BAD_REQUEST);
                    }
                } else {
                    return new JsonResponse([
                        'status' => 'failed',
                        'message' => 'A category was not submitted',
                    ], JsonResponse::HTTP_BAD_REQUEST);
                }
            }
        }

        return $this->render('category/index.html.twig', [
            'categories' => $categoryRepository->findBy(
                ['parent' => NULL],
                ['name' => 'ASC']
            )
        ]);
    }

    /**
     * @Route("/{id}/edit", name="category_edit", methods="POST")
     * @param Request $request
     * @param Category $category
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Category $category): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($request->isXmlHttpRequest()) {
            if($request->get('action')) {
                $action = $request->get('action');
                if($action == 'getEditCatForm') {
                    return $this->render('category/_form.html.twig', [
                        'form' => $form->createView(),
                        'action' => 'edit',
                        'id' => $category->getId()
                    ]);
                } else {
                    return new JsonResponse([
                        'status' => 'failed',
                        'message' => 'no action specified, please retry!',
                    ], JsonResponse::HTTP_BAD_REQUEST);
                }
            }
            if ($form->isSubmitted() && $request->isXmlHttpRequest()) {

                if($form->isValid()) {
                    $this->getDoctrine()->getManager()->flush();

                    return new JsonResponse([
                        'status' => 'success',
                        'message' => 'Category was successfully edited',
                        'category' => [
                            'id' => $category->getId(),
                            'name' => $category->getName()
                        ]
                    ], 200);
                } else {
                    $errors = $this->formErrorSerializer->convertFormToArray($form);
                    return new JsonResponse([
                        'status' => 'failed',
                        'message' => 'some errors occurred while editing a category',
                        'errors' => $errors,
                    ], JsonResponse::HTTP_BAD_REQUEST);
                }

            }
        }


        return new JsonResponse([
            'status' => 'failed',
            'errors' => 'sorry you can not just call this outside Ajax'
        ], JsonResponse::HTTP_BAD_REQUEST);


//        $form_html = $this->renderView('category/_form.html.twig', [
//                        'newCatform' => $form->createView()
//                    ]);

//        return $this->render('category/edit.html.twig', [
//            'category' => $category,
//            'form_html' => $form_html,
//        ]);

    }

    /**
     * @Route("/{id}", name="category_delete", methods="DELETE")
     * @param Request $request
     * @param Category $category
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Category $category): Response
    {
        $childCount = $category->getChildren()->count();
        if($childCount > 0) {
            $childPlural = 'child';
            $categoryPlural = 'category';
            if($childCount > 1) {
                $childPlural = 'children';
                $categoryPlural = 'categories';
            }
            return new JsonResponse([
                'status' => 'failed',
                'errors' => 'the category is associated with ' . $category->getChildren()->count() . ' ' . $childPlural . ', please delete the child ' . $categoryPlural . ' first'
            ], JsonResponse::HTTP_BAD_REQUEST);
        } else {
            //if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
            //}

            return new JsonResponse([
                'status' => 'ok'
            ], 204);
            //return $this->redirectToRoute('category_index');
        }

    }

    /**
     * @Route("/getform/new/", name="category_getform_new", methods="POST")
     * @param Request $request
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function categoryGetFormNew(Request $request) {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        if($request->isXmlHttpRequest()) {
            return $this->render('category/_form.html.twig', [
                'newCatform' => $form->createView()
            ]);
        } else {
            return null;
        }
    }
}
