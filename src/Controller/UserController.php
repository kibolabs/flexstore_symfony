<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     *
     */
    public function index(): Response
    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        return $this->render('user/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     * @IsGranted("ROLE_MANAGE_USERS")
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // after validating the user and saving them to the database
            // do anything else you need here, like send an email to the registered user

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {   if($this->getUser() == $user) {
            return $this->redirectToRoute('user_profile');
        }
        return $this->render('user/profile.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @return Response
     * @IsGranted("ROLE_MANAGE_USERS")
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/make-owner/{id}", name="user_role_make_owner", methods={"POST"})
     * @param Request $request
     * @param User $user
     * @return Response
     * @IsGranted("ROLE_MANAGE_OWNER")
     */
    public function makePortalOwner(Request $request, User $user): Response
    {
        //$requesting_user = $this->getUser();
        //$this->denyAccessUnlessGranted('ROLE_MANAGE_ADMINS', null, 'Only portal owners can do this :( Please ask your portal owner!');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_MANAGE_OWNER')) {

//            if ($this->get('security.authorization_checker')->isGranted('ROLE_OWNER', $user)) {
//                return new JsonResponse([
//                    'status' => 'failed',
//                    'message' => $user->getUsername() . ' is already a portal owner',
//                ], JsonResponse::HTTP_BAD_REQUEST);
//            } else {
                $em = $this->getDoctrine()->getManager();
                $user->setRoles([]);
                $user->setRoles(['ROLE_OWNER']);
                $em->persist($user);
                $em->flush();

                return new JsonResponse([
                    'status' => 'success',
                    'message' => $user->getUsername() . ' is now a portal owner',
                ], 200);
//            }
        }
        else {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Only super administrators can do this :( Please ask your portal developer or super administrator!',
                'role' => 'Portal owner'
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * @Route("/make-admin/{id}", name="user_role_make_admin", methods={"POST"})
     * @param User $user
     * @return Response
     * @IsGranted("ROLE_MANAGE_ADMINS")
     */
    public function makePortalAdmin(User $user): Response
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_MANAGE_ADMINS')) {
//            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN', $user)) {
//                return new JsonResponse([
//                    'status' => 'failed',
//                    'message' => $user->getUsername() . ' is already an admin',
//                ], JsonResponse::HTTP_BAD_REQUEST);
//            } else {
                $em = $this->getDoctrine()->getManager();
                $user->setRoles([]);
                $user->setRoles(['ROLE_ADMIN']);
                $em->persist($user);
                $em->flush();

                return new JsonResponse([
                    'status' => 'success',
                    'message' => $user->getUsername() . ' is now an admin',
                    'role' => 'Administrator'
                ], 200);
//            }
        }
        else {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Only portal owners can do this :( Please ask your portal owner!',
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * @Route("/make-user/{id}", name="user_role_make_user", methods={"POST"})
     * @param User $user
     * @return Response
     * @IsGranted("ROLE_MANAGE_ADMINS")
     */
    public function makeStandardUser(User $user): Response
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_MANAGE_ADMINS')) {
//            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN', $user)) {
                $em = $this->getDoctrine()->getManager();
                $user->setRoles([]);
                $user->setRoles(['ROLE_USER']);
                $em->persist($user);
                $em->flush();

                return new JsonResponse([
                    'status' => 'success',
                    'message' => $user->getUsername() . ' is now a standard user',
                    'role' => 'Standard user'
                ], 200);

//            } else {
//                return new JsonResponse([
//                    'status' => 'failed',
//                    'message' => $user->getUsername() . ' is already a standard user',
//                ], JsonResponse::HTTP_BAD_REQUEST);
//            }
        }
        else {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Only portal admins or portal owners can do this :( Please ask your portal admin!',
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }
    }
}
