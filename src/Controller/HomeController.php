<?php
/**
 * Created by PhpStorm.
 * User: sam
 * Date: 5/12/18
 * Time: 11:19 PM
 */

namespace App\Controller;


use App\Repository\AssetRepository;
use App\Repository\CategoryRepository;
use App\Repository\DepartmentRepository;
use App\Repository\LocationRepository;
use App\Repository\UserRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{


    /**
     * @Route("/", name="homepage")
     * @param CategoryRepository $categoryRepository
     * @param LocationRepository $locationRepository
     * @param UserRepository $userRepository
     * @param DepartmentRepository $departmentRepository
     * @return Response
     */
    public function home(CategoryRepository $categoryRepository, LocationRepository $locationRepository, UserRepository $userRepository, DepartmentRepository $departmentRepository, AssetRepository $assetRepository)
    {
        // or add an optional message - seen by developers
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'You must be logged in to access this page');
        return $this->render('home.html.twig', [
            'categories' => $categoryRepository->findAllSubcategories(),
            'locations' => $locationRepository->findAllSubLocations(),
            'users' => $userRepository->findAll(),
            'departments' => $departmentRepository->findAll(),
            'recentAssets' => $assetRepository->findTenRecentAssets()
        ]);
    }
}