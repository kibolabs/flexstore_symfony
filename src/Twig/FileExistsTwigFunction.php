<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FileExistsTwigFunction extends AbstractExtension
{
    /**
     *Return the function registered as twig extension
     *
     *@return array
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('file_exists', array($this, 'checkFile')),
        );
    }

    public function checkFile($file)
    {
        $base_path = '../public/';
        return file_exists($base_path . $file);
    }
}