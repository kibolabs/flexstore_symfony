<?php


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class PregReplaceTwigFunction extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('custom_replace', [$this, 'pregReplace']),
        ];
    }

    public function pregReplace(string $instring, string $replacethis, string $withthat) {
        $pattern = '/' . $replacethis . '/i';
        return preg_replace($pattern, $withthat, $instring);
    }

}