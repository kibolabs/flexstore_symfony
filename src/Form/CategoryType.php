<?php

namespace App\Form;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('parent')
            ->add('parent', EntityType::class, [
                'label' => 'Parent category',
                'placeholder' => 'This is Parent Category',
                'class' => Category::class,
                'query_builder' => function(CategoryRepository $repo) {
                    return $repo->findByParentCategory();
                },
                'required' => false
            ])
            ->add('name', TextType::class, [
                'label' => 'Category name',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
