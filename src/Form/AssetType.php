<?php

namespace App\Form;

use App\Entity\Asset;
use App\Entity\Category;
use App\Entity\Department;
use App\Entity\Location;
use App\Entity\Status;
use App\Entity\Supplier;
use App\EventListener\SupplierListener;
use App\Repository\CategoryRepository;
use App\Repository\LocationRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class AssetType extends AbstractType
{
//    private $supplierListener;

//    public function __construct(SupplierListener $supplierListener)
//    {
//        $this->supplierListener = $supplierListener;
//    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('sn')
            ->add('barcode')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'placeholder' => "Select category",
                'query_builder' => function (CategoryRepository $categoryRepository) {
                    return $categoryRepository->findByParentNull();
                },
                'group_by' => 'parent'
            ])
            ->add('location', EntityType::class, [
                'class' => Location::class,
                'placeholder' => "Select location",
                'query_builder' => function (LocationRepository $locationRepository) {

                    return $locationRepository->findByParentNotNull();
                },
                'group_by' => 'parent'
            ])
            ->add('status', EntityType::class,[
                'class' => Status::class,
                'expanded' => true,
                'multiple' => false
            ])
            ->add('specs')
            ->add('itemtags')
            ->add('supplier', EntityType::class, [
                'class' => Supplier::class,
                'placeholder' => 'Select supplier'
            ])
            ->add('purchasedate', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('price')
            ->add('currency', ChoiceType::class, [
                'choices' => [
                    'USD' => 'usd',
                    'TZS' => 'tzs'
                ]
            ])
            ->add('lifeyears')
            ->add('department', EntityType::class,[
                'required' => false,
                'class' => Department::class,
                'placeholder' => "Select department"
            ])
            ->add('conditionNotes')
            ->add('icon')
            ->add('photo', FileType::class, [
                'data_class' => null,
                'label' => 'Asset photo',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image file, only allows png and jpeg formats!',
                    ])
                ],
            ])
            ->add('deletePhoto', CheckboxType::class, [
                'required' => false,
            ])
            ->add('images', FileType::class, [
                'multiple' => true,
                'data_class' => null
            ])
            ->add('itemNotes')
        ;

        //$builder->addEventSubscriber($this->supplierListener);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Asset::class
        ]);
    }
}
