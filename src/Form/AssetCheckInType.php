<?php

namespace App\Form;

use App\Entity\Asset;
use App\Entity\Location;
use App\Entity\Status;
use App\Repository\LocationRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssetCheckInType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastCheckoutDate', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('checkInDate', DateType::class, [
                'widget' => 'single_text',
                'required' => true
            ])
            ->add('location', EntityType::class, [
                'label' => 'Checked out to (Location)',
                'class' => Location::class,
                'placeholder' => "Select location",
                'query_builder' => function (LocationRepository $locationRepository) {

                    return $locationRepository->findByParentNotNull();
                },
                'group_by' => 'parent'
            ])
            ->add('status', EntityType::class,[
                'class' => Status::class,
                'expanded' => true,
                'multiple' => false
            ])
            ->add('conditionNotes')
            ->add('checkInOutNotes')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Asset::class
        ]);
    }
}
