<?php

namespace App\Form;

use App\Entity\Asset;
use App\Entity\Location;
use App\Entity\User;
use App\Repository\LocationRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssetCheckoutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('assignedUser', EntityType::class, [
                'class' => User::class,
                'placeholder' => "Select assigned user",
            ])
            ->add('lastCheckoutDate', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('checkInDate', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('location', EntityType::class, [
                'label' => 'Checked out to (Location)',
                'class' => Location::class,
                'placeholder' => "Select location",
                'query_builder' => function (LocationRepository $locationRepository) {

                    return $locationRepository->findByParentNotNull();
                },
                'group_by' => 'parent'
            ])
            ->add('checkInOutNotes')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Asset::class
        ]);
    }
}
