<?php


namespace App\Model;

use App\Entity\User;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

class UserPassowrdChangeModel
{
    /**
     * @Assert\Type(type="App\Entity\User")
     * @Assert\Valid()
     */
    protected $user;

    /**
     * @Assert\Length(
     *     min = 6,
     *     minMessage = "Password should by at least 6 chars long"
     * )
     */
    protected $plainPassword;


    public function getUser()
    {
        return $this->user;
    }


    public function setUser(User $user): void
    {
        $this->user = $user;
    }


    public function getOldPassword()
    {
        return $this->oldPassword;
    }


    public function setOldPassword($oldPassword): void
    {
        $this->oldPassword = $oldPassword;
    }


    public function getPlainPassword()
    {
        return $this->plainPassword;
    }


    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

}