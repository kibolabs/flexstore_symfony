var Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/App.js')
    .addEntry('login' , './assets/js/login.js')
    .addEntry('asset_add_edit', './assets/js/asset_add_edit.js')
    .addEntry('asset_index', './assets/js/asset_index.js')
    .addEntry('asset_show', './assets/js/asset_show.js')
    .addEntry('profile' , './assets/js/profile.js')
    .addEntry('category' , './assets/js/category.js')


    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    .enableStimulusBridge('./assets/controllers.json')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // Copy files or folders to the build directory.
    //
    // Add multiple configs in a single call
    // * Encore.copyFiles([
    //     *     { from: './assets/images' },
    // *     { from: './txt', pattern: /\.txt$/ },
    // * ]);
    // *
    // * // Set the context path: files will be copied
    // * // into an images/ directory in the output dir
    // * Encore.copyFiles({
    //     *     from: './assets/images',
    //     *     to: '[path][name].[hash:8].[ext]',
    //     *     context: './assets'
    //     * });
    .copyFiles([
        {
            from: './assets/fonts',
            to: 'fonts/[path][name].[hash:8].[ext]'
        },
        {
            from: './assets/img',
            to: 'images/[path][name].[hash:8].[ext]'
        },
    ])

    // enables Sass/SCSS support
    //.enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use React
    //.enableReactPreset()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
